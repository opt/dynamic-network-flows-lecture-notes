%!TEX root = ../dynamic-network-flows.tex
% !TeX spellcheck = en_GB

\section{Instantaneous Dynamic Equilibria}\label{sec:IDE}

\begin{definition}\label{def:IDELabels}
	Let $f$ be a Vickrey flow in a dynamic flow network~$\network$. We define the \emph{shortest remaining instantaneous distance} from node~$v \in V$ at time~$\theta \in \IR$ by
		\[\lRi(\theta) \coloneqq \inf\Set{\sum_{e \in p}\Big(\tau_e+\tfrac{\ql(\theta)}{\nu_e}\Big) | p \text{ a \stpath[v]}}.\]
	We say that an edge $e=vw$ is \emph{active}\dindex{edge!active} at time $\theta$ if it satisfies $\lRi(\theta) = \tau_e+\frac{\ql(\theta)}{\nu_e} + \lRi[w](\theta)$. We denote by $\setAEi$ the set of all active edges at time $\theta$.
\end{definition}

\begin{remark}
	If we reverse the directions of all edges in~$\network$ and consider~$t$ as the source, then $\lRi(\theta)$ are just the node labels \wrt the edge costs $\tau_e+\frac{\ql(\theta)}{\nu_e}$ in the sense of \Cref{def:NodeLabels}. Moreover, the active edges are just the efficient edges. In particular, we can, therefore, apply \Cref{lemma:PropertiesOfNodeLabels} to the node labels $\lRi$ as well (with the appropriate adjustments).
	
	Moreover, the functions $\lRi$ are absolutely continuous as function in time.
\end{remark}

\begin{definition}\label{def:IDE}
	A Vickrey flow $f$ in~$\network$ is an \emph{instantaneous dynamic equilibrium (IDE)}\dindex{instantaneous dynamic equilibrium}\dindex{IDE|see{instantaneous dynamic equilibrium}} if it satisfies
		\[f^+_e(\theta) > 0 \implies e \in \setAEi\]
	for $e \in E$ and almost all $\theta \in \IR$.
\end{definition}


\subsection{Existence}

\begin{definition}\label{def:NetworkInflowRate}
	Let $u: \IR \to \IRnn$ be some locally integrable function. A dynamic flow~$f$ then has $u$ as its \emph{network inflow rate}\dindex{network inflow rate} if it satisfies
		\[u(\theta) = \sum_{e \in \edgesFrom{s}}f^+_e(\theta) - \sum_{e \in \edgesTo{s}}f^-_e(\theta)\]
	for almost all $\theta \in \IR$.
\end{definition}

\begin{theorem}\label{thm:IDEExistence}
	Let $\network$ be a dynamic flow network, where $t$ is reachable from every node in the network and $u \in \pInt[2]$ a (non-negative) network inflow rate with bounded support. Assume that there exists some time horizon~$T$ such that any Vickrey flow with network inflow rate~$u$ which satisfies the IDE property until time~$T$ becomes an IDE (for all times) if we set all its flow rates to zero after~$T$
	
	Then, there exists an IDE in~$\network$ with network inflow rate~$u$.
\end{theorem}

\begin{proof}
	Let $T \in \IRnn$ some time horizon such that all IDE in~$\network$ with $u$ as network inflow rate have support within $[0,T]$. We now define a set
	\begin{align*}
		K \coloneqq \Set{f \in \pInt[2]^{E\times\set{\pm}} | 
			\begin{array}{l}
				\supp(f) \subseteq [0,T], f \text{ is non-negative,}\\
				\text{satisfies strong flow conservation at all nodes,}\\
				\text{has $u$ as network inflow rate and respects capacities}
			\end{array}}
	\end{align*}
	and a mapping\footnote{By this, we mean that there exists some $h^-_e$ such that $(f^+_e,h^-_e)$ is a Vickrey flow and $g^-_e$ and $h^-_e$ coincide until $T$.}
	\begin{align*}
		\Gamma: K \to \PSet{K}, f \mapsto \Set{g \in K | 
			\begin{array}{l}
				(f^+_e,g^-_e) \text{ is Vickrey flow until time~$T$ for any edge } e \in E \\
				\text{and } g^+_e(\theta) = 0 \text{ f.a.a.~} \theta \in \IR \text{ where } e \text{ is inactive \wrt } f
			\end{array}}, 
	\end{align*}
	We observe that the IDE in~$\network$ with network inflow rate~$u$ are exactly the flows $f \in K$ with $f \in \Gamma(f)$. Showing existence of such an IDE is, therefore, equivalent to showing existence of a fixed point of $\Gamma$. For this, we will use the following two claims together with the fixed point theorem by Kakutani, Fan and Glicksberg (\Cref{thm:KFGFixedPoint}).
	
	\begin{claim}\label{claim:IDEExistencePropertiesOfK}
		The set $K$ is non-empty, convex and weakly compact.
	\end{claim}

	\begin{proofClaim}
		\begin{description}
			\item[$K$ non-empty:] Fix any edge $sv \in \edgesFrom{s}$ (which must exist since $t$ is reachable from $s$ by assumption). Then, we define a dynamic flow $f$ by setting $f^+_{sv} \coloneqq u$, $f^-_{sv} \coloneqq 0$ and $f^+_e \coloneqq f^-_e \coloneqq 0$ for all other edges $e \in E$. This clearly defines an element in~$K$.
			
			\item[$K$ is convex:] This follows directly from the definition of $K$.
			
			\item[$K$ is weakly compact:] This follows from \Cref{thm:UniBallWeaklyClosedIffReflexive} since $\pInt[2]$ is reflexive and $K$ is both bounded and weakly closed. \qedhere
		\end{description}
	\end{proofClaim}

	\begin{claim}\label{claim:IDEExistencePropertiesOfGamma}
		The mapping $\Gamma$ has a weakly closed graph and non-empty, convex images.
	\end{claim}

	\begin{proofClaim}
		We start by fixing some $f \in K$.
		\begin{description}
			\item[$\Gamma(f)$ is non-empty:] We start by defining $g^-$ such that $(f^+_e,g^-_e)$ is a Vickrey flow for every edge $e \in E$ (which is uniquely possible by \Cref{lemma:EdgeLoadingExistence,lemma:EdgeLoadingUniqueness}). Then, we adjust $g^-_e$ by setting them to zero after time~$T$. 
			
			Next, we define the corresponding edge inflow rates~$g^+_e$ on a node-by-node basis: For any fixed node $v \in V \setminus\set{t}$ we partition the interval $[0,T]$ into measurable sets $(I_{v,e})_{e \in \edgesFrom{v}}$ such that for every time $\theta \in [0,T]$ with $\theta \in I_{v,e}$ the edge $e$ is active \wrt $f$. This is possible since there is always at least some outgoing active edge (\cf \Cref{lemma:PropertiesOfNodeLabels}) and both the node labels~$\lRi(\theta)$ and the edge travel times~$\tau_e + \frac{\ql(\theta)}{\nu_e}$ are continuous functions \wrt time.
			
			With this we define
				\[g^+_e(\theta) \coloneqq \begin{cases}
					\sum_{e' \in \edgesTo{v}}g^-_{e'}(\theta), &\text{ if } \theta \in I_{v,e'} \text{ and } v\neq s \\
					u(\theta) + \sum_{e' \in \edgesTo{v}}g^-_{e'}(\theta), &\text{ if } \theta \in I_{v,e'} \text{ and } v = s \\
					0,											&\text{ else}
				\end{cases}.\]
			
			It is now easy to see that this dynamic flow $g$ now satisfies both $g \in K$ and $g \in \Gamma(f)$.
			
			\item[$\Gamma(f)$ is convex:] This follows directly from the definition of the set $\Gamma(f)$ together with the observation that we have $g^- = h^-$ for all $g,h \in \Gamma(f)$ by the uniqueness of the edge-loading mapping (\Cref{lemma:EdgeLoadingUniqueness}).
			
			\item[$\Gamma$ has weakly closed graph:] We will show that the graph of $\Gamma$ is even weakly compact (which implies that it is weakly closed since the weak topology on $\pInt[2]$ is hausdorff).
			By \Cref{thm:WeakCompactnessIfWeakSequentialCompactness} this is equivalent to showing that $\graph{\Gamma}$ is weakly sequentially compact. Hence, let $(f^n,g^n)_{n \in \INo}$ be any sequence in $\graph{\Gamma} \subseteq K \times K$. As $K$ is weakly (sequentially) compact by \Cref{claim:IDEExistencePropertiesOfK}, it must contain a subsequence $(f^{n_k},g^{n_k})_{k \in \INo}$ converging weakly to some $(f,g) \in K \times K$. We will now show that $(f,g) \in \graph{\Gamma}$:
			
			Since the (restricted) edge loading mapping $\edgeloading^T$ is weak-weak sequentially continuous (\Cref{lemma:EdgeLoadingContinuous}), we have $g^{n_k,-}_e = \edgeloading^T(f^{n_k,+}_e) \convto[w] \edgeloading^T(f^+_e)$. At the same time we have $g^{n_k,-}_e \convto[w] g^-_e$ and, therefore, $g^-_e = \edgeloading^T(f^+_e)$ since limit points in Hausdorff spaces are unique. Thus, $(f^+_e,g^-_e)$ are Vickrey flows until~$T$.
			
			Now, assume for contradiction that we nevertheless have $(f,g) \notin \graph{\Gamma}$. Then, there must be some edge $e = vw \in E$ and some set $M \subseteq [0,T]$ of positive measure such that we have $g^+_e(\theta) > 0$ for all $\theta \in M$ while edge~$e$ is inactive \wrt $f$ for all those times. We may even assume that there exists some $\eps > 0$ such that we have $\lRi^f(\theta) + \eps \leq \lRi[w]^f(\theta) + \tau_e + \frac{\ql^f(\theta)}{\nu_e}$ for all $\theta \in M$. By \Cref{claim:IntegrationIsWeakStrongContinuous} we know that $\lRi^{f^{n_k}}$, $\lRi[w]^{f^{n_k}}$ and $\ql^{f^{n_k}}$ all converge strongly (\ie uniformly) to $\lRi^f$, $\lRi[w]^f$ and $\ql^f$, respectively. Hence, there exists some $K \in \INo$ such that $e$ is inactive during~$M$ \wrt all $f^{n_k}$ for $k \geq K$. This, however, implies that we have $g^{n_k,+}_e(\theta) = 0$ for almost all $\theta \in M$ and all such $k$. Since $g^{n_k,+}_e$ converges weakly to $g^+_e$, this implies $g^+_e(\theta)=0$ for almost all $\theta \in M$ which is a contradiction to our choice of~$M$. \qedhere
		\end{description}
	\end{proofClaim}
	Using these two claim \Cref{thm:KFGFixedPoint} now guarantees the existence of a fixed point of~$\Gamma$.
\end{proof}


\subsection{Termination}

\begin{definition}
	Let $f$ be a dynamic flow and $T \in \IRnn$. We say that $f$ \emph{terminates before $T$}\dindex{flow!terminating} if $\supp(f) \subseteq [0,T]$.
\end{definition}

\begin{lemma}\label{lemma:IDETerminationAcyclic}
	Let $\network$ be an acyclic \stnetwork{} and $u$ a network inflow rate with bounded support. Then, there exists some finite time horizon~$T$ such that all IDE in $\network$ with $u$ as network inflow rate terminate before time~$T$.
\end{lemma}

\begin{proof}
	Let $v_1 \prec v_2 \prec \dots \prec v_n$ be a topological order on~$\network$. We observe that we can assume \wlg that we have $v_1=s$ and $v_n=t$ as nodes outside this range can never be reached by an IDE. Next, we define for any $i \in [n]$ the function
		\[M_i: \IR \to \IRnn, \theta \mapsto \begin{cases}
			\int_0^\theta u(\zeta)\diff\zeta,	&\text{ if } i=1 \\
			\sum_{e \in \edgesTo{\set{v_i,\dots,v_n}}}F^-_e(\theta),	&\text{ else}
		\end{cases}\]
	denoting the total volume of flow closer to the sink than node $v_i$ at any time~$\theta$. Moreover, we denote by $U \coloneqq \int_0^\infty u(\zeta)\diff\zeta$ the total amount of flow entering the network.
	
	\begin{claim}
		For any $i \in [n]$ there exists some (flow independent!)  $T_i \in \IRnn$ with $M_i(T_i) \geq U$.
	\end{claim}

	\begin{proofClaim}
		We show this claim via induction on $i$:
		\begin{proofbyinduction}
			\basecase{$i=1$} This follows directly from the assumption that the network inflow rate $u$ has a bounded support.
			
			\inductionstep{$i\to i+1$} Using strong flow conservation at node~$v_i$ and the fact that $\prec$ is a topological order, we get
				\[M_i(T_i) - M_{i+1}(T_i) = \sum_{e \in \edgesFrom{v_i}}F^+_e(T_i) - \sum_{e \in \edgesFrom{v_i}}F^-_e(T_i).\]
			We now define 
				\[T_{i+1} \coloneqq T_i + \max\Set{\tau_e + \tfrac{U}{\nu_e} | e \in \edgesFrom{v_i}}.\]
			We observe that flow conservation guarantees that we have $F^+_e(T_i) - F^-_e(T_i) \leq U$ for all edges $e \in E$ and, therefore, \Cref{ex:VickreyFlowNoIdleing} ensures that we have 			
				\[F^-_e(T_{i+1}) - F^-_e(T_i) \geq F^+_e(T_i) - F^-_e(T_i)\]
			and, therefore, 
				\[M_{i+1}(T_{i+1}) \geq M_{i+1}(T_i) + \sum_{e \in \edgesFrom{v_i}}\big(F^-_e(T_{i+1})-F^-_e(T_i)\big) \geq M_i(T_i) \geq U\]
			by induction. \qedhere
		\end{proofbyinduction}
	\end{proofClaim}
	Finally, from this claim we get the existence of some time horizon $T \coloneqq T_n$ such that for any IDE~$f$ in the given network with network inflow rate~$u$ we have $\fval{f,T} \geq U$. Flow conservation together with the acyclicity of~$\network$ then implies that the support of $f$ is in $[0,T]$.
\end{proof}

\begin{theorem}\label{thm:IDETermination}
	Let $\network$ be a dynamic flow network where all capacities and travel times are at least~$1$. Moreover, let $u$ be a network inflow rate with bounded support. Then, there exists some finite time horizon~$T$ such that all IDE in $\network$ with $u$ as network inflow rate terminate before time~$T$.
\end{theorem}

\begin{proof}
	Let $G=(V,E)$ be the underlying graph of~$\network$. We define the set
		\[\tilde E \coloneqq \Set{e=vw \in E | \lRi(0)+1 > \lRi[w](0)+\tau_e}\]
	of almost active edges and observe that the subgraph $(V,\tilde E)$ is acyclic (note also, that $\lRi(0)$ is independent of the flow as queues are always empty at time~$0$). Furthermore, let $v_1 \prec \dots \prec v_n$ be a topological order of $(V,\tilde E)$. Then, for any $i \in [n]$ and any interval $[a,b]$ we define the value
		\[\hat M_i(a,b) \coloneqq \sum_{e \in E_i}(F^+_e(a)-F^-_e(a))+\sum_{e \in \edgesTo{V_i}}\int_a^b f^-_e(\zeta)\diff\zeta + \begin{cases}
			\int_a^b u(\zeta)\diff\zeta, &\text{ if } s \in V_i \\
			0,							&\text{ else}
		\end{cases},\]
	where $V_i \coloneqq \set{v_i,\dots,v_n}$ and $E_i \coloneqq \set{e=vw \in E| v,w\in V_i}$. This is therefore the sum of all flow in the subgraph $(V_i,E_i)$ at time~$a$ plus all flow entering this subgraph between~$a$ and~$b$. In particular, this value is an upper bound on the total flow volume on all edges of this subgraph at any point in time during~$[a,b]$. This value, in turn, is of interest to us because whenever it is small, then the only active edges in $(V_i,E_i)$ are the edges in $\tilde E$ (which, importantly, form an acyclic subgraph!):
	
	\begin{claim}\label{claim:OnlyAlmostActiveEdgesActiveInSinkLikeSubgraph}
		If $\sum_{e \in \tilde E \cap E_i}\big(F^+_e(\theta)-F^-_e(\theta)\big) < 1$, then we have $\setAEi\cap E_i \subseteq \tilde E$.
	\end{claim}

	\begin{proofClaim}
		Fix any edge $e=vw \in \setAEi \cap \tilde E$ and let $p$ a \stpath[v][t] with $\lRi(0) = \tau_p$. Since we clearly have $p \subseteq \tilde E$, we then get
		\begin{align*}
			\lRi[w](0) + \tau_e 
				&\leq \lRi[w](\theta) + \tau_e + \tfrac{\ql(\theta)}{\nu_e} \leq \lRi[v](\theta) \leq \sum_{e' \in p}\left(\tau_{e'} + \tfrac{\ql[e'](\theta)}{\nu_{e'}}\right) \\
				&\leq \sum_{e'\in p}\tau_{e'} + \sum_{e' \in p}\ql[e'](\theta) < \lRi[v](0) + 1.
		\end{align*}
		This shows that $e \in \tilde E$.
	\end{proofClaim}

	Now, let $T^\ast \in \IR$ be some time with $\supp(u) \subseteq [0,T^\ast]$. If then, at any time $T' \geq T^\ast$, we have $\hat M_1(T',T') < 1$, then the same will be true for all times after that time and, by, \Cref{claim:OnlyAlmostActiveEdgesActiveInSinkLikeSubgraph}, the flow can only use edges in $\tilde E$ then anymore. Since those edges form an acyclic subgraph, the flow is then guaranteed to terminate by \Cref{lemma:IDETerminationAcyclic}. 
	
	Therefore, in order to show the \namecref{thm:IDETermination}, it suffices to show that there exists some (flow independent) time $T' \geq T^\ast$, such that we have $\hat M_1(T',T')<1$ for all IDE in the given network. We will prove this by showing that for any sufficiently long time interval $[a,b]$ we must have either $\fval{f,b} \geq \fval{f,a}+1$ or $\hat M_1(a,a) < 1$. As the former is equivalent to $\hat M_n(a,b) \geq 1$, we will from now on focus on intervals with $\hat M_n(a,b) < 1$ and show that for those we are guaranteed to have $\hat M_1(a,a) < 1$ as well.
	
	We now fix some $i \in [n-1]$, some time interval $[a,b]$ and define $\tauMax \coloneqq \max\set{\tau_e | e \in E}$. Our goal is to show that if we have $\hat M_{i+1}(a,b+\tauMax+1) < 1$, then we also have $M_i(a,b) < 1$. We subdivide this proof into \Cref{claim:FlowVolumeOnEdgesTowardSinkLikeSubgraph,claim:NotToMuchFlowInExtensionOfSinkLikeSubgraph,claim:FlowDistributionAtSinkLikeSubgraphExtensionNode,claim:SinkLikeSubgraphExtension}:
	
	\begin{claim}\label{claim:FlowVolumeOnEdgesTowardSinkLikeSubgraph}
		If we have $\hat M_{i+1}(a,b+\tauMax+1) < 1$, then we have $F^+_e(\theta)-F^-_e(\theta) < 1$ for all $\theta \in [a,b]$ and all $e \in \edgesTo{V_{i+1}}$.
	\end{claim}

	\begin{proofClaim}
		By way of contradiction, assume that there exists some time $\theta \in [a,b]$ and edge $e \in \edgesTo{V_{i+1}}$ with $F^+_e(\theta)-F^-_e(\theta)\geq 1$. Using \Cref{ex:VickreyFlowNoIdleing}, this implies 
			\[\int_a^{b+\tauMax+1}f^-_e(\zeta)\diff\zeta \geq F^-_e(\theta+\tau_e+1)-F^-_e(\theta) \geq 1\]
		and, therefore, $\hat M_{i+1}(a,b+\tauMax+1) \geq 1$, which is a contradiction.
	\end{proofClaim}

	\begin{claim}\label{claim:NotToMuchFlowInExtensionOfSinkLikeSubgraph}
		If we have $\hat M_{i+1}(a,b+\tauMax+1) < 1$, then we have $\sum_{e \in \tilde E \cap E_i}\big(F^+_e(\theta)-F^-_e(\theta)\big) < 1$ for any $\theta \in [a,b]$.
	\end{claim}

	\begin{proofClaim}
		We start by observing that we have $(E_i \setminus E_{i+1})\cap\tilde E \subseteq \edgesTo{V_{i+1}}$ since $v_i \prec v$ for all $v \in V_{i+1}$ and $\prec$ is a topological order on $(V,\tilde E)$. Therefore, \Cref{claim:FlowVolumeOnEdgesTowardSinkLikeSubgraph} and \Cref{ex:VickreyFlowNoIdleing} imply that for any such edge $e \in (E_i \setminus E_{i+1})\cap\tilde E$ we have
			\[F^+_e(\theta)-F^-_e(\theta) \leq \int_\theta^{b+\tauMax+1}f^-_e(\zeta)\diff\zeta.\]
		Using this, we get
		\begin{align*}
			&\sum_{e \in \tilde E \cap E_i}\big(F^+_e(\theta)-F^-_e(\theta)\big) \leq \sum_{e \in (E_i \setminus E_{i+1})\cap\tilde E}\big(F^+_e(\theta)-F^-_e(\theta)\big) + \sum_{e \in \tilde E \cap E_{i+1}}\big(F^+_e(\theta)-F^-_e(\theta)\big) \\
				&\quad \leq \sum_{e \in \edgesTo{V_{i+1}}}\int_\theta^{b+\tauMax+1}f^-_e(\zeta)\diff\zeta + \sum_{e \in \tilde E \cap E_{i+1}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesTo{V_{i+1}}}\int_a^\theta f^-_e(\zeta)\diff\zeta \\
					&\hspace{30em} + \sum_{\substack{v \in V_{i+1}:\\v=s}}\int_a^b u(\zeta)\diff\zeta \\
				&\quad \leq \sum_{e \in \tilde E \cap E_{i+1}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesTo{V_{i+1}}}\int_a^{b+\tauMax+1} f^-_e(\zeta)\diff\zeta + \sum_{\substack{v \in V_{i+1}:\\v=s}}\int_a^{b+\tauMax+1} u(\zeta)\diff\zeta \\
				&\quad = M_{i+1}(a,b+\tauMax+1) < 1. \qedhere
		\end{align*}
	\end{proofClaim}

	Now \Cref{claim:OnlyAlmostActiveEdgesActiveInSinkLikeSubgraph,claim:NotToMuchFlowInExtensionOfSinkLikeSubgraph} ensure that during $[a,b]$ all active edges in $E_i$ are also in $\tilde E$, meaning that any flow arriving at some node $v \in V_i$ during this time interval may only enter edges in~$\tilde E$. 
		
	\begin{claim}\label{claim:FlowDistributionAtSinkLikeSubgraphExtensionNode}
		If we have $\hat M_{i+1}(a,b+\tauMax+1) < 1$ and $b\geq a+2\tauMax+2$, then we have 
			\begin{align*}
				&\sum_{e \in E_i\setminus E_{i+1}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesTo{v_i}\setminus\edgesFrom{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta + \begin{cases}
																								\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
																								0, &\text{ else}
																							\end{cases} \\
				&\quad\quad\quad\quad\leq \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(b)-F^-_e(a)\big).
			\end{align*}
	\end{claim}

	\begin{proofClaim}
		We start by considering the edges $e \in \edgesFrom{V_{i+1}}\cap\edgesTo{v_i}$. By \Cref{ex:VickreyFlowNoIdleing} we have $\int_a^{a+\tauMax+1} f^-_e(\zeta)\diff \geq \min\set{1,F^+_e(a)-F^-_e(a)}$. As observed previously, all this flow then may only enter edges in $\tilde E \cap \edgesFrom{v_i}$. As these edges satisfies $F^+_e(a+\tauMax+1)-F^-_e(a+\tauMax+1)<1$, all this flow has left these edges again by time $a+2\tauMax+2$, meaning that it has a total flow volume of less than~$1$. Hence, we have $\int_a^{a+\tauMax+1} f^-_e(\zeta)\diff \geq F^+_e(a)-F^-_e(a)$ for all $e \in \edgesFrom{V_{i+1}}\cap\edgesTo{v_i}$.
		
		Now, using again the fact that flow arriving at node~$v_i$ during $[a,b]$ may only enter edges in $\edgesFrom{v_i} \cap \tilde E$ we get
		\begin{align*}
			&\sum_{e \in E_i\setminus E_{i+1}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesTo{v_i}\setminus\edgesFrom{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta + \begin{cases}
								\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
								0, &\text{ else}
							\end{cases} \\
				&\quad = \sum_{e \in \edgesFrom{V_{i+1}}\cap\edgesTo{v_i}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(a)-F^-_e(a)\big) \\
					&\hspace{10em}  + \sum_{e \in \edgesTo{v_i}\setminus\edgesFrom{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta + \begin{cases}
						\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
						0, &\text{ else}
					\end{cases} \\
				&\quad \leq \sum_{e \in \edgesFrom{V_{i+1}}\cap\edgesTo{v_i}}\int_a^{a+\tauMax+1} f^-_e(\zeta)\diff \\
					&\hspace{10em}  + \sum_{e \in \edgesTo{v_i}\setminus\edgesFrom{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta + 	\begin{cases}
						\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
						0, &\text{ else}
					\end{cases} \\
				&\quad \leq \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesTo{v_i}}\int_a^b f^-_e(\zeta)\diff + 	\begin{cases}
					\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
					0, &\text{ else}
				\end{cases} \\
				&\quad \leq \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesFrom{v_i}\cap\tilde E}\int_a^b f^+_e(\zeta)\diff \\
				&\quad \leq \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(a)-F^-_e(a)\big) + \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\int_a^b f^+_e(\zeta)\diff\\
				&\quad = \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(b)-F^-_e(a)\big) \qedhere
		\end{align*}
	\end{proofClaim}

	\begin{claim}\label{claim:SinkLikeSubgraphExtension}
		Let $[a,b]$ be an interval with $b\geq a+2\tauMax+2$.
		
		Then, we have
			\[\hat M_{i+1}(a,b+\tauMax+1)< 1 \implies \hat M_i(a,b) < 1.\]
	\end{claim}

	\begin{proofClaim}
		This is essentially a straightforward calculation:
		\begin{align*}
			&\hat M_i(a,b) - \hat M_{i+1}(a,b+\tauMax+1) \\
				&\quad\leq \sum_{e \in E_i\setminus E_{i+1}}(F^+_e(a)-F^-_e(a))+\sum_{e \in \edgesTo{V_i}\setminus\edgesTo{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta \\
						&\quad\quad\quad\quad - \sum_{e \in \edgesTo{V_{i+1}}\setminus\edgesTo{V_i}}\int_a^{b+\tauMax+1} f^-_e(\zeta)\diff\zeta + 	\begin{cases}
							\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
							0, &\text{ else}
						\end{cases} \\
				&\quad= \sum_{e \in E_i\setminus E_{i+1}}(F^+_e(a)-F^-_e(a))+\sum_{e \in \edgesTo{v_i}\setminus\edgesFrom{V_{i+1}}}\int_a^b f^-_e(\zeta)\diff\zeta \\
						&\quad\quad\quad\quad - \sum_{e \in \edgesTo{V_{i+1}}\setminus\edgesTo{V_i}}\int_a^{b+\tauMax+1} 		f^-_e(\zeta)\diff\zeta + 	\begin{cases}
							\int_a^b u(\zeta)\diff\zeta, &\text{ if } v_i=s\\
							0, &\text{ else}
						\end{cases} \\
				&\quad \Croverset{claim:FlowDistributionAtSinkLikeSubgraphExtensionNode}{\leq} \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(b)-F^-_e(a)\big) - \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\int_a^{b+\tauMax+1} f^-_e(\zeta)\diff\zeta \\
				&\quad= \sum_{e \in \edgesFrom{v_i}\cap\edgesTo{V_{i+1}}}\big(F^+_e(b)-F^-_e(b+\tauMax+1)\big) \leq 0,
		\end{align*}
		where the last inequality holds because of \Cref{claim:FlowVolumeOnEdgesTowardSinkLikeSubgraph} and \Cref{ex:VickreyFlowNoIdleing}.
	\end{proofClaim}

	With this \namecref{claim:SinkLikeSubgraphExtension} we can now finally conclude \Cref{thm:IDETermination}'s proof as follows: Consider any time-interval $[a,b]$ with $b \geq a+\abs{V}\cdot(\tauMax+1)$ and $\hat M_n(a,b)<1$. Then, iteratively applying \Cref{claim:SinkLikeSubgraphExtension} shows that we have $\hat M_1(a,a)<1$ (and, therefore, the flow terminates by \Cref{lemma:IDETerminationAcyclic}). 
\end{proof}

\subsection{Computation}\label{sec:IDEComputation}

\begin{definition}
	A function $f: \IR \to \IR$ is \emph{right-constant}\dindex{right-constant} if there exist finitely many points $a_0, a_1, \dots, a_k$ such that $f$ is constant on each of the intervals 
		\[(-\infty,a_0),[a_0,a_1), \dots [a_{k-1},a_k),[a_k,\infty).\]
	We call an equivalence class of functions $[f] \in \locInt[\IR]$ \emph{right-constant} if it contains at least one right-constant representative.
\end{definition}

\begin{remark}
	If some $[f] \in \locInt[\IR]$ is right-constant, then it contains exactly one right-constant representative. Hence, we can choose this as the canonical representative.
\end{remark}

\begin{lemma}\label{lemma:IDEExtensionCharacterization}
	Let $\network$ be a dynamic flow network with strictly positive travel times $\tau_e > 0$ and $f$ a dynamic flow with network inflow rate~$u$. Let $[a,b)$ be some interval such that all edge inflow rates are constant during this interval and for any edge~$e \in E$ its edges outflow function~$f^-_e$ is constant during $[a+\tau_e,b+\tau_e)$. Moreover, assume that no edges switch between being active and non-active during $(a,b)$.
	
	Then, any edge $e=vw \in E$ is active during $[a,b)$ if and only if it is active at time~$a$ and we have
	\begin{align*}
		\rDeriv{\lRi[w]}(a)+\psi_e(f^+_e(a)) \leq \rDeriv{\lRi[w']}(a)+\psi_{e'}(f^+_{e'}(a)) 
	\end{align*}
	for all $e' = vw' \in \edgesFrom{v}\cap\setAEi[a]$, where 
		\[\psi_e(x) \coloneqq \begin{cases}
									\frac{x}{\nu_e}-1, &\text{ if } \ql(a) > 0\\
									\max\set{\frac{x}{\nu_e}-1,0}, &\text{ else}
							\end{cases} \text{ for all } e \in \edgesFrom{v}.\]
\end{lemma}

\begin{proof}
	Due to our assumption on the edge in- and outflow rates, we know that all queue length functions are affine on $[a,b)$. Moreover, since the set of active edges remains unchanged during $(a,b)$, we can deduce from this, that the functions $\lRi$ are also affine on this interval. Hence, we have $\ql(\theta) = \ql(a)+(\theta-a)\cdot\rDeriv{\ql}(a)$ and $\lRi(\theta) = \lRi(a)+(\theta-a)\rDeriv{\lRi}(a)$ for all $e \in E$, $v \in V$ and $\theta \in [a,b)$, where $\rDeriv$ denotes the right side derivative. 
	
	With this we directly get that
		\[\lRi(\theta) = \tfrac{\ql(\theta)}{\nu_e}+\tau_e+\lRi[w](\theta) \text{ for all } \theta \in [a,b)\]
	(\ie $e=vw$ being active during $[a,b)$) is equivalent to 
		\[\lRi(a) = \tfrac{\ql(a)}{\nu_e}+\tau_e+\lRi[w](a) \quad\text{ and }\quad \rDeriv{\lRi}(a) = \tfrac{\rDeriv{\ql}(a)}{\nu_e}+\rDeriv{\lRi[w]}(a).\]
	Now, using the definition of $\lRi$ together with \Cref{lemma:PropertiesOfNodeLabels:SolutionToBE} and \Cref{lemma:DifferentiationRuleForMinima} we get
		\[\rDeriv{\lRi}(a) = \min\Set{\tfrac{\rDeriv{\ql[e'](a)}}{\nu_{e'}}+\rDeriv{\lRi[w']}(a) | e' = vw' \in \edgesFrom{v}\cap\setAEi[a]}.\]
	From this, the \namecref{lemma:IDEExtensionCharacterization} using the formula for the derivative of the queue length function for Vickrey flows from \Cref{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv}:
		\[\rDeriv{\ql}(a) = \psi_e(a)\cdot\nu_e. \qedhere\]
\end{proof}

\begin{algorithm}[H]
	\caption{IDE-Extension Algorithm}\label{alg:IDEExtension}\aindex{IDE-Extension}
	
	\Input{A dynamic flow network $\network$ with $\tau_e >0$, a right constant network inflow rate, an IDE $f$ until $T$ with right-constant flow rates}
	\Output{A constant extension of $f$ until $T+\eps$ for some $\eps > 0$}
	
	Fix some topological order $v_n \prec v_{n-1} \prec \dots \prec v_1=t$ on $(V,\setAEi[T])$
	
	Set $x_e \coloneqq 0$ for all $e \in \edgesFrom{t}$ and $r_t \coloneqq 0$
	
	\For{$i=2,\dots,n$}{ 
		
		Set $x_e \coloneqq 0$ for all $e \in \edgesFrom{v_i} \setminus \setAEi[T]$
		
		Find $(x_e) \in \IRnn^{\edgesFrom{v_i}\cap\setAEi[T]}$ satisfying 
			\[x_e > 0 \implies \psi_e(x_e)+r_w \leq \psi_{e'}(x_{e'})+r_{w'} \text{ f.a. } e=vw,e'=vw' \in \edgesFrom{v_i}\cap\setAEi[T]\]
		and $\sum_e x_e = \sum_{e \in \edgesTo{v_i}}f^-_e(T)+\begin{cases}u(T),&\text{ if } v_i=s\\0,&\text{ else}\end{cases}$\label{alg:IDEExtension:WE}
		
		Set $r_{v_i} \coloneqq \min\Set{\psi_e(x_e)+r_w | e=v_iw \in \edgesFrom{v_i}\cap\setAEi[T]}$
	}
	
	Define a dynamic flow $g=(g^+_e,g^-_e)$ by setting 
		\[g^+_e(\theta) \coloneqq \begin{cases}f^+_e(\theta), &\text{ if } \theta < T\\x_e,&\text{ if } \theta \geq T\end{cases} \quad\text{ and }\quad g^-_e \coloneqq \edgeloading(g^+_e)\]
	
	Choose $\eps > 0$ such that $g$ is an IDE until $T+\eps$
	
	\KwRet{$g$ and $\eps$} 
\end{algorithm}

\begin{lemma}
	Let $f$ be an IDE until some time $T \in \IRnn$ with right-constant flow rates in a network with strictly positive travel times where $t$ is reachable from every node, and a right-constant network inflow rate. Then, we can use a constant extension of $f$ to an IDE until $T+\eps$ for some $\eps>0$ using \Cref{alg:IDEExtension}.
\end{lemma}

\begin{proof}
	We start by observing that the problem we have to solve in \cref{alg:IDEExtension:WE} is just a static Wardrop equilibrium problem. Thus, it always has a solution (\cf \Cref{cor:WardropEqExistence}) and, due to the simple structure of its cost functions ($\psi_e(.)+r_w$) we can also easily compute such a solution.
	
	Now, by its definition, it is already clear that $g$ is a Vickrey flow on all edges for all times and an IDE until~$T$ (as it is equal to~$f$ there). Hence, we only have to find some $\eps > 0$ such that $g$ satisfies strong flow conservation at nodes and the IDE property on $[T,T+\eps)$. To ensure the first one it suffices to choose $\eps$ small enough such that $u$ and all $g^-_e$ are constant on $[T,T+\eps)$. This is always possible since $u$ is right-constant and right-constant edge inflow rates induce right constant outflow rates (\cf \Cref{ex:VickreyFlowStructurePreserving}). To make sure the second property holds as well, we choose $\eps$ small enough such that we can apply \Cref{lemma:IDEExtensionCharacterization} to $[T,T+\eps)$. This, again, is possible because all flow rates and the network inflow rate are right-constant and all queue length and node label functions are continuous (implying that whenever an edge is inactive at some point~$\theta$ it is also inactive in some neighbourhood of~$\theta$). With this the correctness of \Cref{alg:IDEExtension} follows from \Cref{lemma:IDEExtensionCharacterization} by observing that we have $\lRi(T)=r_v$ for all nodes $v \in V$. 
\end{proof}

We will use the following \namecref{lemma:IDEFinitelyManyPhases} without proof:
\begin{lemma}[{\cite[Lemmas 5.12 and 5.13]{GrafThesis}}]\label{lemma:IDEFinitelyManyPhases}
	Let $\network$ be a dynamic flow network with strictly positive travel times, where $t$ is reachable from every node in the network and $u$ a right-constant network inflow rate. Then, there exists some $\alpha > 0$ such that for any $T \in \IRnn$, any IDE~$f$ until $T$ with right-constant flow rates and any $\alpha' \leq \alpha$ such that all edge-outflow rates as well as the network inflow rate are constant on $[T,T+\alpha')$, we can extend $f$ to an IDE until~$T+\alpha'$ using finitely many extensions with \Cref{alg:IDEExtension}.
\end{lemma}

\begin{theorem}\label{thm:IDEComputation}
	Let $\network$ be a dynamic flow network with strictly positive travel times, where $t$ is reachable from every node in the network and $u$ a right-constant network inflow rate. Then, we can compute an IDE up to any fixed time horizon~$T \in \IRnn$ in finite time.
\end{theorem}

\begin{proof}
	Let $T \in \IRnn$ be given. We then want to compute an IDE until~$T$ by starting with the zero flow (which is an IDE until time~$0$) and then repeatedly extending this IDE using \Cref{alg:IDEExtension}. This already proves the \namecref{thm:IDEComputation} provided that a finite number of those extensions suffices to reach~$T$. 
	
	To show this, we partition $[0,T]$ into (finitely many) intervals $[a_0,a_1),\dots,[a_{K-1},a_K]$ of length at most $\tauMin$ each. We can now prove via induction that we can reach any $a_i$ within finitely many extensions: Since the base case is trivial, we only need to show the induction step. We can, therefore, assume that we have already reached $a_i$ within finitely many extensions. This, in particular, implies that the current IDE~$f$ until~$a_i$ has right constant edge inflow rates. As this already completely determines the edge outflow rates during $[a_i,a_i+\tauMin]$ (\cf \Cref{lemma:EdgeLoadingUniqueness}), we can subdivide $[a_i,a_{i+1})$ into finitely many subintervals of length at most $\alpha$ each such that during each of those, all edge-outflow rates as well as the network inflow rate are constant. 
	
	Finally, we can apply \Cref{lemma:IDEFinitelyManyPhases} to each of these subintervals to show that they all can be covered by finitely many extensions. Hence, the same is true for the whole interval $[a_i,a_{i+1})$ and, in particular, for $a_K=T$ which proves the \namecref{thm:IDEComputation}.
\end{proof}

\subsection*{Notes}

This \namecref{sec:IDE} is based on \cite{GrafThesis}.