%!TEX root = ../dynamic-network-flows.tex
% !TeX spellcheck = en_GB

\section{The Vickrey Queuing Model}\label{sec:VickreyFlow}

\begin{definition}\label{def:Queue}\label{def:VickreyFlow}
	Let $e$ be an edge with free flow travel time $\tau_e \in \IRnn$ and capacity $\nu_e \in \IRp$ and $f_e=(f^+_e,f^-_e) \in \locInt^{\set{+,-}}$ a tuple of non-negative, locally integrable functions with $f^+_e(\theta)=f^-_e(\theta)=0$ for all $\theta < 0$. 
	
	We define the \emph{queue length}\dindex{queue} at time $\theta \in \IRnn$ by $\ql(\theta) \coloneqq F^+_e(\theta) - F^-_e(\theta+\tau_e)$ and the \emph{(expected) arrival time}\dindex{arrival time} by $\cexittime(\theta) \coloneqq \theta + \frac{\ql(\theta)}{\nu_e} + \tau_e$.
	
	We call $f_e$ a \emph{Vickrey flow}\dindex{Vickrey flow!on an edge} if
	\begin{enumerate}[label=(\roman*)]
		\item the queue operates at capacity, \ie
		\begin{align}\label[cons]{cons:QueueOperatesAtCapacity}
			f^-_e(\theta+\tau_e) = \begin{cases}\nu_e, &\text{if } \ql(\theta) > 0 \\
			\min\set{f^+_e(\theta),\nu_e},	&\text{else }\end{cases} \text{ for almost all } \theta \in \IRnn.
		\end{align}
		and 
		\item the queue starts empty, \ie $\ql(0) = 0$.		
	\end{enumerate}
\end{definition}

\begin{observation}
	The queue length is an absolutely continuous function (as difference of two absolutely continuous functions). It is non-negative at any time $\theta \in \IRnn$ if and only if $f_e$ satisfies weak flow conservation at that time. Moreover, if $f_e$ satisfies weak flow conservation at time~$0$, the queue starts empty.
\end{observation}


\subsection{Characterizations of Vickrey Flows}\label{sec:VickreyFlowCharacterization}

\begin{lemma}\label{lemma:CharacterizationsOfQueueOpAtCap}
	Let $f_e = (f^+_e,f^-_e)$ be  a tuple of non-negative, locally integrable functions with $f^+_e(\theta)=f^-_e(\theta)=0$ for all $\theta < 0$. Then, the following statements are equivalent:
	\begin{enumerate}[label=\alph*)]
		\item \label[thmpart]{lemma:CharacterizationsOfQueueOpAtCap:Def}
		$f_e$ is a Vickrey flow.
		\item \label[thmpart]{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv}
		The queue starts empty and satisfies 
		\begin{align}\label{eq:CharacterizationsOfQueueOpAtCap:QueueDeriv}
			\deriv{\ql}(\theta) = \begin{cases}
				f^+_e(\theta) - \nu_e, &\text{ if } \ql(\theta) > 0 \\
				\max\set{f^+_e(\theta) - \nu_e,0}, &\text{ else}
			\end{cases} \text{ for almost all } \theta \in \IRnn.
		\end{align}
		\item \label[thmpart]{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts}
		$f_e$ satisfies weak flow conservation, respects capacity of edge~$e$ and the cumulative outflow satisfies:
		\begin{align}\label{eq:CharacterizationsOfQueueOpAtCap:OutflowTwoParts}
			F^-_e(\theta+\tau_e) = F^+_e(\bar\theta) + (\theta-\bar\theta)\nu_e \text{ for all } \theta \in \IRnn,
		\end{align}
		where $\bar\theta \coloneqq \max\set{\theta'\leq\theta | \ql(\theta')=0}$ is the last time before $\theta$ with empty queue.
		\item \label[thmpart]{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}
		The cumulative edge outflow is completely determined by the cumulative edge inflow in the following way for every $\theta \in \IRnn$:
		\begin{align}\label{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined}
			F_e^-(\theta+\tau_e) = \min_{\theta' \leq \theta}\left(F^+_e(\theta')+(\theta-\theta')\nu_e\right)
		\end{align}
		\item \label[thmpart]{lemma:CharacterizationsOfQueueOpAtCap:Cum}
		$f_e$ satisfies weak flow conservation, respects the capacity and satisfies
		\begin{align}\label{eq:CharacterizationsOfQueueOpAtCap:Cum}
			F^-_e(\cexittime(\theta)) = F^+_e(\theta) \text{ for all } \theta \in \IRnn.
		\end{align}
	\end{enumerate}
\end{lemma}

\begin{proof}
	We prove this \namecref{lemma:CharacterizationsOfQueueOpAtCap} by showing \ref{lemma:CharacterizationsOfQueueOpAtCap:Def} $\iff$ \ref{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:Cum} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:Def}.
	
	\begin{description}
		\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv} $\iff$ \ref{lemma:CharacterizationsOfQueueOpAtCap:Def}:] Since both $F^+_e$ and $F^-_e$ are absolutely continuous they are differentiable with  $\deriv{F^+_e}(\theta) = f^+_e(\theta)$ and $\deriv{F^-_e}(\theta+\tau_e)=f^-_e(\theta+\tau_e)$ for almost all $\theta \in \IRnn$. For all those $\theta$ we then have
		\[\deriv{\ql}(\theta) = \deriv{F^+_e}(\theta) - \deriv{F^-_e}(\theta+\tau_e) = f^+_e(\theta) - f^-_e(\theta+\tau_e).\]
		This now immediately implies the equivalence of the constraints \eqref{cons:QueueOperatesAtCapacity} and \eqref{eq:CharacterizationsOfQueueOpAtCap:QueueDeriv}.
		
		\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts}:] We start by showing weak flow conservation or, equivalently, non-negativity of the queue length function. Since we have $\ql(0)=0$ (as the queue starts empty), this is equivalent to showing $\deriv{\ql}(\theta) \geq 0$ for almost all $\theta \in \IRnn$ with $\ql(\theta) \leq 0$ (\cf \Cref{lemma:ACFunctions-CharacterizationNonNegativity}). But this now follows directly from~\eqref{eq:CharacterizationsOfQueueOpAtCap:QueueDeriv}. Also from this, we get
			\[f^-_e(\theta+\tau_e) = \deriv{F^-_e(\theta+\tau_e)} = \deriv{F^+_e}(\theta)-\deriv{\ql}(\theta) = f^+_e(\theta)-\deriv{\ql}(\theta) \eqoverset{eq:CharacterizationsOfQueueOpAtCap:QueueDeriv}{\leq} \nu_e\]
		for almost all $\theta \in \IRnn$, \ie that the flow respects capacity. 
		
		Finally, in order to show~\eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowTwoParts} we first note that the time $\bar\theta \coloneqq \max\set{\theta' \leq \theta | \ql(\theta') = 0}$ is well defined for any fixed $\theta \in \IRnn$ since the queue length function is continuous and starts at $0$. As we have $\ql(\zeta) > 0$ for all $\zeta \in (\bar\theta,\theta)$ we then get
		\begin{align*}
			F^-_e(\theta+\tau_e) 
			&= F^+_e(\theta)-\ql(\theta)=F^+_e(\theta) - \ql(\bar\theta) - \int_{\bar\theta}^\theta \deriv{\ql}(\zeta)\diff\zeta \\
			&\eqoverset{eq:CharacterizationsOfQueueOpAtCap:QueueDeriv}{=} F^+_e(\theta)-0-\int_{\bar\theta}^\theta \left(f^+ _e(\zeta)-\nu_e\right)\diff\zeta =F^+_e(\theta)-\int_{\bar\theta}^\theta f^+_e(\zeta)\diff\zeta + (\theta-\bar\theta)\nu_e \\
			&= F^+_e(\bar\theta)+(\theta-\bar\theta)\nu_e.
		\end{align*}
		
		\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}:] First, note that the minimum is well defined since $F^+_e$ is continuous. From \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowTwoParts} we directly get
		\[F^-_e(\theta+\tau_e) = F^+_e(\bar\theta) + (\theta-\bar\theta)\nu_e \geq \min_{\theta' \leq \theta}\left(F^+_e(\theta') + (\theta-\theta')\nu_e\right).\]
		At the same time, respecting capacity and weak flow conservation imply that for every $\theta' \leq \theta$ we have
		\begin{align*}
			F^-_e(\theta+\tau_e) 
			&= F^-_e(\theta'+\tau_e) + \int_{\theta'}^\theta f^-_e(\zeta+\tau_e)\diff\zeta
			\leq  F^-_e(\theta'+\tau_e) + \int_{\theta'}^\theta \nu_e\diff\zeta \\
			&= F^+_e(\theta') -  \ql(\theta') + (\theta-\theta')\nu_e 
			\leq F^+_e(\theta') -  0 + (\theta-\theta')\nu_e.
		\end{align*}
		Together, this shows that \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined} holds.
		
		\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:Cum}:] Choosing $\theta' = \theta$ in \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined} gives us $F^-_e(\theta+\tau_e) \leq F^+_e(\theta)$, \ie weak flow conservation. As $F^-_e$ is absolutely continuous, it is differentiable for almost all $\theta$ and satisfies $\deriv{F^-_e}(\theta+\tau_e) = f^-_e(\theta+\tau_e)$. We will show that $f_e$ respects the capacity for all those $\theta$: So, fix such a time $\theta \in \IRnn$ and define $\hat{\theta} \coloneqq \arg\min_{\theta' \leq \theta}\left(F^+_e(\theta') + (\theta-\theta')\nu_e\right)$. Then, using \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined}, we have $F^-_e(\theta+\tau_e) = F^+_e(\hat{\theta}) + (\theta - \hat{\theta})\nu_e$ and 
		\[F^-_e(\theta + \varepsilon+\tau_e) =  \min_{\theta' \leq \theta+\varepsilon} \left(F^+_e(\theta')+(\theta + \varepsilon - \theta')\nu_e\right) \leq F^+_e(\hat{\theta})+(\theta + \varepsilon - \hat{\theta})\nu_e\]
		for any $\varepsilon > 0$. Thus, we get
		\begin{align*}
			f^-_e(\theta+\tau_e) 
			&= \deriv{F^-_e}(\theta+\tau_e) = \lim_{\varepsilon\searrow 0}\frac{1}{\varepsilon}\left(F^-_e(\theta+\varepsilon+\tau_e)-F^-_e(\theta+\tau_e)\right) \\
			&\leq \lim_{\varepsilon\searrow 0}\frac{1}{\varepsilon}\left(F^+_e(\hat{\theta}) + (\theta + \varepsilon - \hat{\theta})\nu_e - (F^+_e(\hat{\theta}) + (\theta - \hat{\theta})\nu_e)\right) = \lim_{\varepsilon\searrow 0}\frac{1}{\varepsilon}\varepsilon\nu_e = \nu_e,
		\end{align*}
		\ie the flow respects the capacity for all $\theta \in \IRnn$ with $\deriv{F^-_e}(\theta+\tau_e) = f^-_e(\theta+\tau_e)$ which is almost all.
		
		Finally, in order to show \eqref{eq:CharacterizationsOfQueueOpAtCap:Cum}, take any $\theta \in \IRnn$ and observe that for any $\theta' \in [\theta,\theta+\frac{\ql(\theta)}{\nu_e}]$ we have
		\begin{align*}
			F^+_e(\theta') + (\theta-\theta')\nu_e 
			&\geq F^+_e(\theta)  + (\theta-\theta')\nu_e = F^-_e(\theta+\tau_e) + \ql(\theta)  + (\theta-\theta')\nu_e \\
			&\geq F^-_e(\theta+\tau_e) + \ql(\theta) - \tfrac{\ql(\theta)}{\nu_e}\nu_e = F^-_e(\theta+\tau_e).
		\end{align*}
		Together with \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined} this gives us
		\begin{align}\label{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined:Extended}
			F^-_e(\theta+\tau_e) = \min_{\theta' \leq \theta+\frac{\ql(\theta)}{\nu_e}}\left(F^+_e(\theta') + (\theta-\theta')\nu_e\right).
		\end{align}
		Using \eqref{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined} again, now for $\theta + \frac{\ql(\theta)}{\nu_e}$, we obtain \eqref{eq:CharacterizationsOfQueueOpAtCap:Cum} as follows:
		\begin{align*}
			F^-_e(\cexittime(\theta)) &= F^-_e\left(\theta+\tfrac{\ql(\theta)}{\nu_e} + \tau_e\right)
			\eqoverset{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined}{=} \min_{\theta' \leq \theta+\frac{\ql(\theta)}{\nu_e}}\left(F^+_e(\theta') + \left(\theta+\tfrac{\ql(\theta)}{\nu_e}-\theta'\right)\nu_e\right) \\
			&=\min_{\theta' \leq \theta+\frac{\ql(\theta)}{\nu_e}}\left(F^+_e(\theta') + (\theta-\theta')\nu_e\right) + \ql(\theta) \\
			&\eqoverset{eq:CharacterizationsOfQueueOpAtCap:OutflowDetermined:Extended}{=} F^-_e(\theta+\tau_e) + \ql(\theta) 
			= F^+_e(\theta).
		\end{align*}
		
		\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:Cum} $\implies$ \ref{lemma:CharacterizationsOfQueueOpAtCap:Def}:] Define $Q^+ \coloneqq \set{\theta \in \IRnn | \ql(\theta) > 0}$ as the set of all times where the queue is non-empty. We now have to show two things: 
		\begin{enumerate}[label=(\roman*)]
			\item $f^-_e(\theta+\tau_e) = \nu_e$ for almost all $\theta \in Q^+$. \label{lemma:CharacterizationsOfQueueOpAtCap:ProofOfDefQplus}
			\item $f^-_e(\theta+\tau_e) = \min\set{f^+_e(\theta),\nu_e}$ for almost all $\theta \in \IRnn \setminus Q^+$. \label{lemma:CharacterizationsOfQueueOpAtCap:ProofOfDefQzero}
		\end{enumerate}
		
		\begin{description}
			\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:ProofOfDefQplus}:] Consider any time $\theta \in Q^+$. Then, we have 
				\begin{align*}
					\ql(\theta) 
						&= F^+_e(\theta)-F^-_e(\theta+\tau_e) \roverset{lemma:CharacterizationsOfQueueOpAtCap:Cum}{=} F^-_e(\cexittime(\theta)) - F^-_e(\theta+\tau_e) \\
						&= \int_\theta^{\theta+\frac{\ql(\theta)}{\nu_e}}\underbrace{f^-_e(\zeta+\tau_e)}_{\leq \nu_e}\diff\zeta \leq \nu_e\cdot\frac{\ql(\theta)}{\nu_e} = \ql(\theta).
				\end{align*}
				Thus, we have $f^-_e(\zeta+\tau_e)=\nu_e$ for almost all $\zeta \in [\theta,\theta+\frac{\ql(\theta)}{\nu_e}]$. Using \Cref{lemma:LocIntFunctions-VanishingOnIntervals} this shows that the same holds for almost all $\zeta \in Q^+$.		
			
			\item[\ref{lemma:CharacterizationsOfQueueOpAtCap:ProofOfDefQzero}:] Here, we can apply \Cref{lemma:ACFunctions-CharacterizationNonNegativity} to the queue length function (since we know that this function is absolutely continuous, the queue starts empty and is non-negative due to weak flow conservation). Thus, for almost all $\theta \in \IRnn \setminus Q^+$ we have
				\[0 = \deriv{\ql}(\theta) = \deriv{F^+_e}(\theta) - \deriv{F^-_e}(\theta+\tau_e) = f^+_e(\theta) - f^-_e(\theta+\tau_e).\]
			Together with the fact that the flow respects the edge capacity this implies $f^-_e(\theta+\tau_e) = f^+_e(\theta) = \min\set{f^+_e(\theta),\nu_e}$ for almost all such $\theta$.
		\end{description}
		This shows that \eqref{cons:QueueOperatesAtCapacity} holds. Since weak flow conservation also implies $\ql(0)=0$, this concludes our proof.
		\qedhere
	\end{description}
\end{proof}


\subsection{Properties of Vickrey Flows}\label{sec:VickreyFlowProperties}

\begin{lemma}\label{lemma:EdgeLoadingExistence}
	For any non-negative, locally integrable function $f^+_e$ with $f^+_e(\theta)=0$ for all $\theta < 0$, there exists a locally integrable function $f^-_e$ with $f^-_e(\theta)=0$ for all $\theta < 0$ such that $(f^+_e,f^-_e)$ is a Vickrey flow.
\end{lemma}

\begin{proof}
	We start by defining a function 
		\[G: \IR \to \IR, \theta \mapsto \min_{\theta' \leq \theta-\tau_e}\big(F^+_e(\theta')+(\theta-\tau_e-\theta')\nu_e\big).\]
	\begin{claim}
		$G$ is well-defined, non-negative, non-decreasing and absolutely continuous and satisfies $G(\theta)=0$ for all $\theta \leq \tau_e$.
	\end{claim}

	\begin{proofClaim}
		We start by observing that we have $F^+_e(\theta')+(\theta-\tau_e-\theta')\nu_e \xrightarrow{\theta' \to -\infty} \infty$. Together with the continuity of $F^+_e$ this proves that $G$ is well defined. Moreover, we have $F^+_e(\theta')+(\theta-\tau_e-\theta')\nu_e\geq 0$ for any $\theta' \leq \theta-\tau_e$ and, therefore, $G$ is non-negative. 
		
		For any $\theta \leq \tau_e$ we additionally have $F^+_e(\theta-\tau_e)+(\theta-\tau_e-(\theta-\tau_e))\nu_e=0$, which implies $G(\theta)=0$ for all such~$\theta$.
		
		To show that $G$ is non-decreasing pick any $a \leq b$. Then, we have
			\begin{align*}
				G(b) 
					&= \min_{\theta' \leq b-\tau_e}\big(F^+_e(\theta')+(b-\tau_e-\theta')\nu_e\big) \\
					&= \min\Set{\min_{\theta' \leq a-\tau_e}\big(F^+_e(\theta')+(b-\tau_e-\theta')\nu_e\big), \min_{\theta'\in[a-\tau_e,b-\tau_e]}\big(F^+_e(\theta')+(b-\tau_e-\theta')\nu_e\big)} \\
					&\geq \min\Set{\min_{\theta' \leq a-\tau_e}\big(F^+_e(\theta')+(a-\tau_e-\theta')\nu_e\big), F^+_e(a-\tau_e)+(a-\tau_e-(a-\tau_e))\nu_e} \\
					&=\min_{\theta' \leq a-\tau_e}\big(F^+_e(\theta')+(a-\tau_e-\theta')\nu_e\big) = G(a).
			\end{align*}
		
		Finally, we show Lipschitz-continuity with Lipschitz constant $\nu_e$ as follows: For $a \leq b$ define $\theta'_a \coloneqq \arg\min_{\theta'\leq a-\tau_e}\big(F^+_e(\theta')+(a-\tau_e-\theta')\nu_e\big)$. Then, we have
			\begin{align*}
				\abs{G(b)-G(a)} 
					&= G(b)-G(a) \\
					&= \min_{\theta'\leq b-\tau_e}\Big(F^+_e(\theta')+(b-\tau_e-\theta')\nu_e\Big)-\Big(F^+_e(\theta'_a)+(a-\tau_e-\theta'_a)\nu_e\Big) \\
					&\leq \Big(F^+_e(\theta'_a)+(b-\tau_e-\theta'_a)\nu_e\Big)-\Big(F^+_e(\theta'_a)+(a-\tau_e-\theta'_a)\nu_e\Big) \\
					&=(b-a)\nu_e = \nu_e\cdot\abs{b-a}.
			\end{align*}
		Since Lipschitz-continuity implies absolute continuity, this concludes the proof of the claim.
	\end{proofClaim}
	We can now define $f^-_e: \IR \to \IR, \theta \mapsto \deriv{G}(\theta)$ which is
		\begin{itemize}
			\item well defined almost everywhere since $G$ is absolutely continuous,
			\item non-negative since $G$ is non-decreasing,
			\item satisfies $f^-_e(\theta)=0$ for all $\theta <0$ since $\restr{G}{(-\infty,\tau_e]} \equiv 0$ and
			\item has $F^-_e = G$ by the fundamental theorem of calculus.
		\end{itemize}
	Hence, $(f^+_e,f^-_e)$ is a Vickrey flow by \Cref{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}.
\end{proof}

\begin{lemma}\label{lemma:EdgeLoadingUniqueness}
	Let $f_e$ and $g_e$ be two Vickrey flows and $\bar\theta \in \IRnn$ some time with $f^+_e(\theta) = g^+_e(\theta)$ for almost all $\theta \leq \bar\theta$. Then we have $f^-_e(\theta) = g^-_e(\theta)$ for almost all $\theta \leq \cexittime^f(\bar\theta)$.
\end{lemma}

\begin{proof}
	For any $\theta \leq \bar\theta+\tau_e$ we have
		\begin{align*}
			F^-_e(\theta) &\Croverset{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}{=} \min_{\theta' \leq \theta-\tau_e}\Big(F^+_e(\theta')+(\theta-\tau_e-\theta')\nu_e\Big) \\
				&\toverset{\phantom{\Crefshort{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}}}{=} \min_{\theta' \leq \theta-\tau_e}\Big(G^+_e(\theta')+(\theta-\tau_e-\theta')\nu_e\Big)\Croverset{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}{=} G^-_e(\theta)
		\end{align*}
	which implies $f^-_e(\theta)=g^-_e(\theta)$ for almost all $\theta \leq \bar\theta+\tau_e$ as well as $\ql^f(\bar\theta)=\ql^g(\bar\theta)$ and, therefore, $\cexittime^f(\bar\theta)=\cexittime^g(\bar\theta)$.
	
	Now, for any $\theta \in [\bar\theta+\tau_e,\cexittime^f(\bar\theta))$ we have 
		\[\ql^f(\theta-\tau_e) = \ql^f(\bar\theta-\tau_e)+\int_{\bar\theta-\tau_e}^{\theta-\tau_e}f^+_e(\zeta)\diff\zeta-\int_{\bar\theta-\tau_e}^{\theta-\tau_e}f^-_e(\zeta)\diff\zeta \geq \ql^f(\bar\theta-\tau_e)+0-(\bar\theta-\theta)\nu_e > 0\]
	and, analogously, $\ql^g(\theta-\tau_e) > 0$. As both $f_e$ and $g_e$ are Vickrey flows, this implies
		\[f^-_e(\theta) = \nu_e = g^-_e(\theta)\]
	for almost all $\theta  \in [\bar\theta+\tau_e,\cexittime^f(\bar\theta))$ which concludes the proof.
\end{proof}

\begin{definition}\label{def:EdgeLoading}
	Let $e$ be an edge with travel time $\tau_e \in \IRnn$ and capacity $\nu_e \in \IRp$. We define the \emph{(Vickrey-)edge-loading mapping}\dindex{edge loading} by
		\begin{align*}
			\edgeloading: \set{f^+_e \in \locInt | \supp(f^+_e) \subseteq \IRnn, f^+_e \geq 0} &\to \locInt, \\
			f^+_e &\mapsto f^-_e \text{ s.th. } (f^+_e,f^-_e) \text{ is a Vickrey flow}.
		\end{align*}
\end{definition}

\begin{lemma}\label{lemma:EdgeLoadingContinuous}
	For any finite time horizon~$T$, the restricted Vickrey edge-loading mapping
		\begin{align*}
			\edgeloading^T: \set{f^+_e \in \pInt[2] | \supp(f^+_e) \subseteq [0,T], f^+_e \geq 0} &\to \locInt, \\
			f^+_e &\mapsto \edgeloading(f^+_e).
		\end{align*}
	is weak-weak sequentially continuous
\end{lemma}

\begin{proof}
	We start by showing that integrating non-negative functions is a weak-strong sequentially continuous mapping:
	\begin{claim}\label{claim:IntegrationIsWeakStrongContinuous}
		The mapping
			\[\set{f \in \locInt | \supp(f) \subseteq [0,T], f \geq 0} \to C(\IR), f \mapsto \Big(\IR \to \IRnn, \theta \mapsto \int_0^\theta f(\zeta)\diff\zeta\Big)\]
		is weak-strong sequentially continuous.
	\end{claim}
	
	\begin{proofClaim}
		Let $f_n \convto[w] f$ be a weakly convergent sequence and $F_n(\theta) \coloneqq \int_0^\theta f_n(\zeta)\diff\zeta$ as well as $F(\theta) \coloneqq \int_0^\theta f(\zeta)\diff\zeta$. We have to show that $F_n \convto F$ \wrt the norm $\norm{.}_\infty$ on $C([0,T])$. 
		
		So, let $\eps > 0$. Since $F$ is absolutely continuous there exists some $\delta > 0$ such that we have $\abs{F(\theta)-F(\theta')} \leq \frac{\eps}{2}$ for all $\abs{\theta-\theta'}\leq\delta$. We choose $K \in \INs$ such that $\frac{1}{K} \leq \delta$. As $f_n$ converges weakly to $f$ we have for any $k \in \set{0,1,\dots,\ceil{KT}}$ some $N_k \in \INo$ such that
			\begin{align*}
				\abs{F(\tfrac{k}{K})-F_n(\tfrac{k}{K})} 
					&= \Abs{\int_0^{\frac{k}{K}}f(\zeta)\diff\zeta-\int_0^{\frac{k}{K}}f_n(\zeta)\diff\zeta} \\
					&= \Abs{\int_\IR f(\zeta)\CharF[{[0,\frac{k}{K}]}](\zeta)\diff\zeta-\int_\IR f_n(\zeta)\CharF[{[0,\frac{k}{K}]}](\zeta)\diff\zeta} \leq \frac{\eps}{2}
			\end{align*}
		for all $n \geq N_k$. Then, for any $n \geq \max\set{N_k | k=0,1,\dots,\ceil{KT}}$ and $\theta \in [0,T]$ we can choose some $k \in \set{0,1,\dots,\ceil{KT}}$ with $\theta \in [\frac{k}{K},\frac{k+1}{K}]$ and have
			\begin{align*}
				&\abs{F(\theta)-F_n(\theta)} 
					\symoverset{1}{\leq} \max\set{\abs{F(\tfrac{k}{K})-F_n(\tfrac{k+1}{K})},\abs{F(\tfrac{k+1}{K})-F_n(\tfrac{k}{K})}} \\
					&\quad\leq \max\set{\abs{F(\tfrac{k}{K})-F(\tfrac{k+1}{K})}+\abs{F(\tfrac{k+1}{K})-F_n(\tfrac{k+1}{K})},\abs{F(\tfrac{k+1}{K})-F(\tfrac{k}{K})}+\abs{F(\tfrac{k}{K})-F_n(\tfrac{k}{K})}} \\
					&\quad\leq \max\set{\tfrac{\eps}{2}+\tfrac{\eps}{2},\tfrac{\eps}{2}+\tfrac{\eps}{2}} = \eps,
			\end{align*}
		where \refsym{1} holds because both $F$ and $F_n$ are non-decreasing. Since all~$F_n$ as well as~$F$ are constant outside of $[0,T]$ this result immediately extends to the whole of~$\IR$, \ie $F_n$ converges to~$F$ \wrt to~$\norm{.}_\infty$.
	\end{proofClaim}
	
	Now, let $(f^{n,+}_e)$ be a weakly convergent sequence in 
		\[\set{f^+_e \in \pInt[2] | \supp(f^+_e) \subseteq [0,T], f^+_e \geq 0}\]
	with (weak) limit point~$f^+_e$. Moreover, let $f^{n,-}_e \coloneqq \edgeloading^T(f^{n,+}_e)$ and $f^-_e \coloneqq \edgeloading^T(f^+_e)$. We now have to show that $f^{n,-}_e$ converges weakly to~$f^-_e$.
	
	\begin{claim}\label{claim:OutFlowRatesForWeaklyConvInflowRatesAreBounded}
		There exists some $M \in \IRnn$ such that $f^{n,-}_e \in \ballClosed[M]{0} \subseteq \locInt$ for all $n \in \INo$.
	\end{claim}
	
	\begin{proofClaim}
		Since $f^{n,+}_e \convto[w] f^+_e$ in $\pInt[2]$, we know that
			\[\int_\IR f^{n,+}_e(\zeta)\diff\zeta = \int_\IR f^{n,+}_e(\zeta)\CharF[{[0,T]}](\zeta)\diff\zeta \convto \int_\IR f^+_e(\zeta)\CharF[{[0,T]}](\zeta)\diff\zeta \leq \int_\IR \max\set{1,\abs{f^+_e(\zeta)}^2}\diff\zeta.\]
		Hence, there exists some $M \in \IRnn$ with $\int_\IR f^{n,+}_e(\zeta)\diff\zeta \leq M$ for all $n \in \INo$ and get
			\[\int_\IR \abs{f^{n,-}_e(\zeta)}\diff\zeta = \int_\IR f^{n,-}_e(\zeta)\diff\zeta \leq \int_\IR f^{n,+}_e(\zeta)\diff\zeta \leq M. \qedhere\]
	\end{proofClaim}
	
	With \Cref{claim:OutFlowRatesForWeaklyConvInflowRatesAreBounded} as well as \Cref{thm:UniBallWeaklyClosedIffReflexive} we now know that the sequence~$(f^{n,-}_e)$ is contained in a weakly compact set which, by \Cref{thm:WeakCompactnessIfWeakSequentialCompactness}, is also weakly sequentially compact. Hence, it suffices to show that every convergent subsequence of it converges to $f^-_e$ (\cf \Cref{lemma:ConvergenceInCompactSpace}). 
	
	So, lets assume \wlg that $f^{n,-}_e$ itself converges weakly to some $f^{\ast,-}_e$. Then, we have $f^n_e \coloneqq (f^{n,+}_e,f^{n,-}_e) \convto[w] (f^+_e,f^{\ast,-}_e) \eqqcolon (f^{\ast,+}_e,f^{\ast,-}_e) \eqqcolon f^\ast_e$ and, therefore $F^{n,+}_e \convto F^{\ast,+}$, $F^{n,-}_e \convto F^{\ast,-}$, $\ql^{f^n_e} \convto \ql^{f^\ast_e}$ and $\cexittime^{f^n_e} \convto \cexittime^{f^\ast_e}$ by \Cref{claim:IntegrationIsWeakStrongContinuous} (all \wrt $\norm{.}_\infty)$). It is now easy to check that this implies that $f^\ast_e$ is non-negative, respects capacity and satisfies weak flow conservation (since this is true for all $f^n_e$). Moreover, we get from \Cref{lemma:PointwiseConvergenceOfConcOfUniformlyConvergentFunctions} that $F^{n,-}_e\circ\cexittime^{f^n_e}$ converges pointwise to $F^{\ast,-}\circ\cexittime^{f^\ast_e}$. Therefore, $F^{n,-}_e(\cexittime^{f^n_e}(\theta))=F^{n,+}_e(\theta)$ for all $\theta$ and $n$ (\Cref{lemma:CharacterizationsOfQueueOpAtCap:Cum}) implies that $F^{\ast,-}_e(\cexittime^{f^\ast_e}(\theta))=F^{\ast,+}_e(\theta)$ holds for all $\theta$ as well. Hence, $f^*_e=(f^{\ast,+}_e,f^{\ast,-}_e)$ is a Vickrey flow by \Cref{lemma:CharacterizationsOfQueueOpAtCap}. As $f_e=(f^+_e,f^-_e)$ is a Vickrey flow with the same inflow rate, the uniqueness of the corresponding outflow rate (\Cref{lemma:EdgeLoadingUniqueness}) implies that we have $f^{\ast,-}_e(\theta)=f^-_e(\theta)$ for almost all $\theta \in \IR$. Hence, $f^{n,-}_e$ converges weakly to $f^-_e$ as desired.
\end{proof}

\begin{exercise}\label{ex:VickreyFlowStructurePreserving}
	Let $f_e$ be a Vickrey flow and $[a,b]$ some interval such that $f^+_e$ is constant during this interval. Show that then $f^-_e$ is piecewise-constant on $[\cexittime(a),\cexittime(b)]$ with at most one break point (up to changes on a set of measure zero).
\end{exercise}

\begin{exercise}\label{ex:VickreyFlowMonotonicity}
	Let $f_e$ and $g_e$ be two Vickrey flows on the same edge and $T \geq 0$ some time horizon. Prove the following statement:
		\[\Big(\forall \theta \leq T: F^+_e(\theta) \leq G^+_e(\theta)\Big) \implies \Big(\forall \theta \leq T: F^-_e(\theta+\tau_e) \leq G^-_e(\theta+\tau_e)\Big)\]
\end{exercise}

\begin{exercise}\label{ex:VickreyFlowNoIdleing}
	Let $f_e$ be a Vickrey flow and $\theta \in \IR$ some time with $F^-_e(\theta) < F^+_e(\theta)$ (\ie there is some flow on edge~$e$ at time~$\theta$). Let $x \in [0,F^+_e(\theta)-F^-_e(\theta)]$ and $\theta_x \coloneqq \theta + \tau_e + \frac{x}{\nu_e}$. Show that we then have 
		\[F^-_e(\theta_x)- F^-_e(\theta) \geq x.\]
\end{exercise}


\subsection{Network-Loading}\label{sec:VickreyFlowNetworkloading}

\begin{definition}\label{def:PathArrivalTime}\dindex{Vickrey flow}
	Let $\network$ be a dynamic flow network and $f$ an (edge-based) dynamic flow in $\network$. We call $f$ a \emph{Vickrey flow} if it is a Vickrey flow on every edge of the network and satisfies strong flow conservation at all nodes.
	
	We then define for any path~$p$ the arrival time at the end of path~$P$ when entering it at time~$\theta$ by
		\[\cexittime[p](\theta) \coloneqq \begin{cases}
			\theta,							&\text{ if } p=() \\
			\cexittime(\cexittime[p'](\theta)),		&\text{ if } p=p',e
		\end{cases}.\]
\end{definition}

\begin{definition}\label{def:CorrespondingVickreyFlow}
	Let $\network$ be a dynamic flow network, $f$ a Vickrey flow and $(f_p)$ a path-based dynamic flow in $\network$. We say that $f$ is a \emph{corresponding Vickrey flow}\dindex{Vickrey flow!corresponding} to~$(f_p)$ if there exist locally integrable functions $f^+_{p,j}, f^-_{p,j}: \IR \to \IRnn$ for $j \in [\abs{p}]$ for every \stpath{} $p \in \PathSet$ such that
		\begin{align}\label{eq:NetworkLoadingPathEdgeFunctions}
			f^+_{p,j}(\theta) = \begin{cases}
				f_p(\theta),			&\text{ if } j=1 \\
				f^-_{p,j-1}(\theta),	&\text{ else}	
			\end{cases}
				\quad\text{ and }\quad
			\int_0^{\cexittime[{p[j]}](\theta)}f^-_{p,j}(\zeta)\diff\zeta = \int_0^\theta f^+_{p,j}(\zeta)\diff\zeta
		\end{align}
	for all $p \in \PathSet$, $j \in [\abs{p}]$ and almost all $\theta \in \IR$ satisfying
		\begin{align}\label{eq:NetworkLoadingCompatibleWithEdgeFlow}
			\sum_{\substack{p \in \PathSet:\\\exists j: p[j]=e}}f^+_{p,j}=f^+_e \text{ and } \sum_{\substack{p \in \PathSet:\\\exists j: p[j]=e}}f^-_{p,j}=f^-_e \text{ for all } e \in E,
		\end{align}
	where we denote by~$p[j]$ the $j$-th edge on path~$p$
\end{definition}

\begin{lemma}\label{lemma:NetworkLoaidngExistenceUnqiueness}
	Let $\network$ be a dynamic flow network with strictly positive travel times $\tau_e \in \IRp$ and $(f_p)$ some path-based dynamic flow. Then, there exists a unique\footnote{up to changes on a set of measure zero} corresponding Vickrey flow~$f=(f^+_e,f^-_e)$.
\end{lemma}

\begin{proof}
	We will show that for any $K \in \INo$ there are uniquely determined flow rates $f^+_e,f^-_e, f^+_{p,j}$ and $f^-_{p,j}$ satisfying the properties of a corresponding Vickrey flow until time $K\cdot\tauMin$ where $\tauMin \coloneqq \min\set{\tau_e | e \in E}>0$ denotes the minimal edge travel time in~$\network$.\footnote{Note: We are (deliberately) a bit imprecise here as we never formally defined what a Vickrey flow until some fixed time horizon is. Moreover, we will freely apply our lemmas about (full) Vickrey flows for those partial Vickrey flows as well. Intuitively, it should be fairly obvious that they hold here too (in the way we make use of them here), but making this more formal requires some technical intricacies we want to avoid here.}
	\begin{proofbyinduction}
		\basecase{$K=0$} Until time $\theta=0$ all flow rates must be zero. Thus, there is nothing to prove here.
		\inductionstep{$K \to K+1$} By induction we know that all flow rates are already (uniquely) determined until time~$K\tauMin$. Hence, \Cref{lemma:EdgeLoadingExistence,lemma:EdgeLoadingUniqueness} imply that there exist unique edge-outflow rates $f^-_e$ until time $K\tauMin + \tau_e \geq (K+1)\tauMin$ for any edge $e \in E$. Thus, $\cexittime(K\tauMin) \geq K\tauMin+\tau_e \geq (K+1)\tauMin$ is already fixed as well. With this, we can (uniquely) extend the functions $f^-_{p,j}$ on $[K\tauMin,(K+1)\tauMin]$ using~\eqref{eq:NetworkLoadingPathEdgeFunctions}. The equality $\sum_{p \in \PathSet:\,\exists j: p[j]=e}f^-_{p,j}=f^-_e$ on this new interval then follows directly from \Cref{lemma:CharacterizationsOfQueueOpAtCap:Cum} together with the fact that the corresponding equality for the edge inflow rates already holds on $[0,K\tauMin]$. Finally, we can now use \eqref{eq:NetworkLoadingPathEdgeFunctions} and \eqref{eq:NetworkLoadingCompatibleWithEdgeFlow}, respectively, to (uniquely) determine the inflow functions $f^+_{p,j}$ and $f^+_e$ on the new interval. \qedhere
	\end{proofbyinduction}
\end{proof}

\begin{definition}\label{def:NetworkLoading}
	Let $\network$ be a dynamic flow network with strictly positive travel times $\tau_e \in \IRp$. We define the \emph{(Vickrey-)network-loading mapping}\dindex{network loading} by
	\begin{align*}
		\networkloading: \set{(f_p) \in \locInt^\PathSet | \supp(f_p) \subseteq \IRnn, f_p \geq 0} &\to \locInt^{E \times \set{\pm}}, \\
		(f_p) \hspace{12em}&\hspace{-12em}\mapsto (f^+_e, f^-_e) \text{ s.th. } (f^+_e,f^-_e) \text{ is a Vickrey flow corresponding to } (f_p).
	\end{align*}
\end{definition}

\begin{lemma}\label{lemma:NetworkLoadingContinuous}
	For any finite time horizon~$T\geq 0$ and flow volume bound~$M \geq 0$ the restricted Vickrey network-loading mapping
	\begin{align*}
		\networkloading^{T,M}: \set{(f_p) \in \pInt[2]^\PathSet | \supp(f_p) \subseteq [0,T], f_p \geq 0, \sum_{p \in \PathSet}\int_0^T f_p(\zeta)\diff\zeta \leq M} &\to \locInt^{E \times \set{\pm}}, \\
		(f_p) &\mapsto \networkloading((f_p)).
	\end{align*}
	is weak-weak sequentially continuous.
\end{lemma}

\begin{proofsketch}
	This \namecref{lemma:NetworkLoadingContinuous} can be shown in essentially the same way as the corresponding \namecref{lemma:EdgeLoadingContinuous} for the edge-loading mapping (\ie \Cref{lemma:EdgeLoadingContinuous}) once we have the following claim:
	\begin{claim}\label{claim:NetworkLoadingHasBoundedSupport}
		There exists some constant $\tilde T$ such that we have $\supp(f^+_e), \supp(f^-_e) \subseteq [0,\tilde T]$ for all $(f^+_e,f^-_e) \in \im(\networkloading^{T,M})$.
	\end{claim}
	
	\begin{proofClaim}
		Let $h \in \dom(\networkloading^{T,M})$ be any path-based flow and $f$ the corresponding Vickrey flow. We then have $\int_\IR f^+_{p,j}(\zeta)\diff\zeta = \int_\IR f_p(\zeta)\diff\zeta$ for all $p \in \PathSet$ and $j \in [\abs{p}]$ (using~\eqref{eq:NetworkLoadingPathEdgeFunctions} and induction on~$j$). From this we get $\ql(\theta) \leq F^+_e(\theta) \leq M$ for all edges~$e$ and times~$\theta \in \IR$ (using~\eqref{eq:NetworkLoadingCompatibleWithEdgeFlow}). Hence, we have $\cexittime[p](T) \leq T+\tau_p+\frac{\abs{p}\cdot M}{\nuMin}$ for any \stpath{} $p$, where we use $\nuMin \coloneqq \min\set{\nu_e | e \in E}$ to denote the minimal edge capacity in the network. Together with~\eqref{eq:NetworkLoadingPathEdgeFunctions} this implies
			\[\int_0^{T+\tau_p+\frac{\abs{p}\cdot M}{\nuMin}}f^-_{p,\abs{p}}(\zeta)\diff\zeta=\int_0^T f_p(\zeta)\diff\zeta = \int_\IR f_p(\zeta)\diff\zeta\]
		for all paths~$p$ and, therefore, $f^+_{p,j}(\theta) = f^-_{p,j}(\theta) = f^+_e(\theta) = f^-_e(\theta) = 0$ for all $p \in \PathSet$, $j \in [\abs{p}]$, $e \in E$ and almost all $\theta \geq \tilde T\coloneqq \max\set{T+\tau_p+\frac{\abs{p}\cdot M}{\nuMin} | p \in \PathSet}$.
	\end{proofClaim}
	
	Now, let $(f_p^n) \in \dom(\networkloading^{T,M})$ be a weakly convergent sequence with $f_p^n \to f_p$ and let $(f^{n,+}_e,f^{n,-}_e) \coloneqq \networkloading^{T,M}((f^n_p))$ and $(f^+_e,f^-_e) \coloneqq \networkloading^{T,M}((f_p))$ the corresponding Vickrey flows as well as $f^{n,\pm}_{p,j}$ and $f^{\pm}_{p,j}$ the witnesses for that. Since the $f^n_p$ form a converging sequence, we can see that all the corresponding flow rates are contained in some weakly compact set. Hence, we may again assume \wlg that the sequences of edge flow rates are weakly convergent and use \Cref{claim:NetworkLoadingHasBoundedSupport,claim:IntegrationIsWeakStrongContinuous} to obtain that the corresponding sequences of cumulative flow functions and arrival time functions are uniformly convergent. From this we get that the limit point of the sequence is a Vickrey flow corresponding to~$(f_p)$. Therefore, the uniqueness of corresponding Vickrey flows (\Cref{lemma:NetworkLoaidngExistenceUnqiueness}) shows that this limit point is~$(f^+_e,f^-_e)$.
\end{proofsketch}

\subsection*{Notes}

Vickrey flows are named after \citeauthor{VickreyQueuingModel} who in \cite{VickreyQueuingModel} first proposed the deterministic queuing model as a way of modelling congestion. For the first two \namecrefs{sec:VickreyFlowCharacterization} of this \namecref{sec:VickreyFlow} we follow the presentation in \cite[Chapter 3]{GrafThesis} which compiles several well known results about Vickrey flows (\cf \eg \cite{CCLDynEquil,SeringThesis}). For the third \namecref{sec:VickreyFlowNetworkloading} we follow \cite[Sections 5.2 and 5.3]{CCLDynEquil}.