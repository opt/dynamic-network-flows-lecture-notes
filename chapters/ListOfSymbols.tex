%!TEX root = ../dynamic-network-flows.tex
% !TeX spellcheck = en_GB

\section{List of Symbols and Notation}

{
	\newcommand{\losEntry}[3]{#1 & #2 \ifthenelse{\equal{#3}{}}{}{(\cf \Crefshort{#3})} \\}
	\renewcommand{\arraystretch}{1.2}
	
	\begin{longtable}{p{4.5cm}p{9.6cm}}
		Symbol				& Description \\\hline\hline
		
		% General:
		\losEntry{$\INo$}{natural numbers including $0$}{}
		\losEntry{$\INs$}{natural numbers excluding $0$}{}
		\losEntry{$[n]$}{set of integer numbers $1,2,\dots,n$; $[0] = \emptyset$}{}
		\losEntry{$\IR$}{the real numbers}{}
		\losEntry{$\IRnn$}{the non-neative real numbers}{}
		\losEntry{$\CharF[A]$}{characteristic vector/function of the set $A$}{sec:Preliminaries}
		\losEntry{$\supp(x)$}{support of vector/function/flow $x$}{sec:Preliminaries,def:StaticFlow}
		\losEntry{$\dom f$}{domain of a function $f: A \to B$, \ie $\dom f = A$}{}
		\losEntry{$\im f$}{image of a function $f:A \to B$, \ie $\im f = f(A) \subseteq B$}{}
		\losEntry{$\deriv{f}$}{derivative of a function $f: \IR \to \IR$}{}
		\losEntry{$\rDeriv{f}$}{right-side derivative of a function $f: \IR \to \IR$}{}	
		
		%%
		\losEntry{$\pInt[J]$}{the space of $p$-integrable function on some set $J \subseteq \IR$}{def:Lp}
		\losEntry{$\norm{.}_p$}{the $p$-norm $\norm{f}_p \coloneqq \big(\int_J\abs{f(\theta)}^p\diff\theta\big)^{\nicefrac{1}{p}}$ on $\pInt[J]$}{def:Lp}
		\losEntry{$C([a,b])$}{the space of continuous functions on $[a,b]$ (with supremum-norm $\norm{.}_\infty$)}{}
		\losEntry{$\langle.,.\rangle$}{scalar product (on $\IR^d\times\IR^d$) or dual pairing (on $\pInt[p][J]^d\times\pInt[q][J]^d$)}{def:DualPairing}
					
		% Graphs/networks:
		\hline Graphs/Networks & \\\hline
		\losEntry{$G=(V,E)$}{a (simple) directed graph with node set $V$ and edge set $E$}{}
		\losEntry{$\edgesFrom{W}/\edgesFrom{w}$}{edges leaving set $W$/node $w$}{notation:Graphs}
		\losEntry{$\edgesTo{W}/\edgesTo{w}$}{edges towards set $W$/node $w$}{notation:Graphs}
		\losEntry{$G^\leftrightarrow$}{bidirectional graph}{def:BidirectionalGraph}
		\losEntry{$\reve[e]$}{reversed edge}{def:BidirectionalGraph}
		
		\losEntry{$\abs{p}$}{length of a path~$p$ (\ie number of its edges)}{}
		\losEntry{$p|_{<e}$}{subpath of path $p$ before edge $e \in p$, \ie the suffix of~$p$ consisting of all edges until (but not including)~$e$}{def:CorrespondingDirectDynamicFlow}
		
		\losEntry{$\network$}{network}{def:Network}
		\losEntry{$s \in V$}{source node}{}
		\losEntry{$t \in V$}{sink node}{}
		\losEntry{$\nu_e \in \IRnn$}{edge capacity}{}
		\losEntry{$\gamma_e \in \IR$}{edge cost}{}
		\losEntry{$\gamma_e: \IRnn \to \IR$}{flow dependent edge costs}{sec:WardropEquilibria}
		\losEntry{$\tau_e \in \IRnn$}{edge travel time}{}
		
		\losEntry{$\ell_v \in \IRI$}{node label denoting the distance from $s$ to $v$}{def:NodeLabels}
		
		\losEntry{$\PathSet$}{set of \stpath{}s}{def:Network}
		\losEntry{$\CycleSet$}{set of cycles}{def:Network}
		
		\losEntry{$\tauMin \in \IRnn$}{minimum edge travel time $\tauMin \coloneqq \min_e \tau_e$}{}
		\losEntry{$\tauMax \in \IRnn$}{maximum edge travel time $\tauMax \coloneqq \max_e \tau_e$}{}
		\losEntry{$\tauPmin \in \IRnn$}{travel time along shortest \stpath}{}
				
		\losEntry{$\gamma_p \in \IR$}{total cost of path~$p$ (\wrt $\gamma$): $\gamma_p \coloneqq \sum_{e \in p}\gamma_e$}{def:NodeLabels}
		\losEntry{$\tau_p \in \IRnn$}{total travel time along path~$p$ (\wrt $\tau$): $\tau_p \coloneqq \sum_{e \in p}\tau_e$}{}
		
		\losEntry{$u \in \locInt[\IR]$}{network inflow rate}{def:NetworkInflowRate}
		
		% Flow
		\hline Static Flows & \\\hline
		\losEntry{$x = (x_e) \in \IRnn^E$}{edge-based static flow}{def:StaticFlow}
		\losEntry{$x = (x_p) \in \IRnn^{\PathSet\cup\CycleSet}$}{path-cycle-based static flow}{}
		
		\losEntry{$\fvals{x}$}{value of a static \stflow{} $x$}{def:FlowValueStatic}
		\losEntry{$\ccaps{A}$}{capacity of static cut $A \subseteq V$}{def:stCut}
		
		\losEntry{$\fcosts{x}$}{cost of flow $x$}{def:StaticFlowCost}
		
		\losEntry{$\nu^x$}{residual capacity \wrt $x$}{def:ResidualNetwork}
		\losEntry{$\gamma^x$}{residual costs \wrt $x$}{def:CycleAugmentation}
		\losEntry{$\resN[G]{x}$}{residual graph \wrt $x$}{def:ResidualNetwork}
		\losEntry{$\resN[\network]{x}$}{residual network \wrt $x$}{def:ResidualNetwork}
		
		\hline Dynamic Flows & \\\hline
		\losEntry{$(f^+_e,f^-_e) \in \locInt[\IR]^{\{\pm\}\times E}$}{edge-based dynamic flow}{def:DynamicFlow}
		\losEntry{$\fval{f}$}{value of a dynamic \stflow{} $f$}{def:FlowValueDynamic}
		\losEntry{$\ccap{\beta}$}{capacity of dynamic \stcut{} $\beta \in \IR^V$}{def:CutCapacityDynamic}
		\losEntry{$(f_p) \in \locInt[\IR]^{\PathSet}$}{path-based dynamic flow}{def:CorrespondingDirectDynamicFlow}
		\losEntry{$f^+_{p,j},f^-_{p,j}$}{in-/outflow in/from $j$-th edge on path~$p$}{def:CorrespondingVickreyFlow}
		\losEntry{$F^+_e, F^-_e$}{cummulative in- and outflow}{def:DynamicFlow}
		\losEntry{$\ql(\theta)$}{queue on edge~$e$ at time~$\theta$}{def:Queue}
		\losEntry{$\cexittime(\theta)$}{arrival time over edge~$e$ when entering at time~$\theta$}{def:Queue}
		\losEntry{$\cexittime[p](\theta)$}{arrival time over path~$p$ when entering at time~$\theta$}{def:PathArrivalTime}
		
		%%
		\losEntry{$\edgeloading$}{(Vickrey) edge loading mapping}{def:EdgeLoading}
		\losEntry{$\networkloading$}{(Vickrey) network loading mapping}{def:NetworkLoading}
	
		
		%%
		\losEntry{$\lRi[v](\theta)$}{shortest remaining instantaneous distance from node~$v$ to sink~$t$ at time~$\theta$}{def:IDELabels}
		\losEntry{$\setAEi[\theta]$}{set of active edges at time~$\theta$}{def:IDELabels}
		\losEntry{$\lLa[v](\theta)$}{earliest actual arrival time at node~$v$ when starting at~$s$ at time~$\theta$}{def:DELabels}
		\losEntry{$\Theta_e$}{set of times where $e$ lies on a shortest \stpath}{def:DELabels}
		\losEntry{$\setEff[\theta]$}{set of efficient edges at time $\theta$}{def:EfficientResettingEdges}
		\losEntry{$\setRes[\theta]$}{set of resetting edges at time $\theta$}{def:EfficientResettingEdges}
		
		\losEntry{$\psi_e(x) $}{change of travel time along an edge with (constant) inflow rate $x$}{lemma:IDEExtensionCharacterization}
		\losEntry{$\Lambda(u)$}{set of oath-based flows with network inflow rate~$u$}{def:DynamicEquilibrium}
		\losEntry{$\Psi: \Lambda(u) \to C(\IR)^\PathSet$}{an operator mapping path-based flows to path costs}{def:DynamicEquilibrium}	
	\end{longtable}
}