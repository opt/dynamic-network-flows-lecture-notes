%!TEX root = ../dynamic-network-flows.tex
% !TeX spellcheck = en_GB

\section{Optimal Dynamic Flows}\label{chap:DFFixedTravelTimes}

\begin{definition}\label{def:DynamicFlow}
	Let $\network=(G,s,t,\nu,\tau)$ be an $s$,$t$-network with capacities $\nu_e \in \IRp$ and travel times $\tau_e \in \IRnn$ for all edges $e \in E$ (we will also call this a \emph{dynamic flow network}).
	
	An \emph{(edge based) dynamic flow} (or \emph{flow over time})\dindex{flow!dynamic}\dindex{flow!over time|see{dynamic flow}} $f=(f^+_e,f^-_e)_{e \in E}$ in this network then consists of a pair of locally integrable function $f^+_e, f^-_e: \IR \to \IRnn$ for every edge $e \in E$ with $f^+_e(\theta) = f^-_e(\theta) = 0$ for all $\theta < 0$. Here, $f^+_e(\theta)$ is the \emph{ inflow rate} into edge~$e$ at any time~$\theta$ and $f^-_e(\theta)$ is the \emph{outflow rate}.
	
	We denote by
		\[F^+_e(\theta) \coloneqq \int_0^\theta f^+_e(\zeta)\diff\zeta \quad\text{ and }\quad F^-_e(\theta) \coloneqq \int_0^\theta f^-_e(\zeta)\diff\zeta\]
	the \emph{cumulative in-} and \emph{outflow}, respectively.
	
	The \emph{value}\dindex{value!of dynamic flow} of a dynamic $s$,$t$-flow~$f$ up to some time horizon~$T \in \IRnn$ is the cumulative inflow into the sink~$t$ until time~$T$, \ie\label{def:FlowValueDynamic}
		\[\fval{f,T} \coloneqq \sum_{e \in \edgesTo{t}}F^-_e(T) - \sum_{e \in \edgesFrom{t}}F^+_e(T).\]
\end{definition}

\begin{observation}
	The cumulative in- and outflow functions are non-decreasing, absolutely continuous functions with the in- and outflow rates, respectively, as their derivative almost everywhere (\cf \Cref{thm:FundamentalTheoremOfCalculus}).
\end{observation}

\begin{definition}\dindex{flow conservation}
	A dynamic flow $f$
	\begin{itemize}
		\item satisfies \emph{weak flow conservation on edges} if we have
			\begin{align}
				F^-_e(\theta+\tau_e) \leq F^+_e(\theta) \text{ for all } e \in E \text{ and } \theta \in \IRnn.
			\end{align}
		\item satisfies \emph{strong flow conservation on edges} if we have
			\begin{align}
				F^-_e(\theta+\tau_e) = F^+_e(\theta) \text{ for all } e \in E \text{ and } \theta \in \IRnn.
			\end{align}
		\item \emph{respects the edges capacities} if we have
			\begin{align}
				f^-_e(\theta) \leq \nu_e \text{ for all } e \in E \text{ and almost all } \theta \in \IRnn.
			\end{align}
		\item satisfies \emph{weak flow conservation at nodes} if we have
			\begin{align}
				\sum_{e \in \edgesFrom{v}}F^+_e(\theta) \leq \sum_{e \in \edgesTo{v}}F^-_e(\theta) \text{ for all } v \neq s \text{ and } \theta \in \IRnn.
			\end{align}
		\item satisfies \emph{strong flow conservation at nodes} if we have
			\begin{align}
				\sum_{e \in \edgesFrom{v}}F^+_e(\theta) = \sum_{e \in \edgesTo{v}}F^-_e(\theta) \text{ for all } v \neq s,t \text{ and } \theta \in \IRnn.
			\end{align}
			while $\theta \mapsto \fval{f,\theta}$ is a non-decreasing function.
	\end{itemize}	
	We say that a dynamic flow $f$ is \emph{feasible}\dindex{flow!dynamic!feasible} if it respects edge capacities and satisfies weak flow conservation on edges and at nodes. We call it a \emph{direct dynamic flow}\dindex{flow!dynamic!direct} if it satisfies strong flow conservation on edges and at nodes.
\end{definition}

\begin{observation}\label{obs:WlogStrongFlowConservationOnEdges}
	Let $f$ be any feasible dynamic flow and $T\geq 0$ some time horizon. Then, the dynamic flow $g$ defined by
		\[g^-_e(\theta) \coloneqq \begin{cases}f^-_e(\theta),&\text{ if } \theta \leq T\\0,&\text{ else}\end{cases} \quad\text{ and }\quad g^+_e(\theta) \coloneqq g^-_e(\theta+\tau_e) \quad\text{ \fa } e \in E \text{ and } \theta \in \IR\]
	is also feasible and additionally satisfies strong flow conservation on edges. Moreover, it has at least the same value as $f$ up to time~$T$ and we have $G \leq F$.\lukas{I think it should also be true that there exists a flow that additionally satisfies strong flow conservation at nodes, but the proof is probably more involved...}
\end{observation}

\begin{remark}
	If $f=(f^+_e,f^-_e)$ is a dynamic flows satisfying strong flow conservation on edges, then we have $f^-_e(\theta+\tau_e) = f^+_e(\theta)$ for all $e \in E$ and almost all $\theta \in \IR$. Thus, such a flow is already completely described by its edge inflow rates. Consequently, we will often denote such flows as $f=(f^+_e)$. 
\end{remark}

\begin{definition}\label{def:CorrespondingDirectDynamicFlow}
	A \emph{path-based dynamic flow}\dindex{flow!dynamic!path-based} $f=(f^+_p)_{p \in \PathSet}$ consists of a locally integrable function $f_p: \IR \to \IRnn$ for every path $p \in \PathSet$ with $f^+_p(\theta) = 0$ for all $\theta < 0$.
	
	The \emph{corresponding direct dynamic flow} $(f^+_e,f^-_e)$ is defined by
		\[f^+_e(\theta) = \sum_{p \in \PathSet: e \in p}f^+_p(\theta-\tau_{p|_{<e}}) \quad\text{ and }\quad f^-_e(\theta) \coloneqq f^+_e(\theta-\tau_e)\]		
	where we denote by $\tau_p \coloneqq \sum_{e \in p}\tau_e$ the total travel time along a path $p$ and by $p|_{<e}$ the subpath of $p$ consisting of all the edges before (but not including) some edge~$e \in p$.
\end{definition}

\begin{observation}\label{obs:CorrespondingEdgeBasedFlowIsDirectFlow}
	The corresponding edge-based flow to a path-based flow is a direct dynamic flow.
\end{observation}

\subsection{Maximum Dynamic Flows}

\begin{definition}
	A feasible dynamic flow $f$ is a \emph{maximum dynamic flow with time horizon $T$}\dindex{flow!dynamic!maximum} if it attains the maximal value up to $T$ among all feasible dynamic flows in its network, \ie
		\[\fval{f,T} \geq \fval{g,T} \text{ for all feasible dynamic flows } g.\]
\end{definition}

\begin{theorem}\label{thm:DynamicMaxFlowExistence}\tindex{flow!dynamic!maximum}
	Let $\network$ be any dynamic flow network and $T \in \IRnn$ some time horizon. Then, there exists a maximum dynamic flow with time horizon~$T$.  
\end{theorem}

\begin{proof}
	By \Cref{obs:WlogStrongFlowConservationOnEdges} we can restrict ourselves to feasible dynamic flows satisfying strong flow conservation on the edges and $\supp{f^-_e}, \supp{f^+_e} \subseteq [0,T]$ for all $e \in E$. Thus, let 			
		\[\bar{\mathcal{F}}(\network) \coloneqq \Set{(f^+_e) \in \locInt^E | \begin{array}{l}\supp f^+_e \subseteq [0,T] \text{ and } f^+_e \leae \nu_e \text{ \fa } e \in E,\\(f^+_e) \text{ satisfies weak flow conservation at nodes}\end{array}}\]
	be the set of all such flows. We then have $\bar{\mathcal{F}}(\network) \subseteq \pInt[2]^E$ since
		\[\norm{f}_2 = \sum_{e \in E}\norm{f^+_e}_2 = \sum_{e \in E} \Big(\int_{-\infty}^\infty \abs{f^+_e(\zeta)}^2\diff\zeta\Big)^{\nicefrac{1}{2}} \leq \sum_{e \in E} \Big(\int_0^T \nu_e^2\diff\zeta\Big)^{\nicefrac{1}{2}} \eqqcolon M < \infty\]
	for all $f \in \bar{\mathcal{F}}(\network)$. Moreover, it is a weakly-closed subset of $\ballClosed[M]{0} \subseteq \pInt[2]^E$ which is weakly compact by \Cref{thm:UniBallWeaklyClosedIffReflexive}. Thus, it is weakly compact itself as well. 
	
	Finally, the mapping:
		\[\fval{.,T}: \bar{\mathcal{F}}(\network) \to \IRnn, f \mapsto \fval{f,T}\]
	is weak-strong continuous as for any $f \in \bar{\mathcal{F}}(\network)$ and $\eps > 0$ we have
		\begin{align*}
			&\abs{\fval{f,T} - \fval{f',T}} \\
				&\quad\leq \sum_{e \in \edgesTo{t}}\Abs{\int_0^{T-\tau_e}f^+_e(\zeta)\diff\zeta-\int_0^{T-\tau_e}f'^+_e(\zeta)\diff\zeta} + \sum_{e \in \edgesFrom{t}}\Abs{\int_0^{T}f^+_e(\zeta)\diff\zeta-\int_0^{T}f'^+_e(\zeta)\diff\zeta} \\
				&\quad\leq \eps
		\end{align*}
	for all $f' \in U(g,f,\eps)$ where $g \in \pInt[2]$ with $g_e: \theta \mapsto 1$. 
	
	Thus, the extreme value theorem (\Cref{thm:ExtremeValueTheorem}) guarantees the existence of a maximum dynamic flow.
\end{proof}


\subsubsection{Temporally Repeated Flows}

\begin{definition}
	Let $\network=(G,s,t,\tau,\nu)$ be a dynamic flow network and $(x_e)$ a static(!) \stflow{} in the static network $\network= (G,s,t,\nu)$ with path-cycle decomposition $(x_p)$. Then, the associated \emph{temporally repeated flow}\dindex{flow!temporally repeated} $f=(f^+_e,f^-_e)$ is the edge-based dynamic flow corresponding to the following path-based dynamic flow
		\begin{align}
			f^+_p(\theta) \coloneqq x_p \text{ for all } p \in \PathSet \text{ and } \theta \in \IRnn. 
		\end{align}
\end{definition}

\begin{lemma}\label{lemma:TemporallyRepeatedFlow}
	Let $(x_e)$ be a feasible static \stflow{} with path-cycle decomposition~$(x_p)$. Then, the associated temporally repeated flow is a feasible direct dynamic flow and its value up to $T$ is lower bounded by
		\begin{align}\label{eq:ValueOfTemporallyRepeatedFlow}
			T\cdot\fvals{x}-\sum_{e \in E}\tau_e x_e.
		\end{align}
	If $(x_p)$ is a path-decomposition which does not use any paths $p$ with $\tau_p > T$, then the value of the corresponding temporally repeated flow is equal to \eqref{eq:ValueOfTemporallyRepeatedFlow}.
\end{lemma}

\begin{proof}
	The definition of the temporally repeated flow $(f^+_e,f^-_e)$ corresponding to $(x_e)$ directly ensures that all flow rates are zero before time zero and, together with \Cref{obs:CorrespondingEdgeBasedFlowIsDirectFlow}, that strong flow conservation holds both on edges and at nodes. The flow also respects the edge capacities since the same is true for the static flow:
		\[f^-_e(\theta) = f^+_e(\theta-\tau_e) = \sum_{p \in \PathSet: e \in p}f^+_p(\theta-\tau_e-\tau_{p|_{<e}}) \leq \sum_{p \in \PathSet: e \in p}x_p = x_e \leq \nu_e.\]
	Now, in order to compute the value of $f$, let $(x_p)$ be a path-cycle decomposition of $(x_e)$. Then, we have
	\begin{align*}
		\fvals{f,T}
			&=\sum_{e \in \edgesTo{t}}\int_0^T \underbrace{f^-_e(\zeta)}_{=f^+_e(\zeta-\tau_e) = \sum_{p \in \PathSet:e \in p}f^+_p(\zeta-\tau_e-\tau_{p|_{<e}})}\diff\zeta - \sum_{e \in \edgesFrom{t}}\int_0^T \underbrace{f^+_e(\zeta)}_{=0}\diff\zeta \\
			&=\sum_{e \in \edgesTo{t}}\sum_{p \in \PathSet: e \in p}\int_0^T f^+_p(\zeta-\tau_e-\tau_{p|_{<e}})\diff\zeta = \sum_{p \in \PathSet}\int_0^Tf^+_p(\zeta-\tau_p)\diff\zeta \\
			&\symoverset{1}{\geq} \sum_{p \in \PathSet}(T-\tau_p)x_p = T\sum_{p \in \PathSet}x_p - \sum_{p \in \PathSet}\tau_px_p \symoverset{2}{\geq} T\sum_{p \in \PathSet}x_p - \sum_{p \in \PathSet\cup\CycleSet}\tau_px_p \\
			&= T\cdot\fvals{x}-\sum_{e \in E}\tau_ex_e. 
	\end{align*}
	Since \refsym{1} holds with equality for all paths with $\tau_p \leq T$ and \refsym{2} holds with equality if the decomposition of $(x_e)$ does not use any cycles, this also proves the second part of the \namecref{lemma:TemporallyRepeatedFlow}.
\end{proof}

\begin{lemma}\label{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}
	Let $\network=(G,s,t,\nu,\tau)$ be a dynamic flow network and $T \geq 0$ some time horizon. Define the \stnetwork{} $\network'=(G',s,t,\nu,\gamma)$ by setting $G'\coloneqq(V,E \dcup \set{ts})$\lukas{We implicitly assume here, that the edge $ts$ is not already present in $\network$. But this shouldn't be a problem.}, $\nu_{ts} \coloneqq \infty$ and defining edge costs
		\[\gamma_e \coloneqq \begin{cases}
			\tau_e, &\text{ if } e \in E \\
			-T,		&\text{ if } e = ts
		\end{cases}.\]
	Let $(x'_e)$ be a min-cost flow of value~$0$ in $\network'$, $(x_e)$ its restriction to $\network$ and $(x_p)$ some path-cycle decomposition of $(x_e)$. Then, we have
		\begin{itemize}
			\item $\gamma_p \leq T$ for all $p \in \PathSet(\network)$ with $x_p > 0$ and
			\item $\gamma_c = 0$ for all $c \in \CycleSet(\network)$ with $x_c > 0$.
		\end{itemize}
	Moreover, $x$ is a flow maximizing~\eqref{eq:ValueOfTemporallyRepeatedFlow}.
\end{lemma}

\begin{proof}
	First, let $p \in \PathSet(\network)$ be any \stpath{} with $x_p > 0$. Then, we must also have $x'_{ts} > 0$ and, therefore, $p,ts$ is a cycle with positive flow on each edge under $x'$. Thus, the reverse of this cycle is present in $\resN[\network']{x'}$ and has non-negative cost by \Cref{lemma:MinCostFlowCharacterization}. Since the cost of this cycle is $-\gamma_p - \gamma_{ts} = -\tau_p + T$, this implies $\tau_p \leq T$.
	
	Now, let $c \in \CycleSet(\network)$ be any cycle with $x_c > 0$. Then, again by \Cref{lemma:MinCostFlowCharacterization} this cycle must have non-positive cost. But, at the same time, all edges in~$E$ have non-negative cost and, thus, the cost of $c$ must be zero.
	
	Moreover, the cost of $x'$ is
		\[\fcosts{x'} = \sum_{e \in E'}\gamma_e x'_e = -T\cdot x'_{ts} + \sum_{e \in E}\tau_e x'_e = -T\cdot\fvals{x} + \sum_{e \in E}\tau_e x_e.\]
	Thus, $x'$ minimizes this value, which means that $x$ maximizes~\eqref{eq:ValueOfTemporallyRepeatedFlow}.
\end{proof}

\begin{algorithm}[H]
	\caption{Dynamic Ford--Fulkerson Algorithm}\label{alg:DynamicFordFulkerson}\aindex{Dynamic Ford-Fulkerson}
	
	\Input{A dynamic flow network $\network=(G,s,t,\nu,\tau)$ and a time horizon $T \geq 0$}
	\Output{A maximum dynamic flow $f$}
	
	Compute a min-cost flow $x'$ of value $0$ in $\network'$ defined as in \Cref{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}
	
	Compute a path-cycle decomposition $(x_p$) of the restriction of $x$ to $\network$\label{alg:DynamicFordFulkerson:FlowX}
	
	\KwRet{temporally repeated flow $f$ associated with $(x_p)$} 
\end{algorithm}

\subsubsection{An Optimality Criterium for Maximum Dynamic Flows}

\begin{definition}
	A \emph{dynamic $s$,$t$-cut with time horizon $T$}\dindex{cut!dynamic} is specified by a number $\beta_v \in \IR$ for every node $v \in V$ such that we have $\beta_s = 0$ and $\beta_t = T$.
	
	For any node $v \in V$ we then say that it belongs to the $t$-side of this cut until time $\beta_v$ and to the $s$-side afterwards.
	
	We define the \emph{capacity}\dindex{capacity!dynamic cut} of such an $s$-$t$-cut by \label{def:CutCapacityDynamic}
	\[\mathrm{capacity}(\beta) \coloneqq \sum_{e=vw \in E}\nu_e\cdot\max\Set{0,\beta_w - \tau_e-\beta_v}.\]
\end{definition}

\begin{lemma}\label{lemma:DynamicCutIsUpperBound}
	For any fixed dynamic flow network, the capacity of any dynamic \stcut{} with time horizon~$T$ is an upper bound on the value of any feasible dynamic flow up to the same time horizon.
\end{lemma}

\begin{proof}
	Let $f$ be any dynamic \stflow{} and $\beta$ a dynamic \stcut{} with time horizon $T$. Similar to the proof of \Cref{lemma:StaticCutIsUpperBound} we compute $\fval{f,T}$:
	\begin{align*}
		\fval{f,T} 
		&= \sum_{e \in \edgesTo{t}}F^-_e(T) - \sum_{e \in \edgesFrom{t}}F^+_e(T) \\
		&\symoverset{1}{\leq} \sum_{v \in V}\Big(\sum_{e \in \edgesTo{v}} F^-_e(\beta_v) - \sum_{e \in \edgesFrom{v}} F^+_e(\beta_v)\Big) \\
		&= \sum_{e=vw \in E}\Big(F^-_e(\beta_w)-F^+_e(\beta_v)\Big) \\
		&\symoverset{2}{\leq} \sum_{e=vw \in E}\Big(F^-_e(\beta_w)-F^-_e(\beta_v+\tau_e)\Big)\\
		&= \sum_{e=vw \in E}\int_{\beta_v+\tau_e}^{\beta_w}\underbrace{f^-_e(\zeta)}_{\leq\nu_e}\diff\zeta \\
		&\symoverset{3}{\leq} \sum_{e = vw \in E}\nu_e\cdot\max\set{0,\beta_w-\tau_e-\beta_v} = \ccap{\beta},
	\end{align*}
	where \refsym{1} holds since we have weak flow conservation at all nodes as well as $\beta_s=0$ and $\beta_t=T$, \refsym{2} holds due to weak flow conservation on edges and \refsym{3} holds because $f$ respects the edge capacities. 
\end{proof}

\begin{theorem}\label{thm:TemporallyRepeatedFlowIsOpt}\tindex{flow!dynamic!maximum}\tindex{flow!temporally repeated}
	Consider the setting of \Cref{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}. Then, the temporally repeated flow~$f$ associated with~$x$ is a maximum dynamic flow with time horizon $T$.
\end{theorem}

\begin{proof}
	By \Cref{lemma:DynamicCutIsUpperBound} it suffices to find a cut with the same capacity as the value of $f$. 
	For this, we define 
		\[\beta_v \coloneqq \min\set{\inf\set{\gamma'_p | p \text{ an } \stpath[s][v] \text{ in } \resN[\network']{x'}},T}.\]
	This definition certainly satisfies $\beta_s=0$. It also satisfies $\beta_t = T$ since any \stpath{} must have cost of at least~$T$ as otherwise it would create a cycle of negative cost in $\resN[\network']{x'}$ (which cannot exist by \Cref{lemma:MinCostFlowCharacterization}). Thus, $\beta$ is a dynamic \stcut{} with time horizon~$T$. 
	
	We start by showing that these node labels satisfy the triangle inequality in $\resN[\network]{x}$:
	\begin{claim}\label{claim:MinimumDynamicCutTriangleInequality}
		The node labels $\beta_v$ satisfy
		\begin{align}\label{eq:MinimumDynamicCutTriangleInequality}
			\beta_w \leq \beta_v + \gamma'_{vw} \text{ for all } vw \in \resN[E]{x}.
		\end{align}
	\end{claim}
	
	\begin{proofClaim}
		This \namecref{claim:MinimumDynamicCutTriangleInequality} follows essentially from \Cref{lemma:PropertiesOfNodeLabels:TriangleInequality} since we know that $\resN[\network']{x'}$ does not contain any cycles of negative cost (\Cref{lemma:MinCostFlowCharacterization}). In order to make this argument more formal, let $(\ell_v)$ be the node labels with respect to~$\gamma'$ in~$\resN[\network']{x'}$. We now distinguish three cases:
		\begin{proofbycases}
			\caseitem{$\ell_v \leq T$} Here, we have
			\[\beta_w \leq \ell_w \Croverset{lemma:PropertiesOfNodeLabels:TriangleInequality}{\leq} \ell_v + \gamma'_{vw} = \beta_v + \gamma'_{vw}.\]
			\caseitem{$\gamma'_{vw} \geq 0$} Here, we have
			\[\beta_w = \min\set{\ell_w,T} \Croverset{lemma:PropertiesOfNodeLabels:TriangleInequality}{\leq} \min\set{\ell_v+\gamma'_{vw},T} \leq \min\set{\ell_v,T}+\gamma'_{vw}=\beta_v+\gamma'_{vw}.\]
			\caseitem{$\ell_v > T$ and $\gamma'_{vw}<0$} In this case $vw$ must be a backward edge, \ie we have $wv \in E$ and $x'_{wv}>0$. Now, let $(x'_c)$ be a cycle-decomposition of $x'$ (which exists by \Cref{lemma:StaticFlowPathDecomposition}) and $c$ a cycle containing $wv$. Since $\reve[c]$ is a cycle in the residual network, we must have $\gamma'_c \leq 0$. At the same time, it contains at least one edge ($wv$) with strictly positive cost and, therefore, must also contain edge $ts$ (as this is the only edge in $\network'$ with negative cost). Thus, we can write $c=p,wv,p',ts$ where $p$ is an \stpath[s][w] and $p'$ a \stpath[v][t]. Then, we have $st,\reve[p']$ as an \stpath[s][v] in $\resN[\network']{x'}$ with cost $\gamma'_{st,\reve[p']} = \gamma'_{st} - \gamma'_{p'} \leq T+0$, which is now a contradiction to $\ell_v > T$. \qedhere
		\end{proofbycases}		
	\end{proofClaim}
	
	Now consider any edge $e=vw \in E$ in the original network~$\network$. We claim that we then have
		\begin{align}\label{eq:TemporallyRepeatedFlowIsOpt:PerEdge}
			\nu_e\cdot\max\Set{0,\beta_w-\tau_e-\beta_v} = x_e\cdot(\beta_w-\tau_e-\beta_v).
		\end{align}
	We show this by a case distinction on the value of $x_e$:
	\begin{proofbycases}
		\caseitem{$x_e < \nu_e$} Then, edge~$e$ is contained in the residual network and, thus, we have $\beta_w \eqoverset{eq:MinimumDynamicCutTriangleInequality}{\leq} \beta_v + \gamma'_{vw} = \beta_v +\tau_e$. Hence, we have $\max\Set{0,\beta_w-\tau_e-\beta_v}=0$.
		\caseitem{$x_e > 0$} Then, edge~$\reve=wv$ is contained in the residual network and, thus, we have $\beta_v \eqoverset{eq:MinimumDynamicCutTriangleInequality}{\leq} \beta_w + \gamma'_{wv} = \beta_w-\tau_e$. Hence, we have $\max\Set{0,\beta_w-\tau_e-\beta_v} = \beta_w-\tau_e-\beta_v$.
	\end{proofbycases}
	For $x_e=0$ and $x_e=\nu_e$, this immediately shows~\eqref{eq:TemporallyRepeatedFlowIsOpt:PerEdge}. For $0 < x_e < \nu_e$ both cases apply and, together, also imply~\eqref{eq:TemporallyRepeatedFlowIsOpt:PerEdge}. With this we now get
	\begin{align*}
		\mathrm{capacity}(\beta) 
			&= \sum_{e=vw \in E}\nu_e\cdot\max\Set{0,\beta_w-\tau_e-\beta_v} \\
			&\eqoverset{eq:TemporallyRepeatedFlowIsOpt:PerEdge}{=} \sum_{e = vw \in E} x_e\cdot(\beta_w-\tau_e-\beta_v) \\
			&=\sum_{v \in V}\beta_v\left(\sum_{e \in \edgesTo{v}}x_e-\sum_{e \in \edgesFrom{v}}x_e\right) - \sum_{e \in E}x_e\tau_e \\
			&\eqoverset{cons:SFStrongFlowConservation}{=}\beta_s\left(\sum_{e \in \edgesTo{s}}x_e-\sum_{e \in \edgesFrom{s}}x_e\right)+\beta_t\left(\sum_{e \in \edgesTo{t}}x_e-\sum_{e \in \edgesFrom{t}}x_e\right)-\sum_{e \in E}x_e\tau_e \\
			&=0+T\cdot\mathrm{value}(x)-\sum_{e \in E}x_e\tau_e \\
			&\Croverset{lemma:TemporallyRepeatedFlow}{=} \fval{f,T}.
	\end{align*}
	Together with \Cref{lemma:DynamicCutIsUpperBound} this proves that~$f$ is a maximum dynamic flow with time horizon~$T$.
\end{proof}

\begin{corollary}
	\Cref{alg:DynamicFordFulkerson} is correct. \qed
\end{corollary}

\begin{corollary}
	The maximum value of a dynamic \stflow{} with time horizon~$T$ is equal to the minimum capacity of a dynamic \stcut{} with time horizon~$T$. \qed
\end{corollary}


\subsection{Quickest Flows}

\begin{definition}
	A feasible dynamic flow $f$ is a \emph{quickest flow with value~$v$}\dindex{flow!quickest} if among all feasible dynamic flows in its network no flow reaches a value of~$v$ earlier than $f$, \ie
		\[\exists T \in \IRnn: \fval{f,T}=v \text{ and } \fval{g,\theta} < v \text{ \fa feasible dynamic flows } g \text{ and } \theta \in [0,T).\]
\end{definition}

\begin{theorem}\tindex{flow!quickest}
	Let $\network$ be any dynamic flow network where $t$ is reachable from $s$ and $v \in \IRnn$ some desired value. Then, there exists a quickest flow with value~$v$.
\end{theorem}

\begin{proof}
	This can be proven in essentially the same way as the existence of maximum dynamic flows (\ie \Cref{thm:DynamicMaxFlowExistence}): Choose $T$ large enough that there is at least one flow of value at least~$v$ until~$T$ (\eg $T=\frac{v}{\bar\nu}+\tau_p$ where $p$ is some \stpath{} and $\bar\nu$ the minimum capacity on this path) and use the following objective function:
		\[\bar{\mathcal{F}}(\network) \to \IRnn, f \mapsto \inf\set{\theta \in \IRnn | \fval{f,\theta} \geq v}. \qedhere\]
\end{proof}

\begin{lemma}\label{lemma:DynamicMaxValueFunctionStrictlyIncreasing}
	For any dynamic flow network~$\network$ the function 
		\[V: \IRnn \to \IRnn, T \mapsto \max\set{\fval{f,T} | f \text{ a feasible dynamic flow in } \network}\]
	is equal to zero on~$[0,\tauPmin]$ and strictly increasing on $(\tauPmin,\infty)$ where $\tauPmin \coloneqq \min\set{\tau_p | p \in \PathSet}$ is the length of a shortest \stpath. 
\end{lemma}

\begin{proof}
	Since in a feasible dynamic flow no flow can reach the sink before time~$\tauPmin$\lukas{This is currently an exercise}, we have $V(\theta)=0$ for all $\theta \leq \tauPmin$. Now, let $T > T' > \tauPmin$ be any two times horizons in $(\tauPmin,\infty)$. Let $x$ be a static \stflow{} as computed by \Cref{alg:DynamicFordFulkerson} for time horizon~$T'$ and $f$ the associated temporally repeated flow. Then, we have
		\begin{align*}
			V(T) \geq \fval{f,T} 
				&\Croverset{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}{=} T\cdot\fvals{x}-\sum_{e \in E}\tau_e x_e \\
				&> T'\cdot\fval{x}-\sum_{e \in E}\tau_e x_e \Croverset{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}{=} \fval{f,T'} = V(T'). \qedhere
		\end{align*}
\end{proof}

\begin{lemma}\label{lemma:MaxDynamicFlowIffQuickestFlow}\tindex{flow!dynamic!maximum}\tindex{flow!quickest}
	Let $f$ be a feasible dynamic flow and $T \geq 0$ some time horizon. Then, $f$ is a maximum dynamic flow with time horizon~$T$ if and only if it is a quickest flow with value $\fval{f,T}$.
\end{lemma}

\begin{proof}
	\begin{description}
		\item[maximum $\implies$ quickest:] We distinguish two cases: If $T \leq \tauPmin$, we have $\fval{f,T}=0$ and any feasible dynamic flow is a quickest flow with value~$0$. So, assume now that $f$ is a maximum flow with time horizon~$T > \tauPmin$ and let $g$ be any feasible dynamic flow. Since, according to \Cref{lemma:DynamicMaxValueFunctionStrictlyIncreasing}, $V$ is non-decreasing on all of $\IRnn$ and strictly increasing after~$\tauPmin$ we then have
			\[\fval{g,\theta} \leq V(\theta) < V(T) = \fval{f,T}.\]
		for any $\theta < T$. Hence, $f$ is a quickest flow with value~$\fval{f,T}$.
		
		\item[quickest $\implies$ maximum:] Assume that $f$ is a quickest flow with value $\fval{f,T}$ and let $g$ be any feasible dynamic flow. Then, we have $\fval{g,\theta} < \fval{f,T}$ for all $\theta < T$. As $\fval{g,.}: \IRnn \to \IRnn$ is clearly continuous, this implies $\fval{g,T} \leq \fval{f,T}$. Thus, $f$ is a maximum flow with time horizon~$T$.\qedhere
	\end{description}
\end{proof}

\begin{lemma}\label{lemma:QuickestFlowHasRationalTimeHorizon}
	Let $f$ be a quickest flow with value~$v \in \INo$ in a dynamic flow network with integer edge capacities and travel times. Then,
		\[T \coloneqq \min\set{\theta \in \IRnn | \fvals{f,\theta}=v}\]
	is a rational number with denominator bounded by the capacity of a minimum (static) \stcut{} in the network.
\end{lemma}

\begin{proof}
	Using the $T$ defined in the \namecref{lemma:QuickestFlowHasRationalTimeHorizon}'s statement, define the static \stnetwork{} $\network'$ as in \Cref{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}. According to \Cref{cor:MinCostFlowIsIntegral} there is then an integral min-cost flow~$x$ of value~$0$ which, by \Cref{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow,lemma:TemporallyRepeatedFlow}, corresponds to a temporally repeated flow with value $T\cdot\fvals{x}-\sum_{e \in E}\tau_e x_e$. Moreover, this flow is a maximum dynamic flow with time horizon~$T$ by \Cref{thm:TemporallyRepeatedFlowIsOpt} while $f$ is a maximum dynamic flow with the same time horizon according to \Cref{lemma:MaxDynamicFlowIffQuickestFlow}. Hence, we have
		\[v = \fval{f,T} = T\cdot\fvals{x}-\sum_{e \in E}\tau_e x_e\]
	and, therefore,
		\[T = \frac{v+\sum_{e \in E}\tau_e x_e}{\fvals{x}}.\]
	As $x$ is integral and its value is upper bounded by the minimum capacity of any (static) \stcut{} in the network (by \Cref{lemma:StaticCutIsUpperBound}), this proves the \namecref{lemma:QuickestFlowHasRationalTimeHorizon}.
\end{proof}

\begin{corollary}\tindex{flow!quickest}
	In any dynamic flow network with integer capacities and travel times, a quickest flow of any integral value can be found in finite time.
\end{corollary}

\begin{proof}
	By \Cref{lemma:MaxDynamicFlowIffQuickestFlow} it suffices to correctly guess the time horizon of the quickest flow and then compute a maximum dynamic flow with that time horizon (\eg using \Cref{alg:DynamicFordFulkerson}). By \Cref{lemma:QuickestFlowHasRationalTimeHorizon} there are only finitely many possible values for this time horizon.
\end{proof}


\subsection{Earliest Arrival Time Flows}

\begin{definition}
	A feasible dynamic flow $f$ is an \emph{earliest arrival time flow with time horizon $T$}\dindex{flow!earliest arrival time} if it is a maximum dynamic flow with time horizon $\theta$ for all $\theta \in [0,T]$.
\end{definition}

\begin{definition}
	A \emph{generalized path-based dynamic flow}\dindex{flow!dynamic!path-based!generalized} in a dynamic flow \stnetwork{} $\network=(G,s,t,\nu,\tau)$ is a vector $(f^+_p)_{p \in \PathSet^\leftrightarrow} \in \locInt^{\PathSet^\leftrightarrow}$ with $f^+_p(\theta) = 0$ for all $\theta < 0$, where $\PathSet^\leftrightarrow$ denotes the set of all \stpath s in the bidirectional graph $G^\leftrightarrow$.
	
	The \emph{corresponding dynamic pseudo-flow} $(f^+_e,f^-_e)$ in~$\network$ is defined by
		\[f^+_e(\theta) \coloneqq \sum_{p \in \PathSet^\leftrightarrow: e \in p}f^+_p(\theta-\tau_{p|_{<e}}) - \sum_{p \in\PathSet^\leftrightarrow: \reve \in p}f^+_p(\theta-\tau_{p|_{<\reve}}+\tau_e)\]
	and $f^-_e(\theta) \coloneqq f^+_e(\theta-\tau_e)$ for all $e \in E$. Here, we use $\tau_{\reve} \coloneqq -\tau_e$ for all $e \in E$ in the calculation of $\tau_p$ for $p \in \PathSet^\leftrightarrow$.
\end{definition}

\begin{observation}\label{obs:CorrespondingPseudoFlowFlowConservation}
	While the corresponding dynamic pseudo-flow is guaranteed to satisfy strong flow conservation on both edges and at nodes, it is, in general, not even an (edge-based) dynamic flow as it need not be non-negative.
\end{observation}

\begin{algorithm}
	\caption{Earliest-Arrival Algorithm}\label{alg:EarliestArrival}\aindex{Earliest Arrival}
	\Input{A dynamic flow network~$\network$ and some time horizon~$T$}
	\Output{An earliest arrival time flow $f$ with time horizon~$T$}
	
	Compute a static \stflow{} $y$ with associated temporally repeated flow that is a maximum dynamic flow with time horizon~$T$
	
	Use the Successive-Shortest-Path algorithm (\Cref{alg:SuccessiveShortestPath}) to compute a static min-cost flow~$(x_p)_{p \in \PathSet^\leftrightarrow}$ of value~$\fvals{y}$ in $\network$ (using $\tau_e$ as edge costs)
	
	Define a generalized path-based dynamic flow $f^+_p$ by setting $f^+_p(\theta) \coloneqq x_p$ for all $\theta \geq 0$
	
	\Return{dynamic pseudo-flow corresponding to $(f^+_p)$}
\end{algorithm}

\begin{theorem}\label{thm:EarliestArrivalAlgCorrect}
	\Cref{alg:EarliestArrival} is correct.\lukas{Line 2 of the algorithm could be stated cleaner if we had stated before that SSP can also be used to compute maximum flows of a certain cost (\cf exercise from Opti 3)} If all capacities are integer, then it is guaranteed to terminate.
\end{theorem}

\begin{proof}
	Assume that the algorithm terminates and let $f=(f^+_e,f^-_e)$ be the returned pseudo-flow. We start by showing that $f$ is a feasible dynamic flow. For this, enumerate the augmenting paths used by the Successive-Shortest-Path algorithm as $p_1, p_2, \dots, p_K$ and let $x^k$ be the (static) \stflow{} after the first $k$ augmentation steps. Moreover, we define $x^0 \coloneqq 0$ as the zero flow. Finally, let $(\ell_v^k) \in \IR^V$ be the node labels with respect to $\gamma'$ in the residual network~$\resN{x^k}$.
	
	\begin{claim}\label{claim:FeasibilityOfGeneralizedTemporallyRepeatedFlow}
		For any $k \in \set{0,1,\dots,K}$ define $f^k = (f^{k,+}_e,f^{k,-}_e)$ as the pseudo-flow corresponding to the generalized path-based flow $(f^{k,+}_p)$ defined by $f^{k,+}_p(\theta) \coloneqq x_p^k$ for all $\theta \geq 0$. Then, this flow satisfies
			\[f^{k,+}_e(\theta) \in [0,\nu_e] \text{ for all } \theta \in [0,\ell^k_v] \text{ and } f^{k,+}_e(\theta) = x^k_e \text{ for all } \theta \geq \ell^k_v\]
		on all edges $e = vw \in E$.
	\end{claim}

	\begin{proofClaim}
		We show this claim by induction on~$k$:
		\begin{proofbyinduction}
			\basecase{$k=0$} As both $x^0$ and $f^0$ are just the zero flows, the claim trivially holds here.
			\inductionstep{$k\to k+1$} Let $e = vw \in E$ be any edge. We distinguish three cases:
				\begin{proofbycases}
					\caseitem{$e \in p_{k+1}$} Since $p_{k+1}$ is a shortest \stpath{} in $\resN{x^k}$ which does not contain any cycle of negative cost, \Cref{lemma:PropertiesOfNodeLabels:PathEfficientIffEdgesEfficient} guarantees that any prefix of $p_{k+1}$ is a shortest path as well. In particular, we have $\tau_{{p_{k+1}}|_{<e}} = \ell^k_v$. With this, the definition of the corresponding pseudo-flow directly gives us
						\[f^{k+1,+}_e(\theta) = f^{k,+}_e(\theta) \in [0,\nu_e] \text{ for all } \theta \leq \ell^k_v\]
					and 
						\[f^{k+1,+}_e(\theta) = f^{k,+}_e(\theta) + x_{p_{k+1}} = x^k_e + x_{p_{k+1}} = x^{k+1}_e \text{ for all } \theta \geq \ell^k_v.\]
					Since $x^{k+1}_e$ is a feasible (static) \stflow{} and we have $\ell^k_v \leq \ell^{k+1}_v$ by \Cref{lemma:AugmentingFlowsWithoutNegCycles}, this shows the claim.
					
					\caseitem{$\reve \in p_{k+1}$} By the same argument as in the previous case, we get that ${p_{k+1}}|_{<\reve},\reve$ is a shortest \stpath[s][v] in $\resN{x^k}$ and, therefore, we have $\tau_{{p_{k+1}}|_{<\reve}}-\tau_e=\ell^k_v$. This then gives us
						\[f^{k+1,+}_e(\theta) = f^{k,+}_e(\theta) \in [0,\nu_e] \text{ for all } \theta \leq \ell^k_v\]
					and 
						\[f^{k+1,+}_e(\theta) = f^{k,+}_e(\theta) - x_{p_{k+1}} = x^k_e - x_{p_{k+1}} = x^{k+1}_e \text{ for all } \theta \geq \ell^k_v.\]
					As before, this proves the claim.
					
					\caseitem{$e,\reve \notin p_{k+1}$} In this case we have $x^{k+1}_e=x^k_e$ as well as $f^{k+1,+}_e = f^{k,+}_e$ and, thus, the claim follows immediately by induction. \qedhere
				\end{proofbycases}
		\end{proofbyinduction}
	\end{proofClaim}

	With \Cref{claim:FeasibilityOfGeneralizedTemporallyRepeatedFlow} (and \Cref{obs:CorrespondingPseudoFlowFlowConservation}) we now have feasibility of the pseudo flow $f=f^K$. 
	
	Now, let $\theta \leq T$ be any time and $\bar k \coloneqq \max\set{k | \tau_{p_k} \leq \theta}$. Since $x^{\bar k}$ is an intermediate step during the Successive-Shortest-Path algorithm, we know that it is a min-cost flow of value~$\fvals{x^k}$. Moreover, if we add an edge~$ts$ with cost $-\theta$ to the network and extend $x^k$ in the natural way to a flow of value~$0$, then it is still a min-cost flow and, therefore, maximizes~\eqref{eq:ValueOfTemporallyRepeatedFlow} (by \Cref{lemma:PropertiesOfMinCostFlowForTemporallyRepeatedFlow}). Hence, according to \Cref{thm:TemporallyRepeatedFlowIsOpt}, the maximum value of a dynamic flow with time horizon~$\theta$ is 
		\[\theta\cdot\fvals{x^k}-\sum_{e \in E}\tau_e x^k_e.\]
	At the same time, the definition of the corresponding pseudo-flow~$f$ allows us to compute its value up to time~$\theta$ to be
		\begin{align*}
			\fval{f,\theta} 
				&= \sum_{k: \tau_{p_k} \leq \theta}(\theta-\tau_{p_k})x^k_{p_k} = \sum_{k=1}^{\bar k}(\theta-\tau_{p_k})x^k_{p_k} \\
				&= \theta\cdot\sum_{k=1}^{\bar k}x^k_{p_k} - \sum_{k=1}^{\bar k}\tau_{p_k}x_{p_k}^k = \theta\cdot\fvals{x^k}-\sum_{e \in E}\tau_e x^k_e.
		\end{align*}
	Therefore, $f$ is a maximum dynamic flow with time horizon~$\theta$. As this holds for any $\theta \leq T$, this proves that $f$ is an earliest arrival time flow.
\end{proof}

\begin{corollary}
	For any dynamic flow network with integer capacities\lukas{This certainly also holds for general capacities. But how to prove this formally?} and any time horizon, there exists an earliest arrival time flow. \qed
\end{corollary}

%\begin{proof}
%	By \Cref{thm:MinCostFlowExistence} there always exists a min-cost flow. As it has a (finite) path decomposition, there is always a way of choosing path such that SSP terminates. Thus, \Cref{alg:EarliestArrival} can always terminate and existence follows from \Cref{thm:EarliestArrivalAlgCorrect}.\lukas{Incomplete!}
%\end{proof}

\subsection*{Notes}

This \namecref{chap:DFFixedTravelTimes} is mostly based on \cite{SkutellaSurvey} by \citeauthor{SkutellaSurvey}.