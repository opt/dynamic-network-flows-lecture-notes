%!TEX root = ../dynamic-network-flows.tex
% !TeX spellcheck = en_GB

\section{Additional Theorems and Definitions}

\subsection{Some Basic Topology and Analysis}

The extreme value theorem states that on a non-empty, compact set every continuous (real-valued) function attains both its minimum and its maximum (see \eg \cite[Corollary 2.35]{InfiniteDimensionalAnalysis}):

\begin{theorem}[Extreme Value Theorem]\label{thm:ExtremeValueTheorem}
	Let $f: K \to \IR$ be a continuous function and $K$ a non-empty, compact set. Then there exist $x^*,x_* \in K$ with $f(x^*) = \sup\set{f(x) | x \in K}$ and $f(x_*) = \inf\set{f(x) | x \in K}$.
\end{theorem}

\begin{definition}
	A function $f: \IR \to \IR$ is 
	\begin{itemize}
		\item \emph{continuous} if for any $x \in \IR$ and  any $\eps > 0$ there exists some $\delta > 0$ such that 
			\[\abs{x-y}\leq \delta \implies \abs{f(x)-f(y)} \leq \eps \text{ for all } y \in \IR.\]
		\item \emph{uniformly continuous} if for any $\eps > 0$ there exists some $\delta > 0$ such that 
			\[\abs{x-y}\leq \delta \implies \abs{f(x)-f(y)} \leq \eps \text{ for all } x,y \in \IR.\]
		\item \emph{absolutely continuous}\dindex{absolute continuity} if for any $\eps > 0$ there exists some $\delta > 0$ such that
			\[\sum_{i=1}^n\abs{b_i-a_i} \leq \delta \implies \sum_{i=1}^n\abs{f(b_i)-f(a_i)}\]	
			for any finite sequence of pairwise disjoint intervals $(a_i,b_i) \in \IR$.
		\item \emph{Lipschitz-continuous}\dindex{Lipschitz-continuity} if there exists some constant $L\geq 0$ such that 
			\[\abs{f(x)-f(y)} \leq L\cdot\abs{x-y} \text{ for all } y \in \IR.\]
	\end{itemize}
\end{definition}

\begin{observation}
	The four continuity types are related to each other as follows:
		\begin{center}
			Lipschitz-continuous $\implies$ absolutely continuous $\implies$ uniformly continuous $\implies$ continuous.
		\end{center}
\end{observation}

The fundamental theorem of calculus states that integration and differentiation are inverse operations on the sets of locally integrable or absolutely continuous functions, respectively (\cf \cite[Theorem 4.4.2 and Corollary 4.4.4]{BSRealAndFunctionalAnalysis}, \cite[Theorem 11.9.1]{TaoAnalysisI}):

\begin{theorem}[Fundamental Theorem of Calculus]\label{thm:FundamentalTheoremOfCalculus}
	Let $f: [a,b] \to \IR$ be a locally integrable function. Then the function $F: [a,b] \to \IR, x \mapsto \int_a^x f(z)\diff z$ is absolutely continuous and satisfies
		\[\deriv{F}(x) = f(x) \text{ for almost all } x \in [a,b].\]
	Moreover, $\deriv{F}(x) = f(x)$ holds for all $x \in [a,b]$ where $f$ is continuous.
	
	Let $F: [a,b] \to \IR$ be an absolutely continuous function. Then, it is differentiable almost everywhere and satisfies
		\[F(x) = F(a) + \int_a^x \deriv{F}(z)\diff z \text{ for all } x \in [a,b].\] 
\end{theorem}

\begin{lemma}[{\cite[Lemma 8]{CCLDynEquil}}]\label{lemma:LocIntFunctions-VanishingOnIntervals}
	Let $f: \IR \to \IRnn$ be any non-negative locally integrable function and $\set{[a_k,b_k)}_{k \in K}$ be any family of half open intervals (possibly uncountably many). Then the following two properties are equivalent
	\begin{enumerate}[label=\alph*)]
		\item $f$ vanishes almost everywhere on $[a_k,b_k)$ for every $k \in K$ (separately!),
		\item $f$ vanishes almost everywhere on $\bigcup_{k \in K}[a_k,b_k)$.
	\end{enumerate}
\end{lemma}

\begin{lemma}[{$\approx$ \cite[Lemma 9]{CCLDynEquil}}]\label{lemma:ACFunctions-CharacterizationNonNegativity}
	Let $G: \IR \to \IR$ be any absolutely continuous function with $G(0) = 0$. Then the following conditions are equivalent:
	\begin{enumerate}[label=\alph*)]
		\item $\forall \theta \in \IRnn: G(\theta) \geq 0$,\label[thmpart]{lemma:ACFunctions-CharacterizationNonNegativity:NonNegative}
		\item For almost all $\theta \in \IRnn$ we have $G(\theta) < 0 \implies \deriv{G}(\theta) \geq 0$.\label[thmpart]{lemma:ACFunctions-CharacterizationNonNegativity:DerivNN}
		\item For almost all $\theta \in \IRnn$ we have $G(\theta) \leq 0 \implies \deriv{G}(\theta) = 0$.\label[thmpart]{lemma:ACFunctions-CharacterizationNonNegativity:DerivEqZero}
	\end{enumerate}	
\end{lemma}

\begin{lemma}\label{lemma:ConvergenceInCompactSpace}\lukas{Shown in an exercise}
	Let $X$ be a sequentially compact set (\ie every sequence has a convergent subsequence), $(x_n)_{n \in \INo}$ a sequence in~$X$ and $x \in X$ some point. Then, the following two statements are equivalent:
	\begin{enumerate}[label=\alph*)]
		\item $x_n$ converges to $x$.
		\item Every convergent subsequence of $(x_n)$ converges to $x$.
	\end{enumerate}
\end{lemma}

\begin{lemma}\label{lemma:PointwiseConvergenceOfConcOfUniformlyConvergentFunctions}\lukas{Shown in an exercise?}
	Let $F_n: \IR \to \IR$ and $G_n: \IR \to \IR$ be two sequences of functions converging uniformly to some function $F: \IR \to \IR$ and $G: \IR \to \IR$, respectively. Then, the sequence $F_n \circ G_n$ converges uniformly to $F\circ G$.
\end{lemma}

\begin{lemma}\label{lemma:DifferentiationRuleForMinima}\lukas{Maybe proof this in an exercise?}
	Let $F_1, \dots, F_n: \IR \to \IR$ be a finite set of piecewise linear, continuous functions. Then, the function
		\[G: \IR \to \IR, \theta \mapsto \min\set{F_1(\theta),\dots,F_n(\theta)}\]
	is  piecewise linear and continuous as well and satisfies
		\[\rDeriv{G}(\theta) = \min\set{\rDeriv{F_i}(\theta) | i \in [n]: F_i(\theta)=G(\theta)} \text{ for almost all } \theta \in \IR.\] 
\end{lemma}

A similar statement for general almost everywhere differentiable functions can be found in \cite[Lemma 2.3]{SeringThesis}.

\subsection[The Lp-Spaces]{The $L^p$-Spaces}

The function
	\[\mu^\ast: \PSet{\IR} \to \IRInn, A \mapsto \inf\Set{\sum_{k=1}^\infty(b_k-a_k) | a_k,b_k \in \IR: A \subseteq \bigcup_{k=1}^\infty(a_k,b_k)}\]
defines the outer Lebesgue measure on $\IR$. A subset $A \subseteq \IR$ is called \demph{measurable} if it satisfies $\mu^\ast(A) = \mu^\ast(A\cap B) + \mu^\ast(A\setminus B)$ for all $B \subseteq \IR$ and a \demph{null set}\dindex{null set} if it satisfies $\mu^\ast(A)=0$. The restriction of $\mu^\ast$ to only the measurable subsets of $\IR$ is called the \demph{Lebesgue measure}\dindex{Lebesgue measure}. 

\begin{proposition}
	The Lebesgue measure is a measure, \ie it assigns the empty set measure zero and satisfies countable additivity. Moreover, any null set is measurable, any countable set is a null set and any countable union of null sets is a null set again.
\end{proposition}

A function $f: \IR \to \IR$ is called \emph{measurable} if for any $M \in \IR$ the set $\set{\theta \in \IR | f(\theta) \leq M}$ is measurable. Two functions $f,g: \IR \to \IR$ are called \emph{equal almost everywhere} (written $f \eqae g$) if they only differ on a null set.

\begin{definition}\label{def:Lp}
	Let $J \subseteq \IR$ be any interval, $p \in [1,\infty)$ and $f: J \to \IR$ some measurable function. We say that $f$ is \emph{locally $p$-integrable} if it satisfies $\int_C \abs{f(\theta)}^p\diff\zeta < \infty$ for every compact subset $C \subseteq J$. Moreover, we define 
		\[\norm{f}_p \coloneqq \Big(\int_J\abs{f(\theta)}^p\diff\theta\Big)^{\nicefrac{1}{p}}\]
	and call $f$ \emph{$p$-integrable} if $\norm{f}_p < \infty$.
	
	We also define 
		\[\norm{f}_\infty \coloneqq \inf\Set{M \in \IRnn | \set{\theta \in J | \abs{f(\theta)}>M} \text{ is a null set}}.\]
		
	For any $p \in [1,\infty]$ we denote by $L^p(J)$ the space of all measurable functions $f: J \to \IR$ with $\norm{f}_p < \infty$, where we consider two functions to be equal if they are equal almost everywhere.
\end{definition}

\begin{proposition}
	For any $p \in [1,\infty]$, the space $\pInt[p][J]$ together with $\norm{.}_p$ is a Banach space, \ie a vector space which is complete with respect to the norm $\norm{.}_p: \pInt[p][J] \to \IRnn$. 
\end{proposition}

\begin{definition}
	Let $(X,\norm{.})$ be a normed (real) vector space. Then its \emph{dual space}\dindex{dual space} is $(X',\norm{.}')$ where
		\[X' \coloneqq \set{\varphi: X \to \IR | \varphi \text{ continuous and linear}}\]
	is the space of continuous linear functionals on $X$ and 
		\[\norm{.}': X' \to \IRnn, \varphi \mapsto \sup\set{\varphi(x) | x \in X, \norm{x}\leq 1}\]
	the dual norm.
	
	The space $(X,\norm{.})$ is called \emph{reflexive}\dindex{reflexive space} if the canonical inclusion of $X$ into its double dual
		\[X \to X'', x \mapsto (X' \to \IR, \varphi \mapsto \varphi(x))\]
	is surjective.
\end{definition}

\begin{proposition}
	For any $p \in [1,\infty)$ the dual of $\pInt[p][J]$ can be identified with $\pInt[q][J]$
		\[\pInt[q][J] \overset{\cong}{\to} \pInt[p][J]', (g: J \to \IR) \mapsto \Big(\pInt[p][J] \to \IR, f \mapsto \int_J f(\zeta)g(\zeta)\diff\zeta\Big),\]
	where $q \in (1,\infty]$ is chosen such that $\frac{1}{p}+\frac{1}{q}=1$.
	
	In particular, $\pInt[p][J]$ is reflexive for all $p \in (1,\infty)$.
\end{proposition}


\subsection{Some Functional Analysis}

\begin{definition}
	Let $(X,\norm{.})$ be a normed space. Then, the \emph{weak topology}\dindex{weak topology} on $X$ is the topology generated by the sets
		\[U(\varphi,x,\eps) \coloneqq \Set{y \in X | \Abs{\varphi(x) - \varphi(y)} < \eps}\]
	for $x \in X, \varphi \in X', \eps \in \IRp$.
	
	In particular, the weak topology on $\pInt[p][J]$ for $p \in [1,\infty)$ is generated by the sets
		\[U(g,f,\eps) \coloneqq \Set{f' \in \pInt[p][J] | \Abs{\int_J f(\zeta)g(\zeta)\diff\zeta - \int_J f'(\zeta)g(\zeta)\diff\zeta} < \eps}\]
	for $f \in \pInt[p][J], g \in \pInt[q][J], \eps \in \IRp$.
\end{definition}

\begin{lemma}[{\cite[Section 5.14]{InfiniteDimensionalAnalysis}}]
	The weak topology is a Hausdorff topology.
\end{lemma}
	
An important consequence of the Banach--Alaoglu Theorem is the following result for compactness with respective to the weak topology in reflexive spaces (\cf \cite[Theorem 6.25]{InfiniteDimensionalAnalysis}):
\begin{theorem}\label{thm:UniBallWeaklyClosedIffReflexive}
	Let $X$ be a Banach space. Then, the closed unit ball $\ballClosed{0} \subseteq X$ is weakly compact iff $X$ is reflexive.
\end{theorem}

The Eberlein–Šmulian Theorem (\cite[Theorem 6.34]{InfiniteDimensionalAnalysis}) states that in normed spaces weak compactness and weak sequential compactness are the same:
\begin{theorem}\label{thm:WeakCompactnessIfWeakSequentialCompactness}
	Let $X$ be a normed space and $K \subseteq X$ some subset. Then, $K$ is weakly compact iff it is weakly sequentially compact.
\end{theorem}

\begin{definition}
	Let $\Gamma: X \to \PSet{X}$ be a mapping from some set $X$ to its power set.\footnote{Such a mapping is also called a \emph{correspondence}.} Then, we denote the \emph{graph of $\Gamma$} by
		\[\graph{\Gamma} \coloneqq \set{(x,y) \in X \times X | y \in \Gamma(x)} \subseteq X \times X.\]
	We call $x \in X$ a \emph{fixed point of $\Gamma$} if it satisfies $x \in \Gamma(x)$.	
	
	If $X$ is a topological space we say that $\Gamma$ has a closed graph if $\graph{\Gamma} \subseteq X \times X$ is a closed subset (\wrt the product topology).
\end{definition}

The Theorem of Kakutani, Fan and Glicksberg provides sufficient conditions for the existence of such a fixed point:
\begin{theorem}[{\cite[Corollary 17.55]{InfiniteDimensionalAnalysis}}]\label{thm:KFGFixedPoint}
	Let $X$ be a locally convex Hausdorff space, $K \subseteq X$ a non-empty, convex and compact subset and $\Gamma: K \to \PSet{K}$ a mapping with closed graph and $\Gamma(x) \subseteq X$ non-empty and convex for all $x \in K$.
	
	Then, the set $\set{x \in K | x \text{ a fixed point of } \Gamma}$ is non-empty and compact. 
\end{theorem}

A closely related theorem is the following existence result for solutions of certain (infinite dimensional) variational inequalities:

\begin{definition}\label{def:DualPairing}
	For any $p,q > 1$ with $\frac{1}{p}+\frac{1}{q}=1$, any interval $[a,b] \subseteq \IR$ and any $d \in \INs$ the \emph{dual pairing} between $\pInt[p][[a,b]]^d$ and $\pInt[q][[a,b]]^d$ by
		\[\langle.,.\rangle: \pInt[p][[a,b]]^d \times \pInt[q][[a,b]]^d \to \IR, (f_i,g_i) \mapsto \sum_{i=1}^d\int_a^b f_i(\zeta)g_i(\zeta)\diff\zeta.\]
\end{definition}

\begin{theorem}[{\cite[Chapitre 2, Théorème 8.1]{Lions}/\cite[Proof of Theorem 4.2]{ZhuMarcotteLinearEdgeDelays}}]\label{thm:VISolutionExistence}\tindex{variational inequality}
	Let $C \subseteq \pInt[q][[a,b]]^d$ be a non-empty, closed, convex and bounded subset and $\mathcal{A}: C \to \pInt[p][[a,b]]^d$ a weak-strong sequentially continuous mapping. Then, there exists a solution $f \in C$ to the following variational inequality
	\begin{align}\tag{Lions}\label{eq:VILionis}
		\langle\mathcal{A}(f),g-f\rangle \geq 0 \quad\text{ for all } g \in C.
	\end{align}
\end{theorem}


\subsection{Some Graph Theory}

\begin{definition}
	Let $G=(V,E)$ be a directed graph. $G$ is called \emph{acyclic}\dindex{graph!acyclic} if it does not contain any cycles. A \emph{topological order}\dindex{topological order} is an order~$\prec$ on the node set $V$ such that we have $v \prec w$ for every edge $vw \in E$.
\end{definition}

\begin{lemma}[{\cite[Theorem 2.6.3]{JungnickelGraphTheory}}]
	A (directed) graph is acyclic if and only if it admits a topological order.
\end{lemma}