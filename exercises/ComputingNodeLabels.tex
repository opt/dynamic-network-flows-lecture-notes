% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{ComputingNodeLabels}

\exTitle{Computing node labels}

\exProblem{We consider the following algorithm for computing node labels in \stnetwork s with edge costs $\gamma \in \IR^E$:
	
\begin{algorithm}[H]
	\caption{Naive node label computing}\label{alg:NodeLabelComputing}
	
	\Input{An \stnetwork{} $\network=(G,s,t,\nu,\gamma)$}
	\Output{A vector $\pi \in \IRI^V$}
	
	Start with $\pi_s \coloneqq 0$ and $\pi_w \coloneqq \infty$ for all $w \in V\setminus\set{s}$.
	
	\While{$\exists e=vw \in E$ with $\pi_w > \pi_v + \gamma_{vw}$}{ 
		set $\pi_w \coloneqq \pi_v + \gamma_{vw}$
	}
	
	\KwRet{$\pi$} 
\end{algorithm}

Prove the following statements about this algorithm:

\begin{enumerate}[label=\alph*)]
	\item If the algorithm terminates and the returned vector satisfies $\pi_s=0$, then $\pi$ is a solution to the Bellman equations~\eqref{eq:BellmanEquations}.
	
	\item If $N$ does not contain any cycle of negative cost and the algorithm terminates, then $\pi$ is a \emph{maximal} solution to the Bellman equations~\eqref{eq:BellmanEquations}.
	
	\textit{Hint:} Make use of the following invariant: If $\pi_w < \infty$, then there exists some \stwalk[s][w]~$p$ with $\gamma_p=\pi_w$.
	
	\item If $N$ does not contain any cycle of negative cost, then the algorithm terminates.
	
	\item If $N$ does not contain any cycle of negative cost, then the algorithm can always terminate after $\abs{E}$ iteration (when choosing the edges correctly).
	
	\item There exist networks without cycles of negative cost where the algorithm takes $2^{\abs{E}/2}$ iteration until it terminates (when choosing the edges poorly).
	
	\textit{Hint:} Consider a network consisting of $\abs{E}/2$ consecutive pairs of parallel edges.
\end{enumerate}
}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item We observe that during the execution of the algorithm the following invariant always holds
		\begin{align*}
			\pi_w \geq \inf\set{\pi_v + \gamma_{vw} | vw \in \edgesTo{w}} \text{ for all } w \in V \setminus \set{s}.
		\end{align*}
		At the start this is trivially fulfilled. After that, we only ever reduce the values of $\pi_w$ meaning that this cannot create a violation of the invariant at another node. Additionally, the way we reduce $\pi_w$ automatically ensures that the invariant stays true at~$w$.
		
		Thus, the invariant still holds at the end of the algorithm, while termination also implies $\pi_w \leq \pi_v + \gamma_{vw}$ for all $vw \in E$. Together, this implies
			\[\pi_w = \inf\set{\pi_v + \gamma_{vw} | vw \in \edgesTo{w}} \text{ for all } w \in V \setminus \set{s}.\]
		Hence, $\pi$ is a solution to~\eqref{eq:BellmanEquations}.
					
		\textit{Remark:} It turns out that the assumption $\pi_s=0$ is not necessary in this exercise. This is because if the algorithm terminates, we automatically have $\pi_s=0$ (why?).
	
	\item We start by proving the invariant given in the hint: At the start, this is clearly true (as $s$ is the only node with finite label and there always exists an \stpath[s][s]). Now, whenever the algorithm changes the value of some $\pi_w$, this can only happen because the start node of the edge $vw \in \edgesTo{w}$ used for this satisfies $\pi_v < \infty$. According to our invariant, this implies that there exists an \stwalk[s][v] $p$ with $\gamma_p=\pi_v$. Consequently, after the change of $\pi_w$ the walk~$p,vw$ is a \stwalk[s][w] with $\gamma_{p,vw}=\gamma_p+\gamma_{vw} = \pi_v + \gamma_{vw} = \pi_w$.
	
	Since the network does not contain any cycles with negative cost, the invariant directly implies
		\[\pi_w \geq \inf\set{\gamma_p | p \text{ a \stwalk[s][w]}} = \inf\set{\gamma_p | p \text{ a \stpath[s][w]}} = \ell_w\]
	for all $w$ reachable from $s$. Hence, $\pi$ is a solution to~\eqref{eq:BellmanEquations} while at the same time being larger (or equal) to $\ell$ which is the \emph{maximal} solution to~\eqref{eq:BellmanEquations} (by \Cref*{lemma:PropertiesOfNodeLabels:MaxSolutionToBE}). This implies $\pi=\ell$.
	
	\item By the invariant from b) every time the algorithm changes the value of some $\pi_w$, this implies the existence of an additional $\stwalk[s][w]$ with strictly lower cost than the one from the previous time $\pi_w$ was changed. However, since there are no cycles of negative cost, this can only happen a finite number of times.\lukas{Explain this in more detail?} This implies that the algorithm must terminate eventually.
	
	\item Since there are no negative cycles there must be a tree of efficient edges rooted in $s$ and spanning all nodes reachable from $s$ (\cf exercise 4a)).\lukas{Explain this better/independent of the referenced exercise?} Checking the edges of this tree in exactly the order they appear on this tree means that every value $\pi_w$ is changed exactly once and immediately gets its correct value. Thus, the algorithm needs $\abs{V}-1 \leq \abs{E}$ iterations.
	
	Note that this is very similar, to what Dijkstra's algorithm for computing shortest paths does.
	
	\item We construct recursively for any even $m$ a network with $m$ edges such that \Cref{alg:NodeLabelComputing} may need $2^{m/2+1}-2$ iterations. We start with the following $2$-edge network:
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex] (s) at (0,0) {$s$};
				\node[namedVertex] (t) at (3,0) {$t$};
				
				\draw[edge] (s) to[bend left=20]node[above]{$2^0$} (t);
				\draw[edge] (s) to[bend right=20]node[below]{$0$} (t);
			\end{tikzpicture}
		\end{center}
	In this network\Cref{alg:NodeLabelComputing} uses $2=2^2-2 = 2^{2/2+1}-2$ iterations if first checking the upper and then the lower edge. 
	
	For $m=4$ edges we add two parallel edges at the beginning of the previous network to get
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex] (s) at (0,0) {$s$};
				\node[namedVertex] (v) at (3,0) {$v$};
				\node[namedVertex] (t) at (6,0) {$t$};
				
				\draw[edge] (s) to[bend left=20]node[above]{$2^1$} (v);
				\draw[edge] (s) to[bend right=20]node[below]{$0$} (v);
				\draw[edge] (v) to[bend left=20]node[above]{$2^0$} (t);
				\draw[edge] (v) to[bend right=20]node[below]{$0$} (t);
			\end{tikzpicture}
		\end{center}
	Checking the edges in the order: upper $sv$, upper $vt$, lower $tv$, lower $sv$, upper $vt$, lower $vt$ gives us a sequence of $6=2^{4/2+1}-2$ iterations. 
	
	Now, generalizing this process we can extend such a network $\network^m$ with $m$ edges enabling $2^{m/2+1}-2$ iterations to a network with $m+2$ edges and the possibility of $2\cdot (2^{m/2+1}-2)+2 = 2^{(m+2)/2+1}-2$ iterations as follows:
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex] (s) at (0,0) {$s$};
				\node[namedVertex] (v) at (3,0) {$v$};
				\node 	               at (5.5,0) {$\dots$};
				\node 	               at (5.5,.5) {$\network^m$};
				\node[namedVertex] (t) at (8,0) {$t$};
				
				\draw[rounded corners=.3cm] ($(v)+(-.5,.8)$) rectangle ($(t)+(.5,-.5)$);
				
				\draw[edge] (s) to[bend left=20]node[above]{$2^{m/2-1}$} (v);
				\draw[edge] (s) to[bend right=20]node[below]{$0$} (v);
			\end{tikzpicture}
		\end{center}
\end{enumerate}}

\exComment{}