% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{QuickestTransshipment}

\exTitle{The Quickest Transshipment Problem}

\exProblem{In this exercise we consider networks with multiple sources and sinks with fixed supply/demand, \ie we have a network $\network=(G,S,T,\nu,\tau,d)$ with a set $S \subseteq V$ of source nodes and a set $T \subseteq V$ of sink nodes. Moreover, we have a vector $d \in \IR^V$ with 
	\[d_v \begin{cases}
			> 0, &\text{ for } v \in S \\
			< 0, &\text{ for } v \in T \\
			= 0, &\text{ else}
		\end{cases}\]
denoting the given supply/demand of each node. Our goal is now to compute flows satisfying these supplies and demands.

At first, we consider the static version of this problem, \ie we want to find a static flow $x \in \IRnn^E$ respecting all edge capacities and satisfying
	\[\sum_{e \in \edgesFrom{v}}x_e - \sum_{e \in \edgesTo{v}}x_e = d_v \text{ for all } v \in V.\]
\begin{enumerate}[label=\alph*)]
	\item How can we determine whether any given network admits such a static flow?
	\item Describe an algorithm for computing such a flow with minimum cost (considering $\tau_e$ as costs) if it exists.
\end{enumerate}

We now want to look at the dynamic version of the same problem, \ie whether there exists a dynamic flow satisfying all supplies/demands.
\begin{enumerate}[label=\alph*),resume]
	\item Give a formal definition of such a dynamic multi-source multi-sink flow.
	
		\textit{Hint:} The overall goal of this exercise is to generalize the quickest flow problem to multiple sources/sink (where the ``quickest''-part will come in part e)).
	\item How can we determine whether a given network has such a dynamic flow?
	\item Does your approach for the static flow problem in b) translate to an algorithm for the dynamic version where we want to minimize the time it takes to satisfy all supplies/demands (\ie similar to how we were able to solve the quickest flow problem by reducing it to a static min-cost flow problem)?
	
	You do not need to provide a fully formal answer here.
\end{enumerate}
}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item Add a super-source $s$ and a super-sink $t$ to the network as well as edges $sv$ with cost~$0$ and capacity $d_v$ for all $v \in V$ and edges $vt$ with cost~$0$ and capacity~$-d_v$ for all $v \in T$. Now compute a maximum \stflow{} in this \stnetwork{} with $s$ as its only source and $t$ as its only sink. If this flow saturates all edges $sv$ and $vt$, then its restriction to the original network is a multi-source multi-sink flow satisfying all supplies/demands.
	
	\item Use the same construction as before and compute a min-cost flow of value $\sum_{v \in S}d_v$. Note that this is the same approach taken in exercise~4 on exercise sheet~3.
	
	\item We are looking for a dynamic flow $f=(f^+_e,f^-_e)$ with some time horizon $T$ such that
		\begin{itemize}
			\item $f$ respects edge capacities,
			\item $f$ satisfies weak flow conservation on all edges,
			\item $f$ satisfies weak flow conservation at all nodes in $V\setminus S$,
			\item $f$ has its support in $[0,T]$,
			\item $f$ satisfies
				\[\sum_{e \in \edgesTo{v}}F^-_e(T)-\sum_{e \in \edgesFrom{v}}F^+_e(T)=d_v \text{ for all } v \in V\]
			\item and we have $F^+_e(T)=F^-_e(T)$ for all $e \in E$.
		\end{itemize}
	
	\item Set all edge capacities to $\infty$ and consider the static version of the problem. This now has a solution if and only if the dynamic version has a solution.
	
	\item Introducing a super-source/super-sink does not work in the dynamic setting as we cannot control anymore how much flow goes through each of the original sources/sinks then: This is, because in the dynamic setting the amount of flow being able to travel from the super-source to an original source now depends not only on the capacity of the respective edge but also on the time horizon available -- but this in turn depends on the chosen paths and, thus, cannot be known beforehand (as well as being different for different paths).
\end{enumerate}}

\exComment{The static version was essentially already solved in \exref{ModellingTransportationWithMinCost}, part d) is also \cite[Exercise 6.3]{SkutellaSurvey}}