% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{MaxDynamicFlowExistenceByArzelaAscoli}

\exTitle{Existence of maximum dynamic flows via Arzelà–Ascoli}

\exProblem{For any interval $[a,b] \subseteq \IR$ we denote by $C([a,b])$ the set of continuous functions from~$[a,b]$ to~$\IR$. Together with the norm $\norm{.}_\infty: C([a,b]) \to \IRnn, f \mapsto \sup\set{f(\theta) | \theta \in [a,b]}$ this becomes a normed vector space. A subset $K \subseteq C([a,b])$ is called \emph{uniformly equicontinuous} if for every $\eps > 0$ there exists a $\delta > 0$ such that we have
	\[\abs{\theta-\zeta} < \delta \implies \abs{f(\theta)-f(\zeta)} < \eps \text{ for all } f \in K, \theta,\zeta \in [a,b].\]
	
The Arzelà–Ascoli theorem now states:

\begin{quote}
	A set $K \subseteq C([a,b])$ is compact iff it is closed, bounded and uniformly equicontinuous (all with respect to the topology induced by $\norm{.}_\infty$).
\end{quote}

Use this theorem to provide an alternative proof for the existence of maximum dynamic flows.

\textit{Hint:} Consider the cumulative flow functions $F^+_e$ instead of the flow rates $f^+_e$. It is okay to skip some more technical details.}

\exSolution{We start by defining for every edge~$e \in E$ the set
		\[K_e \coloneqq \Set{F^+_e \in C([0,T]) | \begin{array}{l}F^+_e \text{ non-negative and non-decreasing with } F^+_e(0)=0\\ F^+_e(\theta)-F^+_e(\zeta) \leq \nu_e(\theta-\zeta) \text{ \fa } 0 \leq \zeta \leq \theta \leq T\end{array}}.\]
	This set is clearly closed. It is also bounded since we have
		\[\norm{F^+_e}_\infty = F^+_e(T) \leq \nu_e(T-0)+F^+_e(0) = \nu_e\cdot T.\]
	Finally, it is uniformly equicontinuous as for any  $\eps > 0$ we have
		\[\abs{\theta-\zeta} < \frac{\eps}{\nu_e} \implies \abs{F^+_e(\theta)-F^+_e(\zeta)} \leq \nu_e\abs{\theta-\zeta}<\eps\]
	for all $F^+_e \in K_e$. Thus, all $K_e$ are compact by the Arzelà-Ascoli theorem. Thus, the set $\prod_{e \in E}K_e$ is compact as well and the set
		\[K \coloneqq \Set{(F^+_e) \in \prod_e K_e | \sum_{e \in \edgesTo{v}}F^+_{e}(\theta-\tau_e) \leq \sum_{e \in \edgesFrom{v}}F^+_e(\theta) \text{ \fa } v \neq s, \theta \in \IRnn} \]
	is compact as a closed subset of this set (for the last condition we assume $F^+_e$ to be extended constantly outside of $[0,T]$).
	
	As the mapping $\Phi: K \to \IRnn, (F^+_e) \mapsto \sum_{e \in \edgesTo{t}}F^+_e(T-\tau_e)-\sum_{e \in \edgesFrom{t}}F^+_e(T)$ is clearly continuous, we can apply the extreme value theorem to get a maximal element~$(F^+_e)$ of~$K$ (\wrt $\Phi$). 
	
	Since all $F^+_e$ are Lipschitz-continuous and, therefore, absolutely continuous, they are differentiable almost everywhere and we have integrable functions $f^+_e: [0,T] \to \IRnn$ such that $F^+_e(\theta) = \int_0^\theta f^+_e(\zeta)\diff$ for all $\theta \in [0,T]$ (using the fundamental theorem of calculus). 
	
	We now claim that $f\coloneqq (f^+_e,f^-_e)$ with $f^-_e$ defined by $f^-_e(\theta) \coloneqq f^+_e(\theta-\tau_e)$ (and both extended by zero outside of $[0,T]$) is a maximum dynamic flow.
	
	First of all, we get strong flow conservation on edges directly from the definition of $f^-_e$ and weak flow conservation at nodes from the definition of the set~$K$. To see that $f$ also respects capacities, let $\theta \in [0,T)$ be any time where $F^+_e$ is differentiable. Then, we have
		\[f^-_e(\theta+\tau_e) = f^+_e(\theta) = \deriv{F^+_e}(\theta) =  \lim_{h \searrow 0}\frac{F^+_e(\theta+h)-F^+_e(\theta)}{h} \leq \frac{\nu_e(\theta+h-\theta)}{h}=\nu_e.\]
	Hence, $f$ is a feasible dynamic flow. Moreover, we have $\Phi((F^+_e))= \fval{f,T}$.
	
	Now, let $g=(g^+_e,g^-)$ be any feasible dynamic flow. By \Cref*{obs:WlogStrongFlowConservationOnEdges} we can assume without loss of generality that $g$ satisfies strong flow conservation on edges and is zero outside of $[0,T]$. It is then easy to see that we have $(G^+_e) \in K$ as well as $\fval{g,T} = \Phi((G^+_e))$. The fact that $(F^+_e)$ was a maximal element of~$K$ then immediately implies $\fval{f,T}=\Phi((F^+_e))\geq\Phi((G^+_e))=\fval{g,T}$ which shows that $f$ is a maximum dynamic flow.
}

\exComment{A source for the Arzelà–Ascoli theorem in this form is \cite[Theorem 1.8.4]{BSRealAndFunctionalAnalysis}}