%!TEX root = ../exercise-sheet.tex

\exID{BraessParadoxon}

\exTitle{The Braess Paradoxon}

\exProblem{Consider the following two \stnetwork s with flow dependent edge costs:

\begin{center}
	\begin{tikzpicture}
		\node[namedVertex](s)at(0,0){$s$};
		\node[namedVertex](v)at(2.5,1.5){$v$};
		\node[namedVertex](w)at(2.5,-1.5){$w$};
		\node[namedVertex](t)at(5,0){$t$};
		
		\draw[edge] (s) --node[sloped,above]{$x \mapsto x$} (v);
		\draw[edge] (s) --node[sloped,below]{$x \mapsto 1$} (w);
		\draw[edge] (v) --node[sloped,above]{$x \mapsto 1$} (t);
		\draw[edge] (w) --node[sloped,below]{$x \mapsto x$} (t);
	\end{tikzpicture}
	\hspace{2em}
	\begin{tikzpicture}
		\node[namedVertex](s)at(0,0){$s$};
		\node[namedVertex](v)at(2.5,1.5){$v$};
		\node[namedVertex](w)at(2.5,-1.5){$w$};
		\node[namedVertex](t)at(5,0){$t$};
		
		\draw[edge] (s) --node[sloped,above]{$x \mapsto x$} (v);
		\draw[edge] (s) --node[sloped,below]{$x \mapsto 1$} (w);
		\draw[edge] (v) --node[sloped,above]{$x \mapsto 1$} (t);
		\draw[edge] (w) --node[sloped,below]{$x \mapsto x$} (t);
		\draw[edge] (v) --node[right]{$x \mapsto 0$} (w);
	\end{tikzpicture}
\end{center}

For each of them determine for every $v \in \IRnn$ all Wardrop equilibria of value $v$. Then also determine the total costs of each of those flows. Can you see any ``paradoxical'' effects here?}

\exSolution{For the network on the left there are only two paths ($sv,vt$ and $sw,wt$) which are disjoint from each other and have the ``same'' flow dependent path cost function. Thus, the only Wardrop equilibria are the flows that split equally between the two paths. The cost of such a flow of value $v \in \IRnn$ is then $2\cdot\frac{v}{2}\cdot(\frac{v}{2}+1)=\frac{v^2+2v}{2}$.

For the network on the right, there are three paths: The two paths from before as well as the additional zig-zag-path $sv,vw,wt$. Let $\alpha$ be the amount on the zig-zag path and $\beta$ the amount on the top path (thus, $v-\alpha-\beta$ is the amount on the bottom path). The (per particle) costs of the three paths is then:
\begin{itemize}
	\item path $sv,vw,wt$: $\alpha+\beta \,\,+\,\, 0 \,\,+\,\, \alpha+(v-\alpha-\beta) = v+\alpha$
	\item path $sv,vt$: $\alpha+\beta \,\,+\,\, 1$
	\item path $sw,wt$: $1 \,\,+\,\, \alpha+v-\alpha-\beta = v-\beta+1$
	\end{itemize}
We start by observing that regardless of whether the zig-zag path is used, in an equilibrium the other two path will always be used the same amount (including, possibly, not at all). Hence, we have $\beta=\frac{v-\alpha}{2}$. Now, there are three possibilities for an equilibrium flow:
\begin{description}
	\item[Case 1 ($\alpha = 0$):] Then, we must have
		\[v = v+\alpha \geq \alpha+\beta+1 = \frac{v}{2}+1.\]
		In other words: Splitting the flow equally between the two direct paths is a Wardrop equilibrium if and only if $v \geq 2$ and it has a (total) cost of $\frac{v^2+2v}{2}$.
	\item[Case 2 ($\beta = 0$):] Then, we must have $\alpha = v$ and
		\[2v = v+\alpha \leq \alpha+\beta+1 = v+1.\]
		Hence, sending all flow over the zig-zag path is a Wardrop equilibrium if and only if $v \leq 1$ and it has a cost of $2v^2$.
	\item[Case 2 ($\alpha,\beta > 0$):]  Then, we must have
		\[v+\alpha = \alpha+\beta+1 = \alpha + \frac{v-\alpha}{2}+1,\]
		which holds iff $v+\alpha=2$. Thus, for $v \in [1,2]$ sending $\alpha=2-v$ along the zig-zag-path while splitting the rest equally between the two direct paths results in a Wardrop equilibrium with cost of $v(v+\alpha)=2v$.
\end{description}
We observe that the right network is essentially the same network as the left one, just with one additional edge (edge~$vw$). Nevertheless, for $v \in (\frac{2}{3},2)$ the total cost (and even the individual cost of every single particle) in an equilibrium is larger in the right network compared to the left network. This shows that (within this model) adding additional edges may increase the overall cost (of an equilibrium). The plot below shows the total costs in an equilibrium for different values of~$v$ (in blue for the left network and in red for the right network):

\begin{center}
	\begin{tikzpicture}[declare function = {
			cl(\x) = (\x^2+2*\x)/2;
			cr(\x) = and(\x>=0,\x<1)*(2*\x^2) + and(\x>=1,\x<2)*(2*\x)+(\x>=2)*cl(\x);
		}]
		\begin{axis}[
			axis lines=left,,
			ymin=0,ymax=10.5,
			xmin=0,xmax=3.5,
			xtick={1,2,3},
			width=10cm, height=6.25cm]
			\addplot[red,thick,domain=0:3.5,samples=100]{cr(\x)};
			\addplot[blue,thick,domain=0:3.5,samples=100]{cl(\x)};
			\addplot[red,thick,dashed,domain=0:3.5,samples=100]{cr(\x)};
		\end{axis}		
	\end{tikzpicture}
\end{center}}

\exComment{}