% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{DynamicEquilibriaWithDepartureTimeChoice}

\exTitle{Dynamic Equilibria with Departure Time Choice}

\exProblem{In this exercise we want to take another look at dynamic equilibria in parallel link networks as defined in exercise~4 on exercise sheet~10. However, instead of a fixed network inflow \emph{rate}~$u: \IRnn \to \IRnn$, we now have a fixed network inflow \emph{volume}~$U \in \IRnn$ denoting the amount of particles that want to travel from~$s$ to~$t$.
\begin{enumerate}[label=\alph*)]
	\item Assume that each particle may freely choose both its route (which, in this case, is just one $s$,$t$-edge) \emph{and} its starting time, \ie the time at which it enters the network, within some given time horizon~$[0,T]$. Further assume that the only goal of the particles is to minimize their individual travel times (according to the dynamic of the Vickrey queuing model). Find a suitable definition of an equilibrium flow in this model.
	
	\item Are these equilibria guaranteed to exist? You do not have to provide a full proof here. Moreover, you may assume (without proof) that there exists some constant $M \in \IRnn$ such that \wlg it suffices to only consider flows with edge-inflow rates of at most~$M$.
	
	\item Show that, if particles want to minimize their arrival time instead of their travel time, equilibria are not guaranteed to exist anymore.
	
	\textit{Bonus:} Do you see which part of the existence proof from b) fails here?
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item We define the set of feasible flows by
		\[\Lambda(U) \coloneqq \Set{(f^+_e,f^-_e) \in \pInt[2]([0,T])^{E\times\set{\pm}} | (f^+_e,f^-_e) \text{ a Vickrey flow}, \sum_{e \in E}\int_0^T f^+_e(\zeta)\diff\zeta = U}.\]
	Then, a flow $f \in \Lambda(U)$ is a dynamic equilibrium with departure time choice if we have
		\[f_e(\theta) > 0 \implies \cexittime[e](\theta) - \theta \leq \cexittime[e'](\theta') - \theta' \text{ for all } e,e \in E \text{ and almost all } \theta,\theta' \in [0,T].\]
		
	\item With a proof very similar to the one for dynamic equilibria with fixed departure time, one can show that the equilibrium definition from~a) is equivalent to being a solution to the following variational inequality
		\[\sum_{e \in E}\int_0^T(\cexittime(\zeta)-\zeta)(g^+_e(\zeta)-f^+_e(\zeta))\diff\zeta \geq 0 \text{ for all } g \in \Lambda(U).\]
	Under the additional assumption that we do not have to consider flows with inflow rates larger then~$M$, we can even replace $\Lambda(U)$ by $\set{f \in \Lambda(U) | f^+_e \leq M}$. This set is non-empty, closed, bounded and convex. Moreover, the mapping from $f$ to $\cexittime$ is weak-strong sequentially continuous. Hence, there always exists a solution to the above variational inequality (by \Cref*{thm:VISolutionExistence}) and, therefore, a dynamic equilibrium with departure time choice.
	
	\item We obtain a definition for equilibrium in this setting from the one in part~a) by changing $\cexittime(\theta)-\theta$ to just $\cexittime(\theta)$. This new type of equilibrium is not guaranteed to exists anymore as can be seen already in an instance with a single edge and a flow volume $U>0$. Let $f \in \Lambda(U)$ be any flow. Then, we have $\cexittime(0)=\tau_e < \cexittime(\theta)$ for all $\theta > 0$. But, at the same time, there must be (a set of positive measure of) particles entering the network after time $0$, meaning that $f$ does not satisfy the equilibrium condition.
	
	The reason we cannot show existence of equilibria in the same way as in part~b) is that the assumption that all edge inflow rates (of relevant flows) are bounded by some constant~$M$ does not hold anymore. But without this assumption $\Lambda(U)$ is not bounded as it contains, \eg the sequence $(f^{+,n}_e,f^{-,n}_e)$ with 
		\[f^{+,n}_e: [0,1] \to \IR, \theta \mapsto \begin{cases}
			(1-\theta)\cdot n,	&\text{ if } \theta \leq \tfrac{1}{n}\\
			0,					&\text{ else}
		\end{cases}.\]
	which is unbounded as we have $\int_0^1\abs{f^{+,n}_e(\zeta)}^2\diff\zeta \geq \int_0^{\nicefrac{1}{2n}}\left(\tfrac{n}{2}\right)^2\diff\zeta = \tfrac{n}{8} \to \infty$. 
\end{enumerate}}

\exComment{This builds on \exref{DynamicEquilibriaInParallelLinkNetworks}}