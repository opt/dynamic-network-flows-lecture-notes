% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{EfficientPathsFromNodeLabels}

\exTitle{Deducing efficient paths from node labels}

\exProblem{Let $\network$ be a network with edge costs and assume that we are given the corresponding node labels $\ell \in \IR^E$.
	\begin{enumerate}[label=\alph*)]
		\item If there are no cycles of negative length\lukas{It might be better to assume strictly positive costs for all cycles. Then we can use the usual backwards path reconstruction.} in $\network$ and $w \in V$ is some node with $\ell_w < \infty$, how can we find an efficient $\stpath[s][w]$ (in linear time)?
		
		\item If there is at least one cycle of negative length reachable from $s$, how can we find such a cycle?
	\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item The basic idea is to construct the path backwards starting at~$w$. If all cycles have strictly positive cost, we can do this by taking any efficient edge $vw$ leading towards $w$, then any efficient edge $v'v$ leading to $v$ and so on until we reach~$s$. 
		
		However, if we have cycles of cost~$0$, this naive approach might lead us into a cycle (why?). Thus, for general setting we need a bit of preprocessing: More precisely, we construct a tree rooted in~$s$ and spanning all reachable nodes such that for any such node~$w$ the unique \stpath[s][w] in this tree is also an efficient path. 
		
			\begin{algorithm}[H]				
				\Input{An \stnetwork{} $\network=(G,s,t,\nu,\gamma)$ with corresponding node labels $\gamma \in \IR^V$ and some node $w \in V$}
				\Output{An efficient \stpath[s][w] $p$}
				
				\tcc{tree construction:}
				
				set $Q \coloneqq \set{}$ and $\mathrm{pred}(v) \coloneqq \begin{cases} \bot, &\text{ for } v \neq s\\s, &\text{ else}\end{cases}$ 
				
				\While{$Q \neq \emptyset$}{ 
					pick some $v \in Q$ and delete it from $Q$
					
					\ForEach{efficient edge $vv' \in \edgesFrom{v}$}{
						\If{$\mathrm{pred}(v') = \bot$}{
							set $\mathrm{pred}(v') \coloneqq v$ and add $v'$ to $Q$
						}
					}
				}
								
				\tcc{path reconstruction:}
				
				set $p \coloneqq ()$, $v \coloneqq w$
				
				\While{$v \neq s$}{
					$v' \coloneqq \mathrm{pred}(v)$
					
					add $v'v$ at the front of $p$
					
					$v \coloneqq v'$
				}
				
				\KwRet{$p$} 
			\end{algorithm}
			
			The correctness of this algorithm follows from the following observations:
			
			\begin{enumerate}[label=(\roman*)]
				\item Every node is added to $Q$ at most once.
				
				\item At any point during the first while-loop we have $\mathrm{v} = \bot$ iff $v$ has not been in $Q$ yet.
				
				\item At any point during the first while-loop for any $v \in V$ with $\mathrm{v} \neq \bot$ the sequence of $(v,\mathrm{pred}(v)), (\mathrm{pred}(v),\mathrm{pred}(\mathrm{pred}(v))), \dots$ is the reverse of an \stpath[s][v] consisting only of efficient edges (\ie an efficient \stpath[s][v] by \Cref*{lemma:PropertiesOfNodeLabels:PathEfficientIffEdgesEfficient}). 
				
				\item If a node $v \in V$ is reachable from~$s$, it will eventually have $\mathrm{pred}(v) \neq \bot$. Otherwise, let $q$ be an efficient \stpath[s][v] (which exists because $v$ is reachable) and $ab \in q$ the first edge on $q$ leading from a node~$a$ with $\mathrm{pred}(a)\neq \bot$ to a node~$b$ with $\mathrm{pred}(b)=\bot$ at the end of the first while-loop. But then, edge~$ab$ is efficient (by \Cref*{lemma:PropertiesOfNodeLabels:PathEfficientIffEdgesEfficient}) and $a$ was in~$Q$ at some point (by observation~(ii)). Thus, $b$ must have been added to $Q$ at some point, contradicting $\mathrm{pred}(b)=\bot$ at the end.
			\end{enumerate}
			
		\item Consider any edge $vw \in E$ with $\ell_w > \ell_v + \gamma_{vw}$. Moreover, let $p$ be an efficient \stpath[s][v].\footnote{Note that, while such a path is guaranteed to exist as $v$ is reachable, we cannot necessarily compute it by the algorithm from~a). However, we certainly still can compute such a path in different ways, though not necessarily in linear time.} Then, $p,vw$ is an \stwalk[s][w] with 
			\[\gamma_{p,vw} = \gamma_p + \gamma_{vw} = \ell_v + \gamma_{vw} < \ell_w.\]
		Hence, $p,vw$ cannot be a path and instead is of the form $p',c$ where $p'$ is as \stpath[s][w] and $c$ a cycle (\cf exercise 3d). Moreover, we have
			\[\gamma_{p'}+\gamma_{c} = \gamma_p + \gamma_{vw} = \ell_v + \gamma_{vw} < \ell_w \leq \gamma_{p'}\]
		and, therefore, $\gamma_c < 0$.
	\end{enumerate}}

\exComment{Rewrite this exercise! The reference in part b) is to \exref{NodeLabels}}