% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{IDETerminationLowerBound}

\exTitle{A Network with Large Termination Time}

\exProblem{In this exercise we want to construct a network with an IDE with particular high termination time.
\begin{enumerate}[label=\alph*)]
	\item Consider the following network
		\begin{center}
			\begin{adjustbox}{max width=\linewidth}
			\begin{tikzpicture}
				\clip (1,4.5) rectangle (19,-8.5);
				\node[namedVertex](t)at(10,-8){$t$};
				\foreach \i in {1,2,3,4,5,6,7,8,9}{
					\node[namedVertex](v\i)at(2*\i,0){$v_{\i}$};
					\node[namedVertex](w\i)at(2*\i,-2){$w_{\i}$};
					\node[namedVertex](x\i)at(2*\i,-4){$x_{\i}$};
					\draw[edge](v\i)--(w\i);
					\draw[edge](w\i)--(x\i);
					\draw[edge,dashed](x\i)to(t);
				}
				\foreach \i in {1,2,3,4,5,6,7,8}{
					\pgfmathtruncatemacro{\ip}{\i+1}
					\draw[edge](v\i)--(v\ip);
				}
				\draw[edge] (v9) to[out=45,in=135] node[above]{\small$(\infty,2)$}  (v1);
				
				\draw[edge] (v1) to[bend left=40] node[above]{\small$(\infty,6)$} (v4);
				\draw[edge] (v4) to[bend left=40] (v7);
				\draw[edge] (v7) to[out=45,in=110,looseness=0.8] (v1);
				
				\path(v1)--node[below,sloped]{\small $(\infty,2)$}(v2);
				\path(v1)--node[above,sloped,rotate=180]{\small $(6,17)$}(w1);
				\path(w1)--node[above,sloped,rotate=180]{\small $(2,2)$}(x1);
			\end{tikzpicture}
		\end{adjustbox}
	\end{center}
	where all edges $v_iv_{i+1}$ as well as $v_9v_1$ have a capacity of~$\infty$ and a travel time of~$2$, all edges $v_iw_i$ have a capacity of~$6$ and a travel time of~$19$ and all edges $w_ix_i$ have a capacity of~$2$ and a travel time of~$2$. Edges $v_1v_4$, $v_4v_7$ and $v_7v_1$ each have a capacity of~$\infty$ and a travel time of~$6$, however, they will only become relevant in part c). Moreover, all the paths $x_it$ have the same travel time. Now, assume that $v_1$ is the source node of the network and we have a network inflow rate $u = U\cdot\CharF[{[0,1]}]$ with $U$ much larger than~$2$.
	
	Sketch an IDE up to time $36$ in this network and with network inflow rate~$u$. Do not calculate any exact values but round everything to the next integer. 
	
	Can you give a rough estimate for the termination time of a full IDE in this network?
	
	\item Now consider the following part of a larger network
	\begin{center}
		\begin{tikzpicture}
			\node[namedVertex](y)at(3,-2){$y$};
			\node[namedVertex](z)at(3,-4){$z$};
			\draw[edge](y)--node[above,sloped]{$(2,2)$}(z);
			\draw[edge,dashed](z)--+(0,-.7);
			
			\foreach \i in {1,2,3}{
				\node[namedVertex](x\i)at(3*\i-3,0){$x_\i$};
				\draw[edge,dashed,<-](x\i)--+(0,.7);
				\draw[edge](x\i)--node[above,sloped]{$(2,2)$}(y);
			}
		\end{tikzpicture}
	\end{center}
	Assume that there is no flow on any of the four edges at some time $\theta$ and that there is an inflow of the form $2\cdot\CharF[{[\theta+2i,\theta+2i+9]}]$ into edge~$x_iy$ for $i=1,2,3$. Describe the queue length function on edge~$yz$.
	
	\item Use the partial network from b) to extend the termination time of an IDE in the network from part a). You do not need to provide a formal proof here.
	
	Can you extend this idea to larger networks with even higher termination time?
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item At first all flow originating at node~$v_1$ enters the edge towards node~$w_1$. Hence, a queue starts to grow reaching a length of~$12$ soon after. At that point the edge towards $v_2$ becomes active as well. Thus, from here on flow enters edge $v_1w_1$ only at a rate of~$6$ and edge $v_1v_2$ at a rate of~$U-6$. 
	
	This results in an outflow from edge~$v_1v_2$ at a rate of $U-6$ during $[2+\eps,3]\approx[2,3]$ resulting in essentially the same flow split as two time units earlier at node $v_1$ and so on until it enters edge~$v_9v_1$ at rate of~$U-9\cdot6$ during $\approx[16,17]$.
	
	Edge $v_1w_1$, on the other hand, has an outflow at a rate of~$6$ during $[17,20]$, edge~$v2w_2$ has an outflow of rate~$6$ during $\approx [19,22]$ and so on. This leads to a queue growing on edge $w_1x_1$ beginning with time~$17$ and reaching a length of~$4$ (\ie a waiting time of~$2$) at time~$18$ which is exactly the time at which flow arrives again at node~$v_1$ (at a rate of $U-9\cdot6$). Hence, this flow is directly diverted further on to node~$v_2$. This process repeats until all this flow enters again edge~$v_9v_1$ at time~$34$.
	
	Finally, the outflow from any edge~$w_ix_i$ is at a rate of~$2$ during $\approx[17+2i,26+2i]$ for $i \in [9]$.
	
	Starting at time~$36$ the pattern repeats only with $U$ replaced by $U-54$. Hence, this IDE continues until $\approx 36\cdot \tfrac{U}{54}$.
	
	\item The queue on edge~$yz$ will have the following form
		\begin{center}
			\begin{adjustbox}{max width=\linewidth}
				\begin{tikzpicture}[declare function = {
						Q(\x) = and(x>=4,x<6)*2*(x-4)+and(x>=6,x<11)*(4+4*(x-6))+and(x>=11,x<13)*(24+2*(x-11))+and(x>=13,x<15)*28+(x>=15)*(28-2*(x-15));
					}]
					\begin{axis}[
						axis lines=left,,
						ymin=0,ymax=29,
						xmin=1.5,xmax=20.5,
						width=16cm, height=8cm,
						xtick = {2,4,6,9,11,13,15},
						xticklabels={$\theta+4$,$\theta+6$,$\theta+8$,$\theta+11$,$\theta+13$,$\theta+15$,$\theta+17$},
						ytick = {4,16,24,28}]
						\addplot[blue,thick,domain=0:20.5,samples=400]{Q(\x)}node[below,pos=.72]{$\ql[yz]$};
						\addplot[dashed] coordinates {(0,4) (6,4) (6,0)};
						\addplot[dashed] coordinates {(0,24) (11,24) (11,0)};
						\addplot[dashed] coordinates {(0,28) (13,28) (13,0)};
						\addplot[dashed] coordinates {(15,28) (15,0)};
						
						\addplot[dashed] coordinates {(0,16) (9,16) (9,0)};
					\end{axis}		
				\end{tikzpicture}
			\end{adjustbox}
		\end{center}
		The outflow from the edge is at a rate of~$2$ during $[\theta+6,\theta+33]$.
		
	\item Combining the network from part a) and three copies of the partial network from part b) we get the following network (note, that we increased the travel time on edges $w_ix_i$ to~$10$!)
		\begin{center}
			\begin{adjustbox}{max width=\linewidth}
				\begin{tikzpicture}
					\clip (1,4.5) rectangle (19,-11.5);
					\node[namedVertex](t)at(10,-11){$t$};
					\foreach \i in {1,2,3}{
						\node[namedVertex](y\i)at(6*\i-2,-6){$y_\i$};
						\node[namedVertex](z\i)at(6*\i-2,-8){$z_\i$};
						
						\draw[edge](y\i)--(z\i);
						\draw[edge](z\i)--(t);
					}
					
					\foreach \i in {1,2,3,4,5,6,7,8,9}{
						\node[namedVertex](v\i)at(2*\i,0){$v_{\i}$};
						\node[namedVertex](w\i)at(2*\i,-2){$w_{\i}$};
						\node[namedVertex](x\i)at(2*\i,-4){$x_{\i}$};
						\draw[edge](v\i)--(w\i);
						\draw[edge](w\i)--(x\i);
						\pgfmathtruncatemacro{\im}{(\i-1)/3+1}
						\draw[edge](x\i)--(y\im);
					}
					\foreach \i in {1,2,3,4,5,6,7,8}{
						\pgfmathtruncatemacro{\ip}{\i+1}
						\draw[edge](v\i)--(v\ip);
					}
					\draw[edge] (v9) to[out=45,in=135] node[above]{\small$(\infty,2)$}  (v1);
					
					\draw[edge] (v1) to[bend left=40] node[above]{\small$(\infty,6)$} (v4);
					\draw[edge] (v4) to[bend left=40] (v7);
					\draw[edge] (v7) to[out=45,in=110,looseness=0.8] (v1);
					
					\path(v1)--node[below,sloped]{\small $(\infty,2)$}(v2);
					\path(v1)--node[above,sloped,rotate=180]{\small $(6,17)$}(w1);
					\path(w1)--node[above,sloped,rotate=180]{\small $(2,8)$}(x1);
				\end{tikzpicture}
			\end{adjustbox}
		\end{center}		
		Combining the results from part a) and b) we can see that in this network at time~$36$ we have a queue of length~$16$ on edge $y_1z_1$ and no queue on edge~$y_2z_2$. Hence, the flow arriving at node~$v_1$ via edge~$v_9v_1$ will directly enter the edge $v_1v_4$. Similarly, arriving at node~$v_4$ the flow will directly enter the edge towards $v_7$.
		
		Thus, the main amount of flow can essentially make another round in the top part of the network without loosing (much) additional flow. 
		
		To increase the termination time even further, one can start with a larger network (\eg with nodes $v_1,\dots,v_{27}$) and then add an additional layer of the partial networks from part b) giving us additional round of travel without loosing flow. This way, we can get instances with an (asymptotic) termination time of order $U\cdot\log(\sum_e \tau_e)$. 
\end{enumerate}}

\exComment{}