% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{IDEExistenceVI}

\exTitle{An Alternative Existence Result for IDE}

\exProblem{
	\begin{enumerate}[label=\alph*)]
		\item Let $\network$ be a dynamic flow network consisting only of parallel edges from~$s$ to~$t$. Show that in such a network the definition of an IDE coincides with that of a dynamic equilibrium (as defined in exercise~4 on exercise sheet~10).
		
		\item Now, let $\network$ be a general dynamic flow network with $\tauMin \coloneqq \min\set{\tau_e | e \in E} > 0$, where $t$ is reachable from all nodes in the network but has no outgoing edges itself. Let $u \in \pInt[2]$ be a non-negative, bounded network inflow rate and $f$ an IDE up to some time horizon~$T$ (\ie it satisfies all conditions of an IDE up to time~$T$, but not necessarily after that time). We now want to extend this flow to an IDE until $T+\tauMin$. In other words, we want to find a dynamic flow~$g$ which is an IDE until time $T+\tauMin$ and coincides with $f$ on $[0,T]$.
		
		Characterize such extensions using a variational inequality of the form used in exercise~4d) on exercise sheet~10, \ie find a set $C$ and a mapping $\mathcal{A}$ such that $g$ is an IDE-extension of~$f$ iff it is a solution to Lions' variational inequality.
		
		\item Use the characterization from b) to show existence of IDE-extensions (you do not need to be completely formal here!).
		
		\item Use c) to derive an alternative existence result for IDE. Then compare your result to the one from the lecture (\ie \Cref*{thm:IDEExistence}): Are there any situations where it is more/less general?
	\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item We start by observing that in a parallel link network we have
		\[\lRi[t](\theta) = 0 \text{ and }\lRi[s](\theta) = \inf\set{\cexittime(\theta) -\theta | e \in E} = \min\set{\cexittime(\theta) | e \in E} - \theta\]
	for all $\theta \in \IR$. Hence, an edge~$e$ is active (in the IDE sense) at some time~$\theta$ if and only if it satisfies $\cexittime(\theta) = \min\set{\cexittime[e'](\theta) | e' \in E}$. This shows that the definitions of IDE and dynymic equilibrium coincide in this case.
	
	\item We define the set $C$ by
		\[C \coloneqq \Set{g^+ \in \pInt[2][{[0,T+\tauMin]}]^E | \begin{array}{l}
				g^\pm_e(\theta)=f^\pm_e(\theta) \text{ f.a.a. } \theta \in [0,T] \text{ and } \exists g^-_e \in \pInt[2]: \\
				(f^+_e,g^-_e) \text{ a Vickrey flow until } T+\tauMin \text{ f.a. } e \in E,\\
				(g^+,g^-) \text{ satisfies strong flow conservation with}\\
				\text{network inflow rate } u \text{ until } T+\tauMin
			\end{array}}\]
		and the mapping $\mathcal{A}: C \to \pInt[2][{[0,T+\tauMin]}]^E$ by 
			\[\mathcal{A}(g)_e(\theta) \coloneqq \tau_e+\tfrac{\ql^g(\theta)}{\nu_e}+\lRi[w]^g(\theta)-\lRi[v]^g(\theta)\]
		for any edge $e=vw \in E$. Note that $\mathcal{A}(g)_e(\theta)$ is always non-negative and $\mathcal{A}(g)_e(\theta)=0$ iff $e$ is active at time $\theta$ (\wrt $g$).
		
		\begin{claim}
			$g$ is an extension of $f$ to an IDE until time $T+\tauMin$ if and only if $g^+$ is a solution to the variational inequality
				\begin{align}\tag{VI-IDE}\label{eq:VI-IDE}
					\sum_{e \in E}\int_0^{T+\tauMin}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \geq 0 \text{ for all } h^+ \in C
				\end{align}
		\end{claim}
		
		We prove this claim along similar lines to exercise~4c) on exercise sheet~10:
		
		\begin{proofClaim}
			Assume first that $g$ is an IDE-extension of $f$ and $h^+ \in C$ arbitrary. We define for every node $v \in V\setminus\set{t}$ a function 
				\[\alpha_v: [0,T+\tauMin]\to\IRnn, \theta \mapsto \min\set{\cexittime^g(\theta) | e \in \edgesFrom{v}}\]
			and for every edge $e \in E$ a set $M_e \coloneqq \set{\theta \in [0,T+\tauMin] | h^+_e(\theta) > g^+_e(\theta)}$. Then, we have
			\begin{align*}
				&\sum_{e \in E}\int_0^{T+\tauMin}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
				&\quad\quad = \sum_{v \in V\setminus\set{t}}\sum_{e \in \edgesFrom{v}}\int_0^{T+\tauMin}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
				&\quad\quad = \sum_{v \in V\setminus\set{t}}\sum_{e \in \edgesFrom{v}}\int_{M_e}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
					&\quad\quad\quad\quad + \int_{[0,T+\tauMin]\setminus M_e}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
				&\quad\quad \geq \sum_{v \in V\setminus\set{t}}\sum_{e \in \edgesFrom{v}}\int_{M_e}\alpha_v(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
					&\quad\quad\quad\quad + \int_{[0,T+\tauMin]\setminus M_e}\alpha_v(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
				&\quad\quad = \sum_{v \in V\setminus\set{t}}\int_0^{T+\tauMin}\alpha_v(\theta)\Big(\sum_{e \in \edgesFrom{e}}h^+_e(\theta) - \sum_{e \in \edgesFrom{e}}g^+_e(\theta)\Big)\diff\theta \\
				&\quad\quad \symoverset{1}{=} \sum_{v \in V\setminus\set{t}}\int_0^{T+\tauMin}\alpha_v(\theta)\Big(\sum_{e \in \edgesFrom{e}}h^-_e(\theta) - \sum_{e \in \edgesFrom{e}}g^-_e(\theta)\Big)\diff\theta \\
				&\quad\quad \symoverset{2}{=} \sum_{v \in V\setminus\set{t}}\int_0^{T+\tauMin}\alpha_v(\theta)\cdot 0 = 0.
			\end{align*}
			Here, \refsym{1} holds because both $g$ and $h$ satisfy strong flow conservation at nodes (until $T+\tauMin$) and \refsym{2} because the edge inflow rates of $g$ and $h$ are the same until $T$ and, therefore, the edge outflow rates are the same until $T+\tauMin$ by \Cref*{lemma:EdgeLoadingUniqueness}.
			
			For the other direction, let $g$ be a solution to \eqref{eq:VI-IDE} and assume for contradiction that $g$ is not an IDE-extension of~$f$. Since we have $g^+ \in C$ (and $f$ is an IDE until~$T$), this is only possible because $g$ violates the IDE condition during $[T,T+\tauMin]$. Hence, there must be an edge $e'=vw' \in E$ and a set $M \subseteq [T,T+\tauMin]$ of positive measure such that we have $g^+_{e'}(\theta) > 0$ and $e' \notin \bar E^g(\theta)$ for all $\theta \in M$. We may assume \wlg that there is one alternative edge $e''=vw'' \in \edgesFrom{v}$ which is active for all those times. Then, we can define an alternative extension $h^+ \in C$ by setting
				\[h^+_e(\theta) \coloneqq \begin{cases}
						0, 									&\text{ if } e=e', \theta \in M \\
						g^+_{e'}(\theta)+g^+_{e''}(\theta), &\text{ if } e=e'', \theta \in M \\
						f^+_e(\theta),						&\text{ else}
				\end{cases}.\]
			Then, we have
				\begin{align*}
					0 &\leq \sum_{e \in E}\int_0^{T+\tauMin}\mathcal{A}(g)_e(\theta)(h^+_e(\theta)-g^+_e(\theta))\diff\theta \\
						&= \int_M\mathcal{A}(g)_{e'}(\theta)(0-g^+_{e'}(\theta))\diff\theta + \int_M\underbrace{\mathcal{A}(g)_{e''}(\theta)}_{=0}(g^+_{e'}+g^+_{e''}-g^+_{e''}(\theta))\diff\theta \\
						&= -\int_M\underbrace{\mathcal{A}(g)_{e'}(\theta)}_{>0}\underbrace{g^+_{e'}(\theta)}_{>0}\diff\theta < 0
				\end{align*}
			which is a contradiction.
		\end{proofClaim}
		
		\item We only need to observe that $C$ is non-empty, closed, convex and bounded as well as $\mathcal{A}$ weak-strong sequentially continuous. The latter follows directly from \Cref*{lemma:EdgeLoadingContinuous} and \Cref*{claim:IntegrationIsWeakStrongContinuous} in its proof (\ie the weak-weak sequential continuity of the Vickrey edge-loading mapping and the fact that integrating is weak-strong sequentially continuous).	The former follows directly from the definition of~$C$ (note that the edge outflow rates are completely fixed until~$T+\tauMin$ by the edge inflow rates until~$T$ which are the same for every element of~$C$!). The existence theorem for solutions of variational inequalities from exercise~4 on exercise sheet~10 then guarantees the existence of a solution to~\eqref{eq:VI-IDE} which, by part b), is an IDE-extension of $f$ until $T+\tauMin$.
		
		\item Starting with the zero flow which is an IDE until time~$0$, we can repeatedly apply the result from c) to extend this to an IDE up to any finite time horizon $T \in \IRnn$. Using a simple limit argument, we can even obtain a full IDE. 
		
		Comparing this result to \Cref*{thm:IDEExistence} we see that it is more general in the sense that we do not require the existence of some finite time horizon before which all IDE in the given network are guaranteed to terminate. In particular, we get existence of IDE with infinitely lasting network inflow rates. On the other hand, the result obtained here is less general in the sense that we cannot allow edges with zero travel time (which is allowed in \Cref*{thm:IDEExistence}).
\end{enumerate}}

\exComment{The exercise referenced here is \exref{DynamicEquilibriaInParallelLinkNetworks}. The assumption that there are no outgoing edges from $t$ is \wlg as those edges cannot be used in an IDE anyway.

The proof here is essentially the same as the one given by Leon Sering for multi-commodity IDE in \cite[Appendix 4]{DynamicFlowswithAdaptiveRouteChoice}}