% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{TimeExpandedNetworks}

\exTitle{Time-Expanded Networks}

\exProblem{An alternative approach to solving dynamic flow problems is to use time-expanded networks: Given a dynamic flow network $\network=(G,s,t,\tau_e,\nu_e)$ with integer travel times and some time horizon $T \in \INs$, the corresponding time expanded network $\tilde\network=(\tilde G,\tilde s,\tilde t,\tilde\nu)$ with time horizon~$T$ is defined as follows:
\begin{itemize}
	\item $\tilde G = (\tilde V, \tilde E)$ with 
	\item $\tilde V \coloneqq \Set{(v,k) | v \in V, k \in \set{0,1,\dots,T-1}}$ and 
	\item $\tilde E \coloneqq \Set{e_k \coloneqq ((v,k),(w,k+\tau_e)) | e=vw \in E, k \in \set{0,1,\dots,T-1-\tau_e}}$\\
			\phantom{$\tilde E \coloneqq\,$}$\cup \Set{((s,0),(s,k)) | k \in [T-1]} \cup \Set{((t,k),(t,T-1)) | k \in \set{0,\dots,T-2}}$,
	\item $\tilde\nu_{(v,k)(w,k+\tau_e)} \coloneqq \nu_e$, $\tilde \nu_{(s,0)(s,k)} \coloneqq \infty$, $\tilde\nu_{(t,k)(t,T-1)}\coloneqq\infty$,
	\item $\tilde s \coloneqq (s,0)$ and $\tilde t \coloneqq (t,T-1)$.
\end{itemize}

\begin{enumerate}[label=\alph*)]
	\item Draw the time expanded network with time horizon $T=6$ for
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex](s)at(0,0){$s$};
				\node[namedVertex](v)at(3,0){$v$};
				\node[namedVertex](w)at(0,-3){$w$};
				\node[namedVertex](u)at(3,-3){$u$};
				\node[namedVertex](t)at(5,-1.5){$t$};
				
				\draw[edge] (s) --node[sloped,above]{$\tau_{sv}=1$} (v);
				\draw[edge] (s) --node[sloped,above]{$0$} (w);
				\draw[edge] (v) --node[sloped,above]{$2$} (w);
				\draw[edge] (v) --node[sloped,above]{$1$} (t);
				\draw[edge] (w) --node[sloped,above]{$0$} (u);
				\draw[edge] (u) --node[sloped,above]{$1$} (v);
				\draw[edge] (u) --node[sloped,above]{$2$} (t);
			\end{tikzpicture}
		\end{center}
		(where the edge-capacities have been omitted for simplicity)
		
	\item Show the following statement: For any feasible static \stflow{} $x$ in $\tilde\network$ there exists a direct dynamic flow~$f$ in $\network$ with $\fval{f,T}=\fvals{x}$ and vice versa.
	
	\item How can we use part b) to compute maximum dynamic flows? Compare the runtime of this approach to the one of the Dynamic Ford-Fulkerson algorithm from the lecture.
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item The time expanded network is
		\begin{center}
			\begin{tikzpicture}
				\foreach \k in {0,1,2,3,4,5}{
					\node[namedVertex,inner sep=2](s\k)at(0,-2*\k){$s,\k$};
					\node[namedVertex,inner sep=2](v\k)at(3,-2*\k){$v,\k$};
					\node[namedVertex,inner sep=2](w\k)at(6,-2*\k){$w,\k$};
					\node[namedVertex,inner sep=2](u\k)at(9,-2*\k){$u,\k$};
					\node[namedVertex,inner sep=2](t\k)at(12,-2*\k){$t,\k$};
				}
				
				\foreach \k in {0,1,2,3,4,5}{					
					\ifthenelse{\k<5}{
						\pgfmathtruncatemacro{\kp}{\k+1}\draw[edge] (s\k) -- (v\kp);
					}{}
					\ifthenelse{\k<6}{
						\pgfmathtruncatemacro{\kp}{\k}\draw[edge] (s\k) to[bend left=20] (w\kp);
					}{}
					\ifthenelse{\k<4}{
						\pgfmathtruncatemacro{\kp}{\k+2}\draw[edge] (v\k) -- (w\kp);
					}{}
					\ifthenelse{\k<5}{
						\pgfmathtruncatemacro{\kp}{\k+1}\draw[edge] (v\k) -- (t\kp);
					}{}
					\ifthenelse{\k<6}{
						\pgfmathtruncatemacro{\kp}{\k}\draw[edge] (w\k) -- (u\kp);
					}{}
					\ifthenelse{\k<5}{
						\pgfmathtruncatemacro{\kp}{\k+1}\draw[edge] (u\k) -- (v\kp);
					}{}
					\ifthenelse{\k<4}{
						\pgfmathtruncatemacro{\kp}{\k+2}\draw[edge] (u\k) -- (t\kp);
					}{}
					
					\ifthenelse{\k>0}{
						\draw[edge] (s0) to[bend right=20] (s\k);
					}{}
					\ifthenelse{\k<5}{
						\draw[edge] (t\k) to[bend left=20] (t5);
					}{}
				}
			\end{tikzpicture}
		\end{center}
	
		\item Let $x$ be a feasible static \stflow{} in~$\tilde\network$. Then, we define $(f^+_e)$ as follows:
				\[f^+_e(\theta) \coloneqq \begin{cases}
					x_{e_k}, &\text{ if } \theta \in [k,k+1) \text{ for some } k =0,\dots,T-1-\tau_e \\
					0,		 &\text{ else}
				\end{cases} \text{ for all } e \in E.\]
			It is easy to see that this defines a direct dynamic flow. Moreover, its value until~$T$ is
				\begin{align*}
					\fval{f,T}
						&=\sum_{e \in \edgesTo{t}}F^-_e(T)-\sum_{\edgesFrom{t}}F^+_e(T) \\
						&=\sum_{k=0}^{T-1}\sum_{e \in \edgesTo{t}}\int_k^{k+1}f^-_e(\zeta)\diff\zeta-\sum_{k=0}^{T-1}\sum_{e \in \edgesFrom{t}}\int_k^{k+1}f^+_e(\zeta)\diff\zeta \\
						&=\sum_{k=0}^{T-1}\sum_{e \in \edgesTo{t}}x_{e_{k-\tau_e}}-\sum_{k=0}^{T-1}\sum_{e \in \edgesFrom{t}}x_{e_k} \\
						&=\sum_{k=0}^{T-1}\Big(\sum_{e_\ell \in \edgesTo{t,k}}x_{e_\ell} - \sum_{e_\ell \in \edgesFrom{t,k}}x_{e_\ell}\Big) \\
						&=\sum_{e=(t,k)(t,T-1)}x_e + \sum_{e_\ell \in \edgesTo{t,T-1}}x_{e_\ell} - \sum_{e_\ell \in \edgesFrom{t,T-1}}x_{e_\ell} \\
						&=\fvals{x},
				\end{align*}
			where we assume $x_{e_k}=0$ for all pairs $e$ and $k$ such that the edge $e_k$ is not present in $\tilde\network$.
				
			Now, let $(f^+_e)$ be a direct dynamic flow in~$\network$. We then define a static \stflow{} $x$ in~$\tilde\network$ by
				\[x_{e_k} \coloneqq \int_k^{k+1}f^+_e(\zeta)\diff\zeta\]
			as well as
				\[x_{(s,0)(s,k)} \coloneqq \sum_{e \in \edgesFrom{s}}\int_k^{k+1}f^+_e(\zeta)\diff\zeta-\sum_{e \in \edgesTo{s}}\int_k^{k+1}f^-_e(\zeta)\diff\zeta\]
			and
				\[x_{(t,k)(t,T-1)} \coloneqq \sum_{e \in \edgesTo{t}}\int_k^{k+1}f^-_e(\zeta)\diff\zeta-\sum_{e \in \edgesFrom{t}}\int_k^{k+1}f^+_e(\zeta)\diff\zeta.\]
			Strong flow conservation of~$f$ on edges and nodes then ensures that $x$ satisfies flow conservation at nodes as well. Moreover, $f$ respecting capacities together with strong flow conservation on edges, implies that $x$ respects capacities. Hence, $x$ is a feasible \stflow. Moreover, the same computation as for the reverse direction yields $\fvals{x}=\fval{f,T}$.
			
		\item According to part b) a maximum static \stflow{} in~$\tilde\network$ corresponds to a maximum dynamic \stflow. Thus, we can compute a maximum dynamic flow in the latter by computing a maximum static flow in the former. Since we can compute static maximum flows in polynomial time in the number of nodes and edges of a given network (using, e.g., the Edmonds-Karp algorithm), this yields a runtime polynomial in $\abs{V}$, $\abs{E}$ and $T$. To compute a maximum dynamic flow using Dynamic Ford-Fulkerson we only need to compute a min-cost flow in the original (unexpanded) network. This is possible in polynomial time in $\abs{V}$ and $\abs{E}$ (using, e.g., Orlin's algorithm).
	\end{enumerate}}

\exComment{Part b) is \cite[Lemma 4.4]{SkutellaSurvey}. Maybe add a part, where one should relate cuts over time to static cuts in the time expanded  network? Maybe FF did something similar in their original paper?}