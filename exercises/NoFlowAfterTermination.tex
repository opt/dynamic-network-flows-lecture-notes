% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{NoFlowAfterTermination}

\exTitle{Properties of Flow Volume in Networks}

\exProblem{Let $\network$ be a dynamic flow network and $f$ a dynamic flow in~$\network$ with network inflow rate~$u$.
\begin{enumerate}[label=\alph*)]
	\item Assume that $f$ satisfies strong flow conservation at all nodes. Show that we have
		\[\int_0^\theta u(\zeta)\diff\zeta-\fval{f,\theta} = \sum_{e \in E}\big(F^+_e(\theta)-F^-_e(\theta)\big) \text{ for all } \theta \in \IR.\]
\end{enumerate}
Now, let $f$ be a Vickrey flow and $T \in \IRnn$ some time with $\supp(u) \subseteq [0,T]$ and $F^+_e(T)=F^-_e(T)$ for all edges $e \in E$. 
\begin{enumerate}[label=\alph*),resume]
	\item Assume that $\network$ has no edges with $\tau_e=0$. Show that we have $\supp(f) \subseteq [0,T]$.
	
	\item What happens with the statement in b) if we allow $\network$ to have edges of zero travel time?
\end{enumerate}
}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item We have
		\begin{align*}
			\sum_{e \in E}\big(F^+_e(\theta)-F^-_e(\theta)\big) = \sum_{v \in V}\Big(\sum_{e \in \edgesFrom{v}}F^+_e(\theta)-\sum_{e \in \edgesTo{v}}F^-_e(\theta)\Big) = \int_0^\theta u(\zeta)\diff\zeta-\fval{f,\theta}
		\end{align*}
		where the second inequality holds due to strong flow conservation at nodes as well as the definitions of $\fval{f,\theta}$ and $u$ being the network inflow rate of~$f$.
		
	\item Using weak flow conservation on edges we get for any time $\theta \geq T$ that
		\begin{align*}
			0 &\leq \sum_{e \in E}\big(F^+_e(\theta)-F^-_e(\theta)\big) \toverset{a)}{=} \int_0^\theta u(\zeta)\diff\zeta-\fval{f,\theta} \symoverset{1}{\leq} \int_0^T u(\zeta)\diff\zeta - \fval{f,T} \\
				&\toverset{a)}{=} \sum_{e \in E}\big(F^+_e(T)-F^-_e(T)\big) = 0,
		\end{align*}
		where \refsym{1} holds because of $\supp(u) \subseteq [0,T]$ and since $\fval{f,.}$ is non-decreasing. This implies $F^+_e(\theta)=F^-_e(\theta)$ for all $e \in E$ and $\theta \geq T$.  
		
		Now, let $e \in E$ be any edge and define $\hat\theta \coloneqq \inf\set{\theta \geq T | F^+_e(\theta) > F^+_e(T)}$. If $\hat\theta < \infty$, then weak flow conservation on edge~$e$ would imply
			\[F^+_e(\hat\theta) < F^+_e(\hat\theta+\tau_e) = F^-_e(\hat\theta+\tau_e) \leq F^+_e(\hat\theta) \] 
		which is a contradiction. Thus, we must have $\hat\theta=\infty$ which implies $f^+_e(\theta) = 0$ for almost all $\theta \geq T$. Because of weak flow conservation, we also get $f^-_e(\theta)=0$ for almost all $\theta \geq T$.
		
	\item If we allow edges of travel time $0$ the statement from~b) is not necessarily true anymore. Consider, for example, a network consisting of just two edges $st$ and $ts$, both with capacity~$1$ and travel time~$0$. Then, the Vickrey flow defined by $f^+_{st} \coloneqq \CharF[{[0,2]}]$ and $f^+_{ts} \coloneqq \CharF[{(1,2]}]$ is a flow with network inflow rate $u = \CharF[{[0,1]}]$. Moreover, we have $F^+_e(1)=F^-_e(1)$ for $e \in \set{st,ts}$, but also $\supp(u)=[0,1]$ as well as $\supp(f)=[0,2]$.
\end{enumerate}}

\exComment{Closely related to \exref{NetworkInflowProperties}. Note that part b) does not really need the assumption that $f$ is a Vickrey flow. It suffices that $f$ satisfies weak flow conservation on edges and strong flow conservation on nodes. A generalization of b) can be found in \cite[Proposition 3.73]{GrafThesis}. I would assume that b) also holds if we allow edges of zero travel time as long as there are no cycles of travel time zero.}