% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{BetterAlternativeDynamicCutCorrectness}

\exTitle{Static Cuts for Dynamic Flows II}

\exProblem{Let $\network=(G,s,t,\nu,\tau)$ be a dynamic flow network, $f$ a feasible flow in this network, $T\geq 0$ some time horizon and $A \subseteq V$ a (static) \stcut{} in $(G,s,t,\nu)$.
\begin{enumerate}[label=\alph*)]
	\item Find a dynamic \stcut{} with capacity
		\[\sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,T-\tau_e}.\]
	\item Let $e \in E$ be an edge with $\inf\set{\tau_p | p \in \PathSet: e \in p}>T$. Show that removing such an edge from the network does not decrease the value of a maximum dynamic flow with time horizon~$T$.
	\item Show that we have
		\[\fval{f,T} \leq \sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,(T-\inf\set{\tau_p | p \in \PathSet: e \in p})}.\]
	\item Deduce from this, that in a feasible dynamic flow no flow arrives at the sink before time $\tauPmin \coloneqq \inf\set{\tau_p | p \in \PathSet}$.
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item Define a dynamic \stcut{} by
			\[\beta_v \coloneqq \begin{cases}0, &\text{ if } v \in A\\ T, &\text{ if } v \notin A\end{cases}.\]
			This is clearly a dynamic cut and we have
			\[\beta_w-\tau_e-\tau_v = \begin{cases}T-\tau_e, &\text{ if } e\in\edgesFrom{A}\\0, &\text{ else}\end{cases}.\]
			This then directly implies 
			\[\ccap{\beta} = \sum_{e=vw\in E}\nu_e\cdot\max\set{0,\beta_w-\tau_e-\beta_v} = \sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,T-\tau_e}.\]
			
		\item According to \Cref*{thm:TemporallyRepeatedFlowIsOpt} there exists a path-based dynamic flow not using any path of length more than~$T$ which has a corresponding direct dynamic flow that achieves the maximum value up to time~$T$. Thus, all edges~$e$ used by such a flow satisfy $\inf\set{\tau_p | p \in \PathSet: e \in p} \leq T$. Hence, we can remove all other edges without destroying this maximum flow and, therefore, without reducing the value of a maximum dynamic flow in this network.
		
		\item By b) we can assume \wlg that the given network does not contain any edges $e$ with $\inf\set{\tau_p | p \in \PathSet: e \in p}>T$. We now define a dynamic \stcut{} by 
			\[\beta_v \coloneqq \begin{cases}\min\set{\inf\set{\tau_p | p \text{ an \stpath[s][v]}},T}, &\text{ if } v \in A\\ \max\set{T-\inf\set{\tau_p | p \text{ a \stpath[v][t]}},0}, &\text{ if } v \notin A\end{cases}.\]	
		This is indeed a dynamic \stcut{} as we have $\beta_s = \inf\set{\tau_p | p \text{ an \stpath[s][s]}}=0$ and $\beta_t = T-\inf\set{\tau_p | p \text{ a \stpath[t][t]}}=T$. In order to compute the capacity of this cut, we determine the contribution of each edge $e=vw \in E$ separately using a case distinction:
		\begin{proofbycases}
			\caseitem{$v,w \in A$} Since all edge travel times are non-negative, we have
				\[\inf\set{\tau_p | p \text{ an \stpath[s][w]}} \leq \inf\set{\tau_p | p \text{ an \stpath[s][v]}} + \tau_{vw}.\]
				Hence, we have $\beta_w\leq\tau_e+\beta_v$ and, therefore, $\max\set{0,\beta_w-\tau_e-\beta_v} = 0$.
			\caseitem{$v,w \notin A$} As in the previous case we have
				\[\inf\set{\tau_p | p \text{ a \stpath[v][t]}} \leq \tau_{vw} + \inf\set{\tau_p | p \text{ a \stpath[w][t]}}\]
				and, therefore, $\beta_v \geq \beta_w - \tau_e$. Thus, we have again $\max\set{0,\beta_w-\tau_e-\beta_v}=0$.
			\caseitem{$vw \in \edgesFrom{A}$} Here, we have
				\[\min\set{\tau_p | p \in \PathSet: e \in p} \leq \min\set{\tau_p | p \text{ an \stpath[s][v]}} + \tau_{vw} + \min\set{\tau_p | p \text{ a \stpath[w][t]}},\]
				which implies $T-\min\set{\tau_p | p \in \PathSet: e \in p}\geq\beta_w-\beta_v-\tau_e$ and, therefore,
					\[\max\set{0,\beta_w-\tau_e-\beta_v}\leq\max\set{0,T-\min\set{\tau_p | p \in \PathSet: e \in p}}.\]
			\caseitem{$vw \in \edgesTo{A}$} In this case we have
				\[\min\set{\tau_p | p \text{ an \stpath[s][v]}} +\min\set{\tau_p | p \text{ a \stpath[w][t]}} \leq \min\set{\tau_p | p \in \PathSet: e \in p} \leq T\]
				and, therefore, 
				\[\beta_w-\tau_e-\beta_v \leq \min\set{\tau_p | p \text{ an \stpath[s][v]}} - T + \min\set{\tau_p | p \text{ a \stpath[w][t]}} \leq 0.\]
				Thus, we have $\max\set{0,\beta_w-\tau_e-\beta_v}=0$.
		\end{proofbycases}		
		
		Now, we can apply \Cref*{lemma:DynamicCutIsUpperBound} to get
			\begin{align*}
				\fval{f,T} 
					&\leq \ccap{\beta} = \sum_{e=vw \in E}\nu_e\cdot\max\set{0,\beta_w-\tau_e-\beta_v} \\
					&\leq \sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,T-\min\set{\tau_p | p \in \PathSet: e \in p}}
			\end{align*}
		
		\item Choose $T=\tauPmin$ and $A \coloneqq \set{s}$. Then, we get from c):
			\[\fval{f,\tauPmin} \leq \sum_{e \in \edgesFrom{s}}\nu_e\cdot \max\set{0,\tauPmin-\min\set{\tau_p | p \in \PathSet: e \in p}}=0.\]
\end{enumerate}}

\exComment{This finishes the proof of \exref{AlternativeDynamicCut} part c)}