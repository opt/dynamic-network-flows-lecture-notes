% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{WeakAndStrongTopology}

\exTitle{Weak and strong topology}

\exProblem{\begin{enumerate}[label=\alph*)]
	\item Let $(X,\norm{.})$ be a normed vector space. Show that the weak topology on~$X$ is weaker than the strong topology (\ie the topology induced by the norm). In other words: Show that any weakly open set in~$X$ is also strongly open.
	
	\textit{Hint:} It suffices to only consider the weakly open sets of the form $U(\varphi,x,\eps) \subseteq X$.
	
	\item Now consider $\IR^n$ with the 1-norm\footnote{Since all norms on $\IR^n$ are equivalent, there is nothing special about the choice of the 1-norm.}: $\norm{x}_1 \coloneqq \sum_{i=1}^n\abs{x_i}$. Show that the weak topology on $\IR^n$ is the same as the (strong) topology induced by~$\norm{.}_1$.
	
	\textit{Hint:} It suffices to show that for any $x \in \IR^n$ and $\eps > 0$ there exists a weakly open set $U \subseteq \ballOpen[\eps]{x}$ with $x \in U$. Why?
	
	\item \textit{Bonus:} Show that in $L^2(\IR)$ the weak and strong topology are not the same.
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item We have
			\[U(\varphi,x,\eps) = \set{y \in X | \abs{\varphi(x)-\varphi(y)} < \eps} = \varphi^{-1}\big((\varphi(x)-\eps,\varphi(x)+\eps)\big).\]
		Since $(\varphi(x)-\eps,\varphi(x)+\eps)$ is an open set in $\IR$ and $\varphi: X \to \IR$ is continuous (\wrt the strong topology on $X$), this shows that $U(\varphi,x,\eps)$ is open with respect to the strong topology as well.
		
		\item Let $V \subseteq \IR^n$ be an open set (\wrt the strong topology). To show that this set is also open \wrt the weak topology we have to show that for every $x \in V$ there exists a weakly open set $U \subseteq \IR^n$ with $x \in U \subseteq V$.
		Since $V$ is open, there exists an open ball $\ballOpen[\eps]{x} \subseteq V$. We now define the set
			\[U \coloneqq \bigcap_{i=1}^n U\big(\varphi_i,x,\tfrac{\eps}{n}\big) \text{ where } \varphi_i: \IR^n \to \IR, x \mapsto x_i\]
		This set is weakly open as finite intersection of weakly open set. Moreover, we have $x \in U$ as well as $U \subseteq V$ since for every $y \in U$ we have
			\[\norm{x-y}_1 = \sum_{i=1}^n\abs{x_i-y_i} = \sum_{i=1}^n\abs{\varphi_i(x)-\varphi_i(y)}<\sum_{i=1}^n\tfrac{\eps}{n}=\eps\]
		and, therefore $y \in \ballOpen[\eps]{x} \subseteq V$.
		
		\item The closed unit ball is weakly compact but not strongly compact. Thus, there must be strongly open sets which are not weakly open.
		
		\begin{altsolution}
			A more elementary way to show the same result (for general infinite dimensional normed vector spaces $X$) is to show that the open balls $\ballOpen[1]{0} \subseteq X$ is not weakly open. This is equivalent to showing that for any finite collection of $U_i(\varphi_i,x^i,\eps_i)$ with $0 \in \bigcap_i U_i(\varphi_i,x^i,\eps_i)$ we have $\bigcap_i U_i(\varphi_i,x^i,\eps_i) \nsubseteq \ballOpen[1]{0}$. 
			
			Since $X$ has infinite dimension, we can pick linearly independent vectors $y^1, \dots, y^{k+1} \in X$. Then, there exists a non-zero vector $\lambda \in \IR^{k+1}$ such that we have
				\[\sum_{j=1}^{k+1}\lambda_j \varphi_i(y^j) = 0 \text{ for all } i \in [k].\]
			This implies that we have $y^\ast \coloneqq \sum_j \lambda_j y^j \neq 0$ (since the $y^j$ are linearly independent). Moreover, for every $\mu \in \IR$ we have
				\begin{align*}
					\abs{\varphi_i(\mu \cdot y^\ast)-\varphi_i(x_i)} \leq \mu\cdot\abs{\phi_i(y^\ast)}+\abs{\phi_i(0)-\phi_i(x_i)} \leq 0 + \eps_i = \eps_i
				\end{align*}
			and, therefore $\mu\cdot y^\ast \in U(\varphi_i,x_i,\eps_i)$ for every $i \in [k]$. Thus, we have $\mu\cdot y^\ast \in \bigcap_i U(\varphi_i,x_i,\eps_i)$ while at the same time we have $\mu\cdot y^\ast \notin \ballOpen[1]{0}$ for $\mu$ large enough.
		\end{altsolution}
\end{enumerate}}

\exComment{}