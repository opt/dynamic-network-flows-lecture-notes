% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{DynamicEquilibriaInParallelLinkNetworks}

\exTitle{Dynamic Equilibria in Parallel Link Networks}

\exProblem{Consider a dynamic flow network $\network$ which consists only of parallel edges, \ie we have only edges from~$s$ directly to~$t$ (but, in contrast to our usual convention, there may be multiple such parallel edges). We call a dynamic flow in this network a Vickrey flow if it is a Vickrey flow on every edge.
	
\begin{center}
	\begin{tikzpicture}
		\node[namedVertex] 	(s) at (0,0) {$s$};
		\node[namedVertex] 	(t) at (4,0) {$t$};
		\node () at (2,-.2) {$\vdots$};
		
		\draw[edge] (s) to[bend left=40] node[above]{$e_1$} (t);
		\draw[edge] (s) to[bend left=10] node[above]{$e_2$} (t);
		\draw[edge] (s) to[bend right=40] node[below]{$e_m$} (t);
	\end{tikzpicture}
\end{center}
	
We call a Vickrey flow $f$ in this network a \demph{dynamic equilibrium} if it satisfies
	\[f^+_e(\theta) > 0 \implies \cexittime(\theta) \leq \cexittime[e'](\theta)\]
for all $e,e' \in E$ and almost all $\theta \in \IRnn$.

\begin{enumerate}[label=\alph*)]
	\item Compare this definition to that of a Wardrop equilibrium (for static flows). Convince yourself that this is a sensible translation of this concept to the dynamic setting.
	\item Consider a network with three parallel edges with capacities $1$, $2$ and $4$ and travel times $1$, $6$ and $8$. Determine a dynamic equilibrium $f$ with 
		\[\sum_{e \in E}f^+_e(\theta) = \begin{cases}6, &\text{ for } \theta \in [0,8]\\0, &\text{ else}\end{cases}.\]
	\item Show that a Vickrey flow $f$ with support in $[0,T]$ is a dynamic equilibrium if and only if it satisfies the following variational inequality
		\begin{align}\tag{VI}\label{eq:VIDEParallelLink}
			\begin{gathered}
				\sum_{e \in E}\int_0^T \cexittime^f(\zeta)\big(g^+_e(\zeta)-f^+_e(\zeta)\big)\diff\zeta \geq 0\\
				\text{for all dynamic flows } (g^+_e) \text{ with } \sum_{e \in E}g^+_e(\theta) = \sum_{e \in E}f^+_e(\theta) \text{ for almost all } \theta \in \IR 
			\end{gathered}
		\end{align}
	
	\textit{Hint:} For the `if'-direction the set  $N \coloneqq \set{\theta \in [0,T] | f^+_{e'}(\theta) > 0 \text{ and } \cexittime[e'](\theta) > \cexittime[e''](\theta)}$ might be helpful. You may also use the fact that integrating a strictly negative function over a set of positive measure results in a negative number.
	
	\item Let $u \in \pInt[2]$ be a non-negative, bounded function with support in $[0,T]$. Show that there exists a dynamic equilibrium $f$ with $\sum_{e \in E}f^+_e(\theta) = u(\theta)$ for almost all $\theta \in \IR$.
	
	You may use (without proof) the following existence result for solutions to variational inequalities by \cite[Chapitre 2, Théorème 8.1]{Lions}:
	
	Let $C \subseteq \pInt[2][{[a,b]}]^m$ be a non-empty, closed, convex and bounded subset and $\mathcal{A}: C \to \pInt[2][{[a,b]}]^m$ a weak-strong sequentially continuous mapping. Then, there exists a solution $f \in C$ to the following variational inequality
		\begin{align}\tag{Lions}\label{eq:VILionis}
			\begin{gathered}
				\sum_{i=1}^m\int_a^b \mathcal{A}_i(\zeta)\big(g_i(\zeta)-f_i(\zeta)\big)\diff\zeta \geq 0\\
				\text{for all } g \in C
			\end{gathered}
		\end{align}
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item For any fixed time~$\theta$ if we consider $\cexittime(\theta)$ as the cost of edge~$e$ (under the given flow), the definition of dynamic equilibrium (at that point) is exactly the same as that of a Wardrop equilibrium in the static setting.
	
	\item The flow $f$ defined by
		\begin{align*}
			f^+_{e_1}(\theta) &\coloneqq \begin{cases}6, &\text{ for } \theta \in [0,1)\\2, &\text{ for } \theta \in [1,3)\\1, &\text{ for } \theta \in [3,8]\end{cases}
			& f^+_{e_2}(\theta) &\coloneqq \begin{cases}0, &\text{ for } \theta \in [0,1)\\4, &\text{ for } \theta \in [1,3)\\2, &\text{ for } \theta \in [3,8]\end{cases}
			& f^+_{e_3}(\theta) &\coloneqq \begin{cases}0, &\text{ for } \theta \in [0,1)\\0, &\text{ for } \theta \in [1,3)\\3, &\text{ for } \theta \in [3,8]\end{cases} \\
			f^-_{e_1}(\theta) &\coloneqq 1 \text{ for } \theta \in [1,16]
			& f^-_{e_2}(\theta) &\coloneqq 2 \text{ for } \theta \in [7,16]
			& f^-_{e_3}(\theta) &\coloneqq 3 \text{ for } \theta \in [13,16]
		\end{align*}
		is a Vickrey flow with the following queue length functions for the three edges:
		\begin{center}
			\begin{tikzpicture}[declare function = {
					Q1(\x) = and(x>=0,x<1)*x*5+and(x>=1,x<3)*(5+(x-1))+and(x>=3,x<8)*7+and(x>=8,x<15)*(7-(x-8));
					Q2(\x) = and(x>=1,x<3)*2*(x-1)+and(x>=3,x<8)*4+and(x>=8,x<10)*(4-2*(x-8));
					Q3(\x) = 0;
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=8.5,
					xmin=0,xmax=15.5,
					width=13cm, height=5cm]
					\addplot[blue,thick,domain=0:15.5,samples=400]{Q1(\x)}node[above,pos=.4]{$\ql[e_1]$};
					\addplot[red,thick,domain=0:15.5,samples=400]{Q2(\x)}node[above,pos=.4]{$\ql[e_2]$};
					\addplot[green,thick,domain=0:15.5,samples=400]{Q3(\x)}node[above,pos=.4]{$\ql[e_3]$};
				\end{axis}		
			\end{tikzpicture}
		\end{center}
		and, consequently, the following arrival time functions
		\begin{center}
			\begin{tikzpicture}[declare function = {
					Q1(\x) = and(x>=0,x<1)*x*5+and(x>=1,x<3)*(5+(x-1))+and(x>=3,x<8)*7+and(x>=8,x<15)*(7-(x-8));
					Q2(\x) = and(x>=1,x<3)*2*(x-1)+and(x>=3,x<8)*4+and(x>=8,x<10)*(4-2*(x-8));
					Q3(\x) = 0;
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=18.5,
					xmin=0,xmax=15.5,
					width=13cm, height=10cm]
					\addplot[blue,thick,domain=0:15.5,samples=400]{\x+1+Q1(\x)/1}node[right,pos=.1]{$\cexittime[e_1]$};
					\addplot[red,thick,domain=0:15.5,samples=400]{\x+6+Q2(\x)/2}node[above,pos=.03]{$\cexittime[e_2]$};
					\addplot[green,thick,domain=0:15.5,samples=400]{\x+8+Q3(\x)/4}node[above,pos=.15]{$\cexittime[e_3]$};
				\end{axis}		
			\end{tikzpicture}
		\end{center}	
		It is now easy to see that $f$ is in fact a dynamic equilibrium.
		
		\item First assume that $f$ is a dynamic equilibrium and $g$ any dynamic flow with $\sum_e f^+_e(\theta) = \sum_e g^+_e(\theta)$ for almost all $\theta \in \IR$. We then define the function
			\[\alpha: \IR \to \IR, \theta \mapsto \min\set{\cexittime(\theta) | e \in E}\]
		and for every edge $e \in E$ the set $M_e \coloneqq \set{\theta \in [0,T] | g^+_e(\theta) \geq f^+_e(\theta)}$. Then, we have
			\begin{align*}
				&\sum_{e \in E}\int_0^T \cexittime^f(\zeta)\big(g^+_e(\zeta)-f^+_e(\zeta)\big)\diff\zeta \\
					&\quad\quad= \sum_{e \in E}\Big(\int_{M_e} \underbrace{\cexittime^f(\zeta)}_{\geq\alpha(\zeta)}\big(\underbrace{g^+_e(\zeta)-f^+_e(\zeta)}_{\geq 0}\big)\diff\zeta+\int_{[0,T]\setminus M_e} \underbrace{\cexittime^f(\zeta)}_{=\alpha(\zeta)}\big(g^+_e(\zeta)-\underbrace{f^+_e(\zeta)}_{> g^*_e(\zeta)\geq 0}\big)\diff\zeta\Big) \\
					&\quad\quad\geq \sum_{e \in E}\int_0^T \alpha(\zeta)\big(g^+_e(\zeta)-f^+_e(\zeta)\big)\diff\zeta = \int_0^T \alpha(\zeta)\Big(\sum_e g^+_e(\zeta)- \sum_e f^+_e(\zeta)\Big)\diff\zeta = 0.
			\end{align*}
			
		Now, for the other direction let $f$ be a solution to \eqref{eq:VIDEParallelLink} and $e',e'' \in E$ two edges. Define $N \coloneqq \set{\theta \in [0,T] | f^+_{e'}(\theta) > 0 \text{ and } \cexittime[e'](\theta) > \cexittime[e''](\theta)}$ as the set of all times where we have positive inflow into edge~$e'$ even though edge~$e''$ would be better. We now have to show that this set has measure zero. For this, we define a new dynamic flow $g$ by choosing
			\[g^+_e(\theta) \coloneqq \begin{cases}
				0, &\text{ for } \theta \in N, e=e' \\ 
				f^+_{e''}(\theta) + f^+_{e'}(\theta), &\text{ for } \theta \in N, e=e'' \\
				f^+_e(\theta), &\text{ else}
			\end{cases}.\]
		In words: $g$ is the flow obtained from $f$ by shifting all inflow from $e'$ to $e''$ during $N$. We now have
			\begin{align*}
				0 &\leq \sum_{e \in E}\int_0^T \cexittime^f(\zeta)\big(g^+_e(\zeta)-f^+_e(\zeta)\big)\diff\zeta \\
					&= \int_N \cexittime[e'](\zeta)\big(0-f^+_{e'}(\zeta)\big)\diff\zeta + \int_N \cexittime[e''](\zeta)\big(f^+_{e''}(\zeta)+f^+_{e'}(\zeta)-f^+_{e''}(\zeta)\big)\diff\zeta \\
					&= \int_N (\underbrace{\cexittime[e''](\zeta)-\cexittime[e'](\zeta)}_{<0})\underbrace{f^+_{e'}(\zeta)}_{>0}\diff\zeta.
			\end{align*}
		This then directly implies that $N$ has measure zero.
		
	\item We define $C \coloneqq \set{(f^+_e) \in \pInt[2][{[0,T]}]^E | \sum_e f^+_e = u}$. This set is clearly non-empty, closed, convex and bounded (since $u$ is bounded). We also define 
		\[\mathcal{A}: C \to \pInt[2][{[0,T]}], (f^+_e) \mapsto (\cexittime^f)_{e \in E}\]
	where $f=(f^+_e,f^-_e)$ uses the (uniquely determined -- \cf \Cref*{lemma:EdgeLoadingExistence,lemma:EdgeLoadingUniqueness}) outflow rates which make the given inflow rates $f^+_e$ into a Vickrey flow. According to \Cref*{lemma:EdgeLoadingContinuous} and \Cref*{claim:IntegrationIsWeakStrongContinuous} within the proof of this \namecref{lemma:EdgeLoadingContinuous} this mapping is weak-strong sequentially continuous. 
	
	Thus, the resulting variational inequality has a solution which, by part c), is a dynamic equilibrium.
\end{enumerate}}

\exComment{}