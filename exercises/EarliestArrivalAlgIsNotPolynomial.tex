% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{EarliestArrivalAlgIsNotPolynomial}

\exTitle{Runtime and Complexity of Earliest-Arrival-algorithms}

\exProblem{\begin{enumerate}[label=\alph*)]
	\item Assume that we used the Earliest Arrival-algorithm (\Cref*{alg:EarliestArrival}) to compute an earliest arrival flow with time horizon~$T$ in a network~$\network$. Let $p_1,\dots,p_K \in \PathSet^\leftrightarrow$ be the augmentation paths found by the Successive Shortest Path-algorithm and $\alpha_1, \dots, \alpha_K > 0$ the corresponding step sizes. 
	
	Use this information to describe the function
		\[V: \IRnn \to \IRnn, T \mapsto \max\set{\fval{f,T} | f \text{ a feasible dynamic flow in } \network}\]
	on the interval $[0,T]$.
	
	\item Consider now the network $\network_2$:
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex](s)at(0,0){$s$};
				\node[namedVertex](v1)at(3,0){$v_1$};
				\node[namedVertex](v2)at(3,-2){$v_2$};
				\node[namedVertex](w1)at(8,0){$w_1$};
				\node[namedVertex](w2)at(8,-2){$w_2$};
				\node[namedVertex](t)at(11,0){$t$};
				
				\draw[edge](s)--node[above,sloped]{$(1,0)$}(v1);
				\draw[edge](s)--node[above,sloped]{$(3,0)$}(v2);
				\draw[edge](v1)--node[above,sloped]{$(\infty,0)$}(w1);
				\draw[edge](v1)--node[above,sloped,pos=.8]{$(\infty,1)$}(w2);
				\draw[edge](v2)--node[above,sloped,pos=.2]{$(\infty,1)$}(w1);
				\draw[edge](w1)--node[above,sloped]{$(2,0)$}(t);
				\draw[edge](w2)--node[above,sloped]{$(2,0)$}(t);
			\end{tikzpicture}
		\end{center} 
		(note that the costs are zero on all edges except for the inner ones and the inner edges all have infinite capacity)
		
		Determine the function $V$ for this network.
		
	\item Next we construct network $\network_3$ from $\network_2$ by adding two nodes $v_3$ and $w_3$ as well as edges from and towards these new nodes as follows:
		\begin{center}
			\begin{tikzpicture}
				\node[namedVertex,gray](s)at(0,0){$s$};
				\node[namedVertex,gray](v1)at(3,0){$v_1$};
				\node[namedVertex,gray](v2)at(3,-2){$v_2$};
				\node[namedVertex](v3)at(3,-4){$v_3$};
				\node[namedVertex,gray](w1)at(8,0){$w_1$};
				\node[namedVertex,gray](w2)at(8,-2){$w_2$};
				\node[namedVertex](w3)at(8,-4){$w_3$};
				\node[namedVertex,gray](t)at(11,0){$t$};
				
				\draw[edge,gray](s)--node[above,sloped]{$(1,0)$}(v1);
				\draw[edge,gray](s)--node[above,sloped]{$(3,0)$}(v2);
				\draw[edge](s)--node[above,sloped]{$(5,0)$}(v3);
				\draw[edge,gray](v1)--node[above,sloped]{$(\infty,0)$}(w1);
				\draw[edge,gray](v1)--node[above,sloped,pos=.9]{$(\infty,1)$}(w2);
				\draw[edge](v1)--node[above,sloped,pos=.85]{$(\infty,3)$}(w3);
				\draw[edge,gray](v2)--node[above,sloped,pos=.1]{$(\infty,1)$}(w1);
				\draw[edge](v2)--node[below,sloped,pos=.8]{$(\infty,3)$}(w3);
				\draw[edge](v3)--node[above,sloped,pos=.15]{$(\infty,3)$}(w1);
				\draw[edge](v3)--node[below,sloped,pos=.2]{$(\infty,3)$}(w2);
				\draw[edge,gray](w1)--node[above,sloped]{$(2,0)$}(t);
				\draw[edge,gray](w2)--node[above,sloped]{$(2,0)$}(t);
				\draw[edge](w3)--node[above,sloped]{$(5,0)$}(t);
			\end{tikzpicture}
		\end{center}
		
		Determine the function $V$ for this network as well.
		
		\item \textit{Bonus:} In general, network~$\network_{n+1}$ is constructed from network~$\network_n$ by adding two new nodes $v_{n+1}$ and $w_{n+1}$ as well as the following edges:
		\begin{itemize}
			\item $sv_{n+1}$ with cost~$0$ and capacity~$2^n+2^{n-2}$,
			\item $v_{n+1}w_k$ and $v_kw_{n+1}$ with cost~$2^n-1$ and capacity~$\infty$ for all $k \in [n]$ and
			\item $w_{n+1}t$ with cost~$0$ and capacity~$2^n+2^{n-2}$.
		\end{itemize}
		Can you make any conjecture about the function~$V$ in these networks? You do not need to formally prove anything here.
		
		\textit{Hint:} Can you find any connection between your computations from part b) and c)? Pay particular attention to which of inner the edges~$v_jw_k$ are part of each augmenting path.
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item The function $V$ is zero on $\tauPmin = \tau_{p_1}$. Afterwards it is continuous and piecewise linear with a slope of $\sum_{j=1}^k\alpha_j$ during $[\tau_{p_k},\tau_{p_{k+1}}]$.
		
	\item The augmentation paths found by SSP in this network are (in this order): 
		\begin{itemize}
			\item $sv_1,v_1w_1,w_1t$ with a cost of $0$
			\item $sv_2,v_2w_1,w_1t$ with a cost of $1$
			\item $sv_2,v_2w_1,w_1v_1,v_1w_2,w_2t$ with a cost of $2$
		\end{itemize}
		The augmentation step size is always $1$. Thus, the function $V$ is given by
		\begin{center}
			\begin{tikzpicture}[declare function = {
					V(\x) = (x>=0)*x+(x>=1)*(x-1)+(x>=2)*(x-2);
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=5.5,
					xmin=0,xmax=3.5,
					xtick={1,2,3},
					ytick={1,2,3,4,5},
					width=5cm, height=5cm]
					\addplot[blue,thick,domain=0:3.5,samples=400]{V(\x)}node[left,pos=.6]{$V$};
					\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:1,1){};
					\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:2,3){};
				\end{axis}		
			\end{tikzpicture}
		\end{center}
		
	\item The augmentation paths found by SSP in this network are (in this order): 
	\begin{itemize}
		\item $sv_1,v_1w_1,w_1t$ with a cost of $0$
		\item $sv_2,v_2w_1,w_1t$ with a cost of $1$
		\item $sv_2,v_2w_1,w_1v_1,v_1w_2,w_2t$ with a cost of $2$
		\item $sv_2,v_2w_3,w_3t$ with a cost of $3$
		\item $sv_3,v_3w_2,w_2t$ with a cost of $3$
		\item $sv_3,v_3w_2,w_2v_1,v_1w_1,w_1v_2,v_2w_3,w_3t$ with a cost of $6-2=4$
		\item $sv_3,v_3w_1,w_1v_2,v_2w_3,w_3t$ with a cost of $6-1=5$
		\item $sv_3,v_3w_1,w_1v_1,v_1w_3,w_3t$ with a cost of $6-0=6$.
	\end{itemize}
	The augmentation step size is always $1$. Thus, the function $V$ is given by
	\begin{center}
		\begin{tikzpicture}[declare function = {
				V(\x) = (x>=0)*x+(x>=1)*(x-1)+(x>=2)*(x-2)+(x>=3)*2*(x-3)+(x>=4)*(x-4)+(x>=5)*(x-5)+(x>=6)*(x-6);
			}]
			\begin{axis}[
				axis lines=left,,
				ymin=0,ymax=30.5,
				xmin=0,xmax=7.5,
				xtick={1,2,3,4,5,6,7},
				width=10cm, height=20cm]
				\addplot[blue,thick,domain=0:7.5,samples=400]{V(\x)}node[left,pos=.6]{$V$};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:1,1){};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:2,3){};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:3,6){};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:4,11){};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:5,17){};
				\node[draw,circle,blue,fill=blue,thick,inner sep=.7]()at(axis cs:6,24){};
			\end{axis}		
		\end{tikzpicture}
	\end{center}
	
	\item Comparing the augmentation paths in b) and c) we make the following observation: If we call the three paths used in b) $p_1$, $p_2$ and $p_3$, then the six of the eight paths used in c) are $p_1$, $p_2$, $p_3$, $sv_3,\reve[p_3],w_3t$, $sv_3,\reve[p_2],w_3t$, $sv_3,\reve[p_1],w_3t$ (with two extra paths in-between the first three and second three). In other words: SSP in~$\network_3$ first does the same augmentations as in~$\network_2$ and then undoes all the augmentations again (in reverse order). 
	
	Moreover, each augmentation increases the flow volume by~$1$ and most of the augmentations have different costs. At the end all edges leaving $s$ are saturated (except for the newest edge which has a remaining capacity of $1$). Assuming that this pattern continues, we can deduce that for $\network_n$, SSP will require $\approx 2^n$ augmentations. If most of them have a different cost, this implies that the function~$V$ in this network will consist of $\approx2^n$ linear pieces. 
	
	Note that this implies both that the earliest arrival algorithm has exponential runtime in~$n$ and that the complexity of an earliest arrival flow has complexity exponential in~$n$.
	
	If one wants to formalize the claimed behaviour of SSP, one can prove via induction on~$n$ the following properties of a run of SSP on~$\network_n$:
		\begin{itemize}
			\item SSP makes $2^n+2^{n-2}-2$ augmentation of size $1$ each. 
			\item The cost of the last augmentation path is $2^n-2$ and there has been at least one augmentation path of each smaller integer cost.
			\item At the end all outer edges are saturated except for edges $sv_n$ and $w_nt$ which have a remaining capacity of $1$.
		\end{itemize}
		
	The base case ($n=2$) is just part b). For the induction step ($n\to n+1$) one shows that SSP in $\network_{n+1}$ behaves as follows: It first uses the exact same $N\coloneqq 2^n+2^{n-2}-2$ augmentations as in $\network_n$ (note that the cost of the last path used there is $2^n-2$ which is strictly less than the cost of~$2^n-1$ of any of the newly added inner edges). Then, it makes two augmentations using paths $sv_n,v_nw_{n+1},w_{n+1}t$ and $sv_{n+1},v_{n+1}w_n,w_nt$ of cost $2^n-1$. After that, all augmentation paths have to start with edge~$sv_{n+1}$ and end with edge~$w_{n+1}t$ (as all other edges starting at $s$ or entering $t$ are saturated). In-between those two edges we can now use the previous augmentation paths reversed and in reversed order: \Ie if the first paths where $p_1, \dots, p_N$ with costs $c_1, \dots, c_N$, we now use $sv_{n+1},\reve[p_N],w_{n+1}t, \dots, sv_{n+1},\reve[p_1],w_{n+1}t$ which have costs $2^{n+1}-2-c_N, \dots, 2^{n+1}-2-c_1$. 
\end{enumerate}}

\exComment{The instance is taken from \cite{BadInstanceForSSP}}