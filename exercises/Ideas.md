# Static Flows #

* There exists a edge-based flow without a path decomposition. Moreover, path decomposition is not unique

## Max Flow ##

* Example where FF takes many iterations

* Find an example where naive FF (without residual edges) does not work -> maybe in lecture

* Apply FF to find max flow and min cut


## Min-cost flow ##

* Use SSP to compute max flow of given cost


## Wardrop ##

* Braess-Paradoxon


------------------------------------------------------------------
# Dynamic Max Flow #

* Show that the Earliest Arrival Flow found by our algorithm is also a latest departure flow (cf. Fleischer/Tardos, section 5).


------------------------------------------------------------------
# Vickrey-Flows #

* Schwache Version von "operates at capacity": Ohne minimum im (Q = 0)-Fall. Immer noch eindeutig? Enthält ursprüngliche Vickrey-Flows?
	-> Evtl. auch Abschwächungen der anderen Charakterisierungen

* Properties of linear edge delays

* FIFO-property of Vickrey (+ why is compatibility important?)

* a < b ohne Einfluss dazwischen und mit positiver queue bei b. Dann gilt a + Q(a)/nu = b + Q(b)/nu (KS Prop 1(iii))

* We have U(\theta) - value(f,\theta) = ... for any flow satisfying weak flow conservation on edges and strong flow conservation at nodes.

* Let $T$ be some time such that there is no network inflow after time $T$ and the total flow volume in $N$ is 0$ at time $T$. After that we only have flow on cycles of length zero...

* restate the definition of corresponding direct dynamic flow in the same way as we defined corresponding Vickrey flow


------------------------------------------------------------------
# IDE #

* Deduce explicit termination bound from the termination proof -> maybe also connect to PoA?

* Find an example with high termination time

* Maybe an exercise showing a simplified version of the lower bound termination time example? a) Describe (approximately) an IDE in the cycling gadget (charging phase), b) describe (approximately) an IDE in the cycling gadget (blocking phase), c) describe an (approximate) IDE in a blocking gadget d) Describe an approximate IDE in the full network (all for a fixed network-size!)

* Computing IDEs and complexity: Compute IDE in the many-phase instance

* Waterfilling. Explain name...

* Can we model IDE by a suitable choice of \Psi? (no) Maybe explicitly give the choice of \Psi?


--------------------------------------------------------------------
# DE #

* Convergence wrt uniform norm implies convergence wrt L^p norm on bounded intervals. 

* Maybe something about DE with departure time choice? Model definition, existence(?), non-existence

