% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{PathDecompositionDirectFlows}

\exTitle{Path decomposition for dynamic flows}

\exProblem{\begin{enumerate}[label=\alph*)]
		\item Show that there exist direct dynamic flows $(f^+_e)$ that do not have a path-decomposition, \ie there is no path-based dynamic flow such that $(f^+_e)$ is the corresponding direct dynamic flow.
		\item \textit{Bonus:} What about \emph{walk}-decompositions? \Ie is it possible to decompose a direct dynamic flow into a walk-based dynamic flow (maybe with some additional assumptions on the network/flow)? 
		
		You do not need to provide a complete formal argument here!
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item Consider the network
			\begin{center}
				\begin{tikzpicture}
					\node[namedVertex](s)at(0,0){$s$};
					\node[namedVertex](v1)at(3,0){$v_1$};
					\node[namedVertex](v2)at(6,0){$v_2$};
					\node[namedVertex](v3)at(4.5,2){$v_3$};
					\node[namedVertex](t)at(9,0){$t$};
					
					\draw[edge] (s) --node[below,sloped]{$(1,1)$} (v1);
					\draw[edge] (v1) --node[below,sloped]{$(1,1)$} (v2);
					\draw[edge] (v2) --node[below,sloped]{$(1,1)$} (t);
					\draw[edge] (v2) --node[above,sloped]{$(1,1)$} (v3);
					\draw[edge] (v3) --node[above,sloped]{$(1,1)$} (v1);
				\end{tikzpicture}
			\end{center}
			and the following dynamic flow:
				\[f^+_{sv_1} \coloneqq \CharF[{[0,1]}], \quad f^+_{v_1v_2} \coloneqq \CharF[{[1,2]}]+\CharF[{[4,5]}], \quad f^+_{v_2v_3} \coloneqq \CharF[{[2,3]}], \quad f^+_{v_3v_1} \coloneqq \CharF[{[3,4]}], \quad f^+_{v_2t} \coloneqq \CharF[{[5,6]}].\]
			This flow is a direct dynamic flow (essentially the direct flow corresponding to the walk-based flow with $f^+_{sv_1,v_1v_2,v_2v_3,v_3v_1,v_1v_2,v_2t} \coloneqq \CharF[{[0,1]}]$) but does not have a path-decomposition (as there is no \stpath{} containing edge $v_2v_3$).
			
		\item If the flow has unbounded support, even a walk decomposition may not suffice. To see this take the same network as in a) and consider the follow dynamic flow
				\[f^+_{sv_1} \coloneqq \CharF[{[0,3]}], \quad f^+_{v_1v_2} \coloneqq \CharF[{[1,\infty)}], \quad f^+_{v_2v_3} \coloneqq \CharF[{[2,\infty)}], \quad f^+_{v_3v_1} \coloneqq \CharF[{[3,\infty)}], \quad f^+_{v_2t} \coloneqq 0.\]
			This flow essentially sends all flow around the cycle in the middle of the network forever. It clearly has no walk-decomposition.
			
			For flows with bounded support (\ie there exists some time horizon $T\geq 0$ such that we have $f^+_e(\theta) = 0$ for almost all $\theta \geq T$ and all $e \in E$), then it might still be impossible to find a walk-decomposition if the network contains cycles of zero travel time (as flow may travel around such a cycles without ever originating at the source).
			
			However, in network without cycles of zero travel time and for flows with finite support a walk-decomposition is always possible. To prove this, one can take a similar approach as in the static setting. The general idea for this proof is as follows: Let $T > 0$ be such that the support of $f$ is in $[0,T]$ and $\eps > 0$ such that there is no cycle with shorter travel time than $\eps$ in the network. We then start by partitioning $[0,T]$ into intervals of length at most $\eps$. For every edge~$e$ and each of those intervals $[a,b]$ we compute the cumulative inflow into~$e$ between~$a$ and~$b$. If this value is always zero, then we are done. Otherwise, take any edge-interval pair where this is not the case. Using strong flow conservation we can then (as in the static case) inductively construct an \stwalk{} $p$ containing this edge as $k$-th edge (for some~$k$) such that
				\[g: [a',b'] \to \IRnn, \theta \mapsto \min\set{f^+_{p[j]}(\theta+\tau_{p\vert_{<j}}) | j=1,\dots,\abs{p}}\]
			satisfies $\int_{a'}^{b'}g(\zeta)\diff\zeta > 0$. Here, we use $p[j]$ to refer to the $j$-th edge of~$p$ and $p|_{<j}$ for the subwalk of~$p$ up to but excluding the $j$-th edge. Moreover, we have $a' \coloneqq a-\tau_{p|_{<k}}$ and $b' \coloneqq b-\tau_{p|_{<k}}$. Note that our assumption on~$\eps>0$ not only ensures that such a (finite) \stwalk{} does in fact exists but also guarantees that if an edge appears multiple time along~$p$ the corresponding intervals do not overlap. Thus, we can remove flow according to~$g$ along this walk and add this to the walk-inflow of~$p$. After repeating this step finitely many times (there are only finitely many \stwalk s with length of at most~$T$), edge~$e$ carries no flow any more. Doing this to all edges of the network, thus, produces a walk decomposition of the given flow.
\end{enumerate}}

\exComment{The network in a) is the same one used as a counter example to the existence of path-decomposition for static flows in StaticFlowPathDecomposition. The first counter example in b) comes from \cite[Example 3.48]{KochThesis}}