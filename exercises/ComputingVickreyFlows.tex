% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{ComputingVickreyFlows}

\exTitle{Computing Vickrey-Flows}

\exProblem{\begin{enumerate}[label=\alph*)]
	\item We consider an edge~$e$ with capacity~$\nu_e=2$ and travel time~$\tau_e=1$. Find an edge outflow rate $f^-_e: \IR \to \IRnn$ such that it forms a Vickrey-flow together with the following edge inflow rate:
		\[f^+_e: \IR \to \IRnn, \theta \mapsto \begin{cases}
			0,	&\text{ for } \theta < 0 \\
			2,	&\text{ for } \theta \in [0,2) \\
			4,	&\text{ for } \theta  \in [2,3) \\
			3,	&\text{ for } \theta  \in [3,5) \\
			1,	&\text{ for } \theta \geq 5 \\
		\end{cases}\]
	Also compute the corresponding queue length function.
	
	\item Now let $(f^+_e,f^-_e)$ be any Vickrey-flow on some edge~$e$ with capacity~$\nu_e$ and travel time~$\tau_e$. Assume that $f^+_e$ is constant on some interval $[a,b) \subseteq \IRnn$. Describe the edge outflow rate $f^-_e$ on the interval $[\cexittime(a),\cexittime(b))$.
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item The edge outflow rate has the form
		\[f^-_e: \IR \to \IRnn, \theta \mapsto \begin{cases}
			0,	&\text{ for } \theta < 1 \\
			2,	&\text{ for } \theta \in [1,10) \\
			1,	&\text{ for } \theta \geq 10 \\
		\end{cases}\]
		while the queue length function is
		\[\ql: \IR \to \IRnn, \theta \mapsto \begin{cases}
			0,					&\text{ for } \theta < 2 \\
			(\theta-2)\cdot2,	&\text{ for } \theta \in [2,3) \\
			2+(\theta-3),		&\text{ for } \theta  \in [3,5) \\
			4-(\theta-5),		&\text{ for } \theta  \in [5,9) \\
			0,	&\text{ for } \theta \geq 9 \\
		\end{cases}.\]
		
		\begin{center}
			\begin{tikzpicture}[declare function = {
					fp(\x) = and(x>=0,x<2)*2+and(x>=2,x<3)*4+and(x>=3,x<5)*3+(x>=5);
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=5.5,
					xmin=-.5,xmax=11.5,
					width=13cm, height=5cm]
					\addplot[blue,thick,domain=-.5:11.5,samples=400]{fp(\x)}node[above,pos=.4]{$f^+_e$};
				\end{axis}		
			\end{tikzpicture}
			
			\begin{tikzpicture}[declare function = {
					Q(\x) = and(x>=2,x<3)*(x-2)*2+and(x>=3,x<5)*(2+(x-3))+and(x>=5,x<9)*(4-(x-5));
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=5.5,
					xmin=-.5,xmax=11.5,
					width=13cm, height=5cm]
					\addplot[blue,thick,domain=-.5:11.5,samples=400]{Q(\x)}node[above,pos=.4]{$\ql$};
				\end{axis}		
			\end{tikzpicture}
			
			\begin{tikzpicture}[declare function = {
					fm(\x) = and(x>=1,x<10)*2+(x>=10);
				}]
				\begin{axis}[
					axis lines=left,,
					ymin=0,ymax=5.5,
					xmin=-.5,xmax=11.5,
					width=13cm, height=5cm]
					\addplot[blue,thick,domain=-.5:11.5,samples=400]{fm(\x)}node[above,pos=.5]{$f^-_e$};
				\end{axis}		
			\end{tikzpicture}
		\end{center}
		
	\item Let $x \in \IRnn$ be the value of $f^+_e$ during $[a,b)$ (\ie $f^+_e(\theta)=x$ for all $\theta \in [a,b)$). We now distinguish three cases:
		\begin{proofbycases}
			\caseitem{$\ql(a)=0$ and $x \leq \nu_e$} Here, we have $\ql(\theta)=0$ for all $\theta \in [a,b)$. To prove this, define $\bar\theta \coloneqq \max\set{\theta' \leq \theta | \ql(\theta')=0} \geq a$. We then have
				\begin{align*}
					\ql(\theta) = F^+_e(\theta)-F^-_e(\theta+\tau_e)
						&\Croverset{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts}{=} F^+_e(\theta)-\big(F^+_e(\bar\theta)+(\theta-\bar\theta)\nu_e\big)\\
						&\toverset{\phantom{Lem. 4.3c}}{=}(\theta-\bar\theta)x-(\theta-\bar\theta)\nu_e \leq 0.
				\end{align*}
				Since the queue operates at capacity, this directly implies $f^-_e(\theta)=f^+_e(\theta-\tau_e)=x$ for almost all $\theta \in [a+\tau_e,b+\tau_e) = [\cexittime(a),\cexittime(b))$.
				
			\caseitem{$x > \nu_e$} Here, we have $\ql(\theta) > 0$ for all $\theta \in (a,\cexittime(b)-\tau_e)$: For $\theta \in [a,b)$ we get this from 
				\[\ql(\theta) = \ql(a) + \int_a^\theta \deriv{\ql}(\zeta)\diff\zeta \Croverset{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv}{\geq} \ql(a) + (x-\nu_e)(\theta-a) > \ql(a) \geq 0.\]
			For $\theta \in [b,\cexittime(b)-\tau_e)$ we have
				\begin{align*}
					\ql(\theta) 
						&= \ql(b) - \int_b^\theta \deriv{\ql}(\zeta)\diff\zeta \Croverset{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv}{\geq} \ql(b) + (0-\nu_e)(\theta-b) \\
						&> \ql(b) - \nu_e(\cexittime(b)-\tau_e-b) = 0.
				\end{align*}
			Again, since the queue operates at capacity, this directly implies $f^-_e(\theta)=\nu_e$ for almost all $\theta \in [\cexittime(a),\cexittime(b))$.
			
			\caseitem{$\ql(a) > 0$ and $x \leq \nu_e$} Here, we define $c \coloneqq \min\set{a+\frac{\ql(a)}{\nu_e-x},b}$. Then we have $\ql(\theta) > 0$ for all $\theta \in [a,c)$ since we have
				\[\ql(\theta) = \ql(a) + \int_a^\theta \deriv{\ql}(\zeta)\diff\zeta \Croverset{lemma:CharacterizationsOfQueueOpAtCap:QueueDeriv}{\geq} \ql(a) + (x-\nu_e)(\theta-a) \geq \ql(a) > 0.\]
			Consequently, we have $f^-_e(\theta)=\nu_e$ for almost all $\theta \in [a+\tau_e,c+\tau_e]$.
			
			Now, if $c < b$, then this implies
				\[\ql(c) = \ql(a)+(x-\nu_e)(c-a) = \ql(a)+(x-\nu_e)\Big(a+\frac{\ql(a)}{\nu_e-x}-a\Big)=0\]
			Hence, during $[c,b)$ we are in the situation of case~1 (with $c$ instead of $a$), implying that we have $\ql(\theta)=0$ for all $\theta \in [c,b)$ and $f^-_e(\theta)=x$ for almost all $\theta \in [c+\tau_e,b+\tau_e)=[c+\tau_e,\cexittime(b))$.
			
			If, on the other hand, we have $c=b$, then we have $\ql(\theta) > 0$ for all $\theta \in [c,\cexittime(b)-\tau_e)$ by the same prove as in case~2 which then implies $f^-_e(\theta) = \nu_e$ for almost all $\theta \in [c+\tau_e,\cexittime(b))$.
		\end{proofbycases}
\end{enumerate}}

\exComment{\cite[Proposition 3.22]{GrafThesis}. Part b) currently included as \Cref*{ex:VickreyFlowStructurePreserving} in the lecture notes.}