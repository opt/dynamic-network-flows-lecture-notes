% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{VickreyFlowPathArrivalTime}

\exTitle{Bounding the Termination Time in a Single-Path Network}

\exProblem{Let $\network$ be a dynamic flow network which consists of a single \stpath, \ie we have $E = \set{e_1, \dots, e_m}$ and $\PathSet = \set{(e_1,e_2,\dots,e_m)}$. Let $f$ be a dynamic flow in this network which is a Vickrey flow on every edge and satisfies strong flow conservation at all nodes. Moreover, let $T \in \IR$ be some time horizon such that we have $\supp(f^+_{e_1}) \subseteq [0,T]$ (\ie all outflow from the source happens before time $T$). 
	
\begin{center}
	\begin{tikzpicture}
		\node[namedVertex] 	(s) at (0,0) {$s$};
		\node[namedVertex] 	(v) at (2,0) {$v$};
		\node 				(w) at (5,0) {$\dots$};
		\node[namedVertex] 	(t) at (8,0) {$t$};
		
		\draw[edge] (s) --node[above]{$e_1$} (v);
		\draw[edge] (v) --node[above]{$e_2$} ($(w)+(-1,0)$);
		\draw[edge] ($(w)+(1,0)$) --node[above]{$e_m$} (t);
	\end{tikzpicture}
\end{center}

Show that there exists some time $\tilde T \in \IRnn$ such that we have $\supp(f^+_{e_i}) \subseteq [0,\tilde T]$ for all $i \in [m]$.

\textit{Bonus:} If you have found an explicit formula for $\tilde T$: Is your bound best possible?}

\exSolution{A simple upper bound can be derived from \Cref*{ex:VickreyFlowNoIdleing}: Denote by $v \coloneqq F^+_{e_1}(T)$ the total flow volume that ever enters the first edge~$e_1$. Moreover, let $x \coloneqq F^+_{e_1}(T)-F^-_{e_1}(T)$. Then, by \Cref*{ex:VickreyFlowNoIdleing}, we have
	\[F^-_{e_1}(T+\tau_{e_1}+\tfrac{x}{\nu_{e_1}}) \geq F^-_{e_1}(T)+x=F^+_{e_1}(T)=v,\]
\ie all flow has left edge~$e_1$ by time $T+\tau_{e_1}+\tfrac{x}{\nu_{e_1}}$. Moreover, we can clearly upper bound $x$ by $v$ to get an upper bound of $T+\tau_{e_1}+\tfrac{v}{\nu_{e_1}}$ for the time after which the outflow from edge~$e_1$ will be zero. Now, repeating this process for the following edge~$e_2$ (with $T+\tau_{e_1}+\tfrac{v}{\nu_{e_1}}$ in place of~$T$) gives us an upper bound of $T+\tau_{e_1}+\tfrac{v}{\nu_{e_1}}+\tau_{e_2}+\tfrac{v}{\nu_{e_2}}$ for the time at which the last particle leaves the second edge. Continuing this way we finally arrive at $\tilde T \coloneqq T + \sum_{i=1}^m\Big(\tau_{e_i}+\tfrac{v}{\nu_{e_i}}\Big)$ for an upper bound on the time such that all flow has arrived at the sink.

The same upper bound can also be derived in the following way (described only informally here): Since $v$ is an upper bound to the total volume of flow that ever is in the network, it is also an upper bound on any queue length. Thus, we have
	\[\cexittime[e_m]\circ \dots \circ \cexittime[e_2]\circ\cexittime[e_1](T) \leq T + \sum_{i=1}^m\Big(\tau_{e_i}+\tfrac{v}{\nu_{e_i}}\Big) = \tilde T.\]
Since the arrival times are non-decreasing this shows that the same upper bound holds for all $\theta \leq T$, \ie any particle entering the path during $[0,T]$ will have arrived by time~$\tilde T$.

A better upper bound can be derived using \Cref*{ex:VickreyFlowMonotonicity} as follows: For any (small enough) $\eps > 0$ we can define a Vickrey flow $g$ as follows: 
	\begin{align*}
		&g^+_{e_1} \coloneqq \tfrac{v}{\eps}\cdot\CharF[{[T,T+\eps]}],\quad 
			&&g^-_{e_1} \coloneqq \nu_{e_1}\cdot\CharF[{[T+\tau_{e_1},T+\tau_{e_1}+\tfrac{v}{\nu_{e_1}}]}] \\
		&g^+_{e_2} \coloneqq g^-_{e_1},\quad	
			&&g^-_{e_2} \coloneqq \min\set{\nu_{e_1},\nu_{e_2}}\cdot\CharF[{[T+\tau_{e_1}+\tau_{e_2},T+\tau_{e_1}+\tau_{e_2}+\tfrac{v}{\min\set{\nu_{e_1},\nu_{e_2}}}]}] \\
		&&\vdots& \\
		&g^+_{e_m} \coloneqq g^-_{e_{m-1}},\quad	
		&&g^-_{e_m} \coloneqq \min_i \nu_{e_i}\cdot\CharF[{[T+\sum_i\tau_{e_i},T+\sum_i\tau_{e_i}+\tfrac{v}{\min_i\nu_{e_i}}]}]
	\end{align*}
It is easy to see that this does in fact describe a Vickrey flow. Moreover, we have $G^+_{e_1} \leq F^+_{e_1}$ which, by \Cref*{ex:VickreyFlowMonotonicity}, implies that we also have $G^+_{e_2} = G^-_{e_1} \leq F^-_{e_1} = F^+_{e_2}$. Inductively, this shows that we have $G^-_{e_m} \leq F^-_{e_m}$. In particular, we have 
	\[v \geq F^-_{e_m}(\hat T) \geq G^-_{e_m}(\hat T) = v\]
for $\hat T \coloneqq T+\sum_i\tau_{e_i}+\tfrac{v}{\min_i\nu_{e_i}}$. Hence, $\hat T$ is an upper bound for the last arrival time of a particle at the sink under~$f$. 

Since we clearly have $\hat T < \tilde T$, this already shows that $\tilde T$ is not best possible. At the same time $\hat T$ is (asymptotically) tight since defining $f$ as $g$ shifted by $-\eps$ results in a flow which still has flow in the network until $\hat T - \eps$.
}

\exComment{}