% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{WardropEquilibriumComputationViaWaterFilling}

\exTitle{Computing Wardrop equilibria in parallel link networks}

\exProblem{In this exercise we consider \stnetwork s consisting only of parallel $s$,$t$-edges and piecewise linear edge cost functions. Our goal is to describe an algorithm for computing Wardrop equilibria of any given value in such networks.
\begin{enumerate}[label=\alph*)]
	\item Consider first the case where all edge costs are simple linear functions of the form $\gamma_e(x_e) = a_e\cdot x_e$ (with $a_e > 0$). How do Wardrop equilibria look like here? How can we compute them?
	\item Next, consider affine edge cost functions of the form $\gamma_e(x_e) = a_e\cdot x_e + b_e$.
	\item Finally, consider the case of general piecewise linear edge cost functions and generalize your algorithm for this setting.
\end{enumerate}
\textit{Hint:} How does a Wardrop equilibrium of value $0$ look like? What happens if we slowly increase the desired flow value from here?
\begin{enumerate}[label=\alph*),resume]
	\item \textit{Bonus:} What can you deduce from your algorithm about the following function?
		\[C: \IRnn \to \IR, v \mapsto \fcosts{x} \text{ where $(x_p)$ is a Wardrop equilibrium of value $v$}\]
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
		\item Assume we want to find a Wardrop equilibrium of value $v \in \IRnn$. We have to choose $x_e$ such that $\sum_{e \in E}x_e = v$ as well as $a_ex_e=a_{e'}x_{e'}$ for all $e,e' \in E$. \Ie for any pair of edges $e,e'$ the split between these two edges must be in the ratio $a_{e'}:a_e$. In general any flow of the form $x_e = \frac{d}{a_e}$ is a Wardrop equilibrium of value $\sum_{e \in E}\frac{d}{a_e}$. Thus, choosing $d=v\cdot\Big(\sum_{e \in E}\frac{1}{a_e}\Big)^{-1}$ gives us the desired Wardrop equilibrium of value~$v$.
		
		Note that the process of obtaining this equilibrium can also be described as follows: We (simultaneously) increase the flow values on all edges until the total flow distributed equals the desired value~$v$. The increase happens at different rates on each edge, namely at a rate of $\nicefrac{1}{a_e}$ on edge~$e$.
		
		\item We use the same general approach as in a) but at the start only send flow into those edges with minimal value of $b_e$. As soon as the (current) cost on those edges reaches $b_e$ from some currently not used edge, we also include this edge into the flow distribution.
		
		\item Generalizing the approach from b) even further leads to the following approach (which is sometimes called a ``water-filling procedure''):
		
		\begin{algorithm}[H]
			\Input{A set of (non-decreasing) piecewise linear functions $\gamma_e: \IRnn \to \IR$ and some $v \in \IRnn$}
			\Output{A vector $(x_e) \in \IRnn^E$ such that $\sum_e x_e=v$ and $x_e > 0 \implies \gamma_e(x_e) \leq \gamma_{e'}(x_{e'})$ \fa $e,e'$}
			
			define $x_e \coloneqq 0$ \fa $e \in E$ and $b \coloneqq 0$
			
			\While{$b < v$}{
				define $A \coloneqq \arg\min\set{\gamma_e(x_e) | e \in E}$, $c \coloneqq \gamma_e(x_e)$ for some $e \in A$
				
				\tcc{Invariant: $(x_e)$ is Wardrop equilibrium of value $b$ with $\gamma_e(x_e) = c$ \fa $e \in A$ and $x_e = 0$ \fa $e \notin A$}
				
				for any edge $e \in A$ define $a_e$ as the slope of this edge right after $x_e$
				
				simultaneously increase $x_e$ on the edges in $A$ at a rate of $\frac{1}{a_e}$, $b$ at a rate of $\sum_{e \in A} \frac{1}{a_e}$ and $c$ at a rate of $1$ until either $b=v$ or the slope of some edge $e \in A$ changes or $c=\gamma_e(0)$ for some $e \notin A$
			}
		\end{algorithm}
		
	\item First of all the function $C$ is well defined as the edge costs in Wardrop equilibria are unique (\cf \Cref*{thm:WardropEquilibriumUniqueness:NonDecreasingCost}). Moreover, the function essentially just describes how the value of~$c$ changes throughout the algorithm from c). In particular, it is piecewise linear (and continuous). Note that, in contrast to the analogous function for min-cost flows (\cf exercise 3 from sheet 3), the function~$C$ here need not be convex in general.
\end{enumerate}}

\exComment{The exercise referenced in part d) is MinCostPiecewiseLinearInFlowValue}