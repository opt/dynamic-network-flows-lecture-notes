% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{WaterfillingAlgorithm}

\exTitle{The Water Filling Algorithm}

\newcommand{\tempWaterfillingAlgorithmGraphH}[5][]{
	\begin{axis}[
		axis lines=left,,
		ymin=0,ymax=5,
		xmin=0,xmax=7,
		width=6cm, height=4cm,
		xtick=\empty,xticklabel=\empty,
		ytick=\empty,yticklabel=\empty,
		declare function = {
			h(\x) = #2+(\x>=#4)*#3*(\x-#4);
		}]
		\ifthenelse{\equal{#1}{}}{
			\addplot[blue,thick,domain=0:7,samples=400]{h(\x)};
		}{
			\addplot[blue,thick,domain=0:7,samples=400]{h(\x)}node[below=.8cm,pos=.5]{$\psi_{e_#1}(.)+r_{w_#1}$};
		}	
		\ifthenelse{\equal{#5}{}}{}{
			\draw[red,ultra thick](axis cs:#5,0)--(axis cs:#5,{h(#5)});
			\coordinate(height)at(axis cs:#5,{h(#5)});
		}
	\end{axis}		
}

\newcommand{\tempWaterfillingAlgorithmPicture}[3]{
	\clip (.1,.1) rectangle (7.6,-4.2);
	\ifthenelse{\equal{#1}{}}{}{
		\fill[blue!10](0,-4) rectangle (7.7,-4+#1);
		\fill[white](4,-1.5) rectangle (6,-2.7+#2);
		\fill[white](6,-1) rectangle (7,-3.2+#3);
	}
	\draw[thick,fill=gray!60](0,0) -- ++(.5,0) -- ++(0,-4) -- ++(1,0) -- ++(0,1.5) -- ++(2,0) -- ++(0,1) -- ++(1,0) -- ++(0,-.1) -- ++(-.5,0) -- ++(0,-1) -- ++(1.3,0) -- ++(0,1) -- ++(-.5,0) -- ++(0,.1) -- ++(1,0) -- ++(0,.5) -- ++(.6,0) -- ++(0,-.1) -- ++(-.3,0) -- ++(0,-2) -- ++(0.9,0) -- ++(0,2) -- ++(-.3,0) -- ++(0,.1) -- ++(.6,0) -- ++(0,1) -- ++(.5,0) -- ++(0,-4.3) -- ++(-7.8,0) -- cycle;
	\ifthenelse{\equal{#1}{0}}{
		\node at(1,-.3){$x_{e_1}$};
		\node at(2.5,-.3){$x_{e_2}$};
		\node at(4.7,-.3){$x_{e_3}$};
		\node at(6.6,-.3){$x_{e_4}$};
		\draw[dashed](1.5,0)--+(0,-2.5);
		\draw[dashed](3.5,0)--+(0,-1.5);
		\draw[dashed](5.8,0)--+(0,-1);
		
		\node at(4.7,-2.1){$\nu_{e_3}$};
		\node at(6.6,-2.1){$\nu_{e_4}$};
		
		\draw[<->,thick] (2.5,-4.2) --node[right]{\small $r_{w_2}-1$} (2.5,-2.5);
		\draw[<->,thick] (1.5,-2.3) --node[above]{\small $\nu_{w_2}$} (3.5,-2.3);
	}{}				
}

\exProblem{This exercise continues exercise~4 from exercise sheet~4 where we described an algorithm for computing Wardrop equilibria.
\begin{enumerate}[label=\alph*)]
	\item Convince yourself that this algorithm can be applied in the context of the IDE-extension algorithm (\Cref*{alg:IDEExtension} in the lecture notes). What is its runtime here?
\end{enumerate}
Now, consider a situation where we are trying to find an IDE-extension at some node $v$ which has five outgoing edges $e_1=vw_1, \dots, e_5 = vw_5$. Assume that the edges $e_1$ to $e_4$ are active at the current point in time while edge~$e_5$ is inactive. Furthermore, the functions $x \mapsto \psi_{e_i}(x)+r_{w_i}$ have the following graphs:
\begin{center}
	\begin{tikzpicture}
		\tempWaterfillingAlgorithmGraphH[1]{.5}{1.3}{0}{}
	\end{tikzpicture}
	\begin{tikzpicture}
		\tempWaterfillingAlgorithmGraphH[2]{2}{.5}{0}{}
	\end{tikzpicture}
	\begin{tikzpicture}
		\tempWaterfillingAlgorithmGraphH[3]{3}{.5}{3}{}
	\end{tikzpicture}\\[1em]
	
	\begin{tikzpicture}
		\tempWaterfillingAlgorithmGraphH[4]{3.5}{.9}{5}{}
	\end{tikzpicture}
	\begin{tikzpicture}
		\tempWaterfillingAlgorithmGraphH[5]{2}{1}{2}{}
	\end{tikzpicture}	
\end{center}
\begin{enumerate}[label=\alph*),resume]
	\item Which of the five edges currently have a queue on them? How could you deduce the values $r_{w_i}$ and $\nu_{e_i}$ from these graphs if they had proper labels?
	
	\item How does the algorithm from exercise 4 on exercise sheet 4 work in this situation (geometrically)?
	
	\item This type of algorithm is also called a ``water filling''-algorithm. Can you explain why? Use the following picture:
		\begin{center}
			\begin{tikzpicture}
				\tempWaterfillingAlgorithmPicture{}{}{}
			\end{tikzpicture}
		\end{center}
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item The edge cost functions considered in \Cref*{alg:IDEExtension} are continuous and piecewise linear with one or two affine pieces (since the same is true for the functions $\psi_e$). Hence, we can apply the algorithm. Since each cost function has at most one break point, the algorithm will run through the while loop at most $2\abs{\edgesFrom{v}}$ times and each such loop can be done in $\bigO(\edgesFrom{v})$ steps. Hence, the total runtime is $\bigO(\edgesFrom{v}^2)$.
	
	\item From the definition of $\psi_e$
		\[\psi_e: \IR \to \IR, x \mapsto \begin{cases}
			\tfrac{x}{\nu_e}-1,		&\text{ of }\ql[a]>0 \\
			\max\set{\tfrac{x}{\nu_e}-1,0},	&\text{ else}
		\end{cases},\]
	we see that its graph has one of the following two forms (depending on whether there is a queue on edge~$e$ at the beginning of the interval (right) or not (left)):
		\begin{center}
			\begin{tikzpicture}[declare function = {
					h(\x) = -2+.7*x;
				}]
				\begin{axis}[
					axis lines=left,,
					axis x line shift=-2.5,
					ymin=-2.5,ymax=4,
					xmin=0,xmax=7,
					width=6cm, height=4cm,
					xtick=\empty,xticklabel=\empty,
					ytick=\empty,yticklabel=\empty]
					\addplot[blue,thick,domain=0:7,samples=400]{h(\x)}node[below=.15cm,pos=.8]{$\psi_{e}$};
					\coordinate(a) at(axis cs:0,0);
					\coordinate(b) at(axis cs:0,-2);
					\coordinate(c) at(axis cs:4.9,1.6);
				\end{axis}		
				\draw [decorate,decoration={brace,raise=2pt,amplitude=3pt}] (b) -- node[left=5pt]{$1$} (a);
				\draw[<-](c)to[bend right=20]+(-.7,.5)node[left]{\small slope $=\tfrac{1}{\nu_e}$};
			\end{tikzpicture}
			\hspace{2em}
			\begin{tikzpicture}[declare function = {
					h(\x) = 0+(x>=3)*1*(x-3);
				}]
				\begin{axis}[
					axis lines=left,,
					axis x line shift=-2.5,
					ymin=-2.5,ymax=4,
					xmin=0,xmax=7,
					width=6cm, height=4cm,
					xtick=\empty,xticklabel=\empty,
					ytick=\empty,yticklabel=\empty]
					\addplot[blue,thick,domain=0:7,samples=400]{h(\x)}node[below=.3cm,pos=.8]{$\psi_{e}$};
					\coordinate(a) at(axis cs:0,0);
					\coordinate(b) at(axis cs:3,0);
					\coordinate(c) at(axis cs:4.5,1.6);
				\end{axis}		
				\draw [decorate,decoration={brace,raise=2pt,amplitude=3pt}] (b) -- node[below=5pt]{$\nu_e$} (a);
				\draw[<-](c)to[bend right=20]+(-.7,.5)node[left]{\small slope $=\tfrac{1}{\nu_e}$};
			\end{tikzpicture}
		\end{center}
		From this, we can directly deduce that the edges $e_3$, $e_4$ and $e_5$ are the ones with queues. Moreover, $\nu_e$ is the inverse of the slopes and $r_e$ is the value of the function at time~$0$ (if there is a queue) or $1$ more than that (if there is no queue).
		
	\item Since $e_5$ is inactive, the algorithm only considers the first four edges. For those it proceeds as follows:
		\begin{center}
			\foreach \i/\j/\k/\l in {0/0/0/0,1.2/0/0/0,1.95/2/0/0,1.95/2/3/0,2.3/3/4/0,2.3/3/4/2}{
				\begin{adjustbox}{max width=\linewidth}
					\begin{tikzpicture}
						\begin{scope}[xshift=5cm]
							\tempWaterfillingAlgorithmGraphH{2}{.5}{0}{\j}
						\end{scope}
						\begin{scope}[xshift=10cm]
							\tempWaterfillingAlgorithmGraphH{3}{.5}{3}{\k}
						\end{scope}
						\begin{scope}[xshift=15cm]
							\tempWaterfillingAlgorithmGraphH{3.5}{.9}{5}{\l}
						\end{scope}
						\begin{scope}
							\tempWaterfillingAlgorithmGraphH{.5}{1.3}{0}{\i}
						\end{scope}
						\draw[dashed,red] (height) -- +(15,0);
					\end{tikzpicture}
				\end{adjustbox}\\[1.5em]
			}
		\end{center}
		
		\item The process depicted in part~c) can also be interpreted as filling a water basin with water in the following way:
			\begin{center}
				\begin{adjustbox}{max width=\linewidth}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{0}{0}{0}
					\end{tikzpicture}\hspace{1em}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{1.5}{0}{0}
					\end{tikzpicture}
				\end{adjustbox}	
				
				\begin{adjustbox}{max width=\linewidth}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{2.5}{0}{0}
					\end{tikzpicture}\hspace{1em}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{2.5}{1.2}{0}
					\end{tikzpicture}
				\end{adjustbox}
				
				\begin{adjustbox}{max width=\linewidth}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{3}{1.2}{0}
					\end{tikzpicture}\hspace{1em}
					\begin{tikzpicture}
						\tempWaterfillingAlgorithmPicture{3}{1.2}{1}
					\end{tikzpicture}
				\end{adjustbox}
			\end{center}
\end{enumerate}}

\exComment{This is a continuation of \exref{WardropEquilibriumComputationViaWaterFilling}}