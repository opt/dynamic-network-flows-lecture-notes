% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{ContinuityOfCumulativeEdgeLoading}

\exTitle{Continuity of Edge-Loading for Cumulative Flow Functions}

\exProblem{Let $e$ be some edge with capacity~$\nu_e \in \IRp$ and travel time~$\tau_e \in \IRnn$ and denote by
		\[\mathfrak{F} \coloneqq \set{F \in C(\IR) | F \text{ absolutely continuous, non-decreasing and } \restr{F}{(-\infty,0)} \equiv 0}\] 
	the set of absolutely continuous, non-decreasing functions which are zero on~$\IRn$. We consider the following mapping:
		\[\tilde{\Phi}: \mathfrak{F} \to \mathfrak{F}, F^+_e \mapsto F^-_e \text{ s.th. } (F^+_e,F^-_e) \text{ are cum. flow functions of a Vickrey flow on } e.\]
		
	\begin{enumerate}[label=\alph*)]
		\item Convince yourself that this mapping is well-defined.
		\item Show that this mapping is strong-strong continuous, \ie continuous \wrt the norm~$\norm{.}_\infty$ on both sides.
	\end{enumerate}
}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item Due to the fundamental theorem of calculus (\Cref*{thm:FundamentalTheoremOfCalculus}) any $F \in \mathfrak{F}$ has a (locally integrable) derivative $f$ almost everywhere and this derivative satisfies $\int_0^\theta f^(\zeta)\diff\zeta = F(\theta)-F(0)=F(\theta)$ for all $\theta \in \IR$. Moreover, this derivative is clearly zero on $\IRn$ and non-negative on $\IRnn$. Hence, we can apply \Cref*{lemma:EdgeLoadingExistence} to get a locally integrable function $g:\IR \to \IRnn$ such that $(f,g)$ is a Vickrey flow on~$e$. We then also have $(G: \IR \to \IR, \theta \mapsto \int_0^\theta g(\zeta)\diff\zeta) \in \mathfrak{F}$ and $(F,G)$ are the cumulative flow functions of a Vickrey flow. Finally, due to \Cref*{lemma:EdgeLoadingUniqueness} and, again, the fundamental theorem of calculus, there is also at most one such~$G$. 
	
	\item Let $(F^{n,+}_e)$ be a sequence in $\mathfrak{F}$ converging to some $F^+_e \in \mathfrak{F}$. We now have to show that
		\[F^{n,-}_e \coloneqq \tilde{\Phi}(F^{n,+}_e) \convto \tilde{\Phi}(F^+_e) \eqqcolon F^-_e.\]
	For this, let $\eps > 0$ be given and choose some $N \in \INo$ such that we have $\norm{F^+_e-F^{n,+}_e}_\infty \leq \eps$ for all $n \geq N$ (which exists because $F^{n,+}_e \convto F^+_e$). Now, take any $n \geq N$ and $\theta \geq \tau_e$. Assume \wlg that we have $F^-_e(\theta) \geq F^{n,-}_e(\theta)$. Then, we have some $\bar\theta \leq \theta-\tau_e$ with $F^{n,-}_e(\theta) = F^{n,+}_e(\bar\theta) + (\theta-\tau_e-\bar\theta)\nu_e$ by \Cref*{lemma:CharacterizationsOfQueueOpAtCap:OutflowInTwoParts} as well as $F^-_e(\theta) \leq F^+_e(\bar\theta)+(\theta-\tau_e-\bar\theta)\nu_e$ by \Cref*{lemma:CharacterizationsOfQueueOpAtCap:OutflowDetermined}. Together, this shows
		\[\abs{F^-_e(\theta)-F^{n,-}_e(\theta)} = F^-_e(\theta)-F^{n,-}_e(\theta) \leq F^+_e(\bar\theta)-F^{n,+}_e(\bar\theta) \leq \norm{F^+_e-F^{n,+}_e}_\infty \leq \eps.\]
	Since this holds for all $\theta \geq \tau_e$ while we also have $F^-_e(\theta) = 0 = F^{n,-}_e(\theta)$ for all $\theta < \tau_e$ (due to weak flow conservation), this shows that we have $\norm{F^-_e-F^{n,-}_e}_\infty \leq \eps$.
\end{enumerate}}

\exComment{}