% !TeX spellcheck = en_GB
%!TEX root = ../exercise-sheet.tex

\exID{AlternativeDynamicCut}

\exComment{Similar to \cite[Exercise 6.1]{SkutellaSurvey}}

\exTitle{Static Cuts for Dynamic Flows}

\exProblem{Let $\network=(G,s,t,\nu,\tau)$ be a dynamic flow network, $f$ a feasible flow in this network and $A \subseteq V$ a (static) \stcut{} in $(G,s,t,\nu)$.
	
\begin{enumerate}[label=\alph*)]
	\item Show that for any time horizon~$T$ we have
			\[\fval{f,T} \leq T\cdot\ccaps{A}.\]
	\item Show that even for the minimum \stcut{} this bound need not be tight.
	\item Can you find a better upper bound on the value of dynamic flows (\eg by replacing $T$ by something smaller in the above bound)?
\end{enumerate}}

\exSolution{\begin{enumerate}[label=\alph*)]
	\item Let $A \subseteq V$ define a static \stcut{}, \ie $s \in A$ and $t \notin A$. Then we have
		\begin{align*}
			\fval{f,T} &= \sum_{e \in \edgesTo{t}}F^-_e(T) - \sum_{e \in \edgesFrom{t}}F^+_e(T) \\
				&\leq \sum_{v \in V\setminus A}\Big(\underbrace{\sum_{e \in \edgesTo{v}}F^-_e(T)-\sum_{e \in \edgesFrom{v}}F^+_e(T)}_{\geq 0 \text{ for all } v \neq s}\Big) \\
				&= \sum_{e \in \edgesFrom{A}}F^-_e(T) - \sum_{e \in \edgesTo{A}}\underbrace{F^+_e(T)}_{\geq 0} + \sum_{\substack{e=vw \in E:\\v,w\notin A}}\big(\underbrace{F^-_e(T)-F^+_e(T)}_{\leq F^-_e(T+\tau_e)-F^+_e(T)\leq 0}\big) \\
				&\leq \sum_{e \in \edgesFrom{A}}\int_0^T\underbrace{f^-_e(\zeta)}_{\leq \nu_e}\diff\zeta \leq \sum_{e \in \edgesFrom{A}}T\cdot\nu_e \\
				&= T\cdot\sum_{e \in \edgesFrom{A}}\nu_e = T\cdot\ccaps{A}.
		\end{align*}
		
	\item That this bound is not tight, can already be seen in a single edge network if this edge $st$ has strictly positive travel time. In this network, the maximum dynamic flow~$f$ is clearly sending flow over this edge at capacity for all times and $A\coloneqq \set{s}$ is a minimum \stcut{}. At the same time we have
		\[\fval{f,T} = (T-\tau_{st})\cdot\nu_{st} < T\cdot\nu_{st} = T\cdot\ccaps{A}\]
	for any $T \geq \tau_{st}$.
	
	\item Two potentially stronger bounds (based on static \stcut s) are
		\[\max\set{0,(T-\tauPmin)}\cdot\ccaps{A} \text{ where } \tauPmin \coloneqq \min\set{\tau_p | p \in \PathSet}\]
	and 
		\[\sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,(T-\min\set{\tau_p | p \in \PathSet: e \in p})}.\]
	Intuitively, the first one holds since every particle that reaches the sink before~$T$ has to start its journey before time $(T-\tauPmin)$ as it has to traverse some \stpath{} before time~$T$. The second bound holds for ostensibly the same reason but we also observe that in order to make use of a specific edge~$e \in \edgesFrom{A}$ within the cut, a particle also has to travel along some \stpath{} containing that edge (which might be longer than the overall shortest \stpath{} of length~$\tauPmin$).
	
	We start by observing that the second bound is stronger than the first:
		\begin{align*}
			&\sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,(T-\underbrace{\min\set{\tau_p | p \in \PathSet: e \in p}}_{\geq \tauPmin})} \\
			&\quad\quad \leq \sum_{e \in \edgesFrom{A}}\nu_e\cdot\max\set{0,(T-\tauPmin)} = \max\set{0,(T-\tauPmin)}\cdot\ccaps{A}.
		\end{align*}
	Thus, both for correctness and non-tightness it suffices to only consider the second bound.
	
	Non-tightness can be seen in the following network:
	\begin{center}
		\begin{tikzpicture}
			\node[namedVertex](s)at(0,0){$s$};
			\node[namedVertex](t)at(6,0){$t$};
			\node[namedVertex](v)at(3,-1.5){$v$};
			
			\draw[edge](s)to[bend left=30]node[above]{$(1,1)$}(t);
			\draw[edge](s)to[bend left=20]node[above,sloped]{$(2,1)$}(v);
			\draw[edge](s)to[bend right=20]node[below,sloped]{$(5,2)$}(v);
			\draw[edge](v)to[bend right=20]node[below,sloped]{$(4,1)$}(t);
		\end{tikzpicture}
	\end{center}
	For the two possible \stcut s in this network we get the bounds of $5T-9$ and $8T-20$ (for $A=\set{s,v}$ and $A=\set{s}$, respectively). At the same time, one can also show (\eg using a dynamic \stcut) that the maximum dynamic flow with time horizon $\geq 3$ in this network has a value of~$5T-11$. Thus, for $T > 3$, neither of the two new bounds is tight.
	
	Since the correctness proof is surprisingly technical (or at least the one I found), we will postpone this for now.
\end{enumerate}}